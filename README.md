# Export ratings to Recommender #
1. Run "meteor shell"
3. RecommenderClient.exportAll()

# Create dummy user for development
In Browser console call:
```

Meteor.call('createDummyLogin')

```

The username and password is then logged to the Meteor console. You can login in the Browser console with the username/password, e.g.:

```

Meteor.loginWithPassword('6rgmnSbMpMvnqgnW6uBb','6rgmnSbMpMvnqgnW6uBb')

```

# Enable Mongo Oplog tailing on Production #
Here is what you have to do to add oplog tailing:

1. Connect to your Ubuntu server
2. Edit mongod config file /etc/mongod.conf (note it’s not the same as /etc/mongodb.conf)
3. Uncomment the line port = 27017 (if you don’t see this commented line, you edit the wrong file)
4. Uncomment the line starting with replSet and set the variable to meteor. It should be: replSet=meteor
5. Save the file
6. Restart mongod: service mongod restart
7. Connect to mongod: mongo
8. Copy paste the following code


```javascript

var config = {_id: "meteor", members: [{_id: 0, host: "127.0.0.1:27017"}]}
rs.initiate(config)

```

https://medium.com/meteor-secret/adding-oplog-tailing-with-meteor-up-mup-and-ubuntu-efa644f397e9#.f868ffw1a


# Useful commands and queries #


## Backup mongo local db ##
```sh

mongodump -h 127.0.0.1:3001 -d meteor -o $dumpsFolder

```



## Restore local mongo db ##
```sh

mongorestore --host 127.0.0.1 --db meteor  --port 3001 --drop /path/to/db/backup
```



## Get all E-mail addresses ##

```javascript

db.users.find({
    "profile.preferredLanguage": "en"
}, {
    "emails": 1
}).forEach(function(user) {
    if (user.emails) {
        for (var i = 0; i < user.emails.length; i++) {
            printjson(user.emails[i].address);
        }
    }
});

```



## Get number of ratings per user ##

```javascript
var userId = 'rR4dHMdR2aaCs4jm2';
var userRatingCount = 0;
db.modules.find().forEach(function(module) {
    module.ratings.forEach(function(rating) {
        if (rating.rating !== null && rating.userId === userId) {
            userRatingCount += 1;
        }
    });
});
print(userRatingCount);

```

## Get ratings per user and module ##

```sql

select * from ratings r, users u, modules m where r.user_id = u.id and m.id = r.module_id and u.meteor_user_id = 'rR4dHMdR2aaCs4jm2';

```


## Delete user in MongoDB ##

```javascript

var userId = '';
// remove all user ratings
db.modules.update({}, {
    $pull: {
        ratings: {
            userId: userId
        }
    }
}, {
    multi: true
});
// remove user
db.users.remove({
    _id: userId
});

```

## User statistics ##

```javascript

var dates = [
    "2016-02-20",
    "2016-02-21",
    "2016-02-22"
];

for (var i = 0; i < dates.length; i += 1) {

    var ratingCount = 0;
    var totalUserCount = 0;
    var usersWithRating = {};
    var networkAgreementAccepted = {};
    var fbPermissionCount = 0;
    var networkAgreementAcceptedCount = 0;
    var fbUsersWithAcceptedAgreementCount = 0;

    var date = new ISODate(dates[i]);


    db.modules.find().forEach(function(module) {
        module.ratings.forEach(function(rating) {
            if (rating.rating !== null && rating.createdAt.getTime() < date.getTime()) {
                ratingCount += 1;
                usersWithRating[rating.userId] = 1;
            }
        });
    });

    db.users.find().forEach(function(user) {
        if (user.createdAt.getTime() < date.getTime()) {
            totalUserCount += 1;
            if (user.profile.networkUserAgreement > 0) {
                networkAgreementAcceptedCount += 1;
                networkAgreementAccepted[user._id] = 1;
            }
        }
    });

    db.fbPermission.find().forEach(function(fbPermission) {
        if (fbPermission.createdAt.getTime() < date.getTime()) {
            fbPermissionCount += 1;
            if (networkAgreementAccepted[fbPermission.userId] > 0) {
                networkAgreementAccepted[fbPermission.userId] += 1;
            }
        }
    });

    Object.keys(networkAgreementAccepted).forEach(function(userId) {
        if (networkAgreementAccepted[userId] == 2) {
            fbUsersWithAcceptedAgreementCount += 1;
        }
    });

    print("Date: " + date);
    print("Number of ratings: " + ratingCount);
    print("User count (with rating): " + Object.keys(usersWithRating).length);
    print("User count (total): " + totalUserCount);
    print("Users who accepted network agreement: " + networkAgreementAcceptedCount);
    print("Users connected with FB: " + fbPermissionCount);
    print("Users connected with FB who accepted network agreement: " + fbUsersWithAcceptedAgreementCount);
    print("\n");
}

```


# Previous Database Updates #

## Database Update from 21.12.15 ##

```javascript

db.users.update({'profile.bsc': { $exists: false } }, { $set: { 'profile.bsc': { name: 'Bachelor', regulations: [] }  } }, { multi: true })
db.users.update({'profile.msc': { $exists: false } }, { $set: { 'profile.msc': { name: 'Master', regulations: [] } } }, { multi: true })
db.users.update({'profile.name': { $exists: false } }, { $set: { 'profile.name' : '' } }, { multi: true })
db.users.update({'profile.userAgreement': { $exists: false } }, { $set: { 'profile.userAgreement' : null } }, { multi: true })
db.users.update({}, { $unset: { 'profile.name': '' } }, { multi: true })

```
```javascript

db.modules.find().forEach(function(module) {
    module.ratings.forEach(function (rating) {
        rating.createdAt = new Date();
    });
    db.modules.save(module);
});

```
