Package.describe({
    summary: 'Notebook - Web App and Admin Template (v.1.1.0), fully responsive web application and admin dashboard template, from Flatfull, packaged for Meteor.js.'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.0.1');
    api.use('ecmascript');
    api.use('less');
    api.use('jquery');

    var path = Npm.require('path');
    var assetPath = path.join('notebook-theme');

    api.addFiles([
        // CSS
        path.join(assetPath, 'css', 'bootstrap.css'),
        path.join(assetPath, 'css', 'animate.css'),
        path.join(assetPath, 'css', 'font.css'),
        path.join(assetPath, 'js', 'fuelux', 'fuelux.css'),
        path.join(assetPath, 'js', 'select2', 'select2.css'),
        path.join(assetPath, 'js', 'select2', 'theme.css'),
        //  path.join(assetPath, 'js', 'datepicker', 'datepicker.css'),
        path.join(assetPath, 'less', 'app.arrow.import.less'),
        path.join(assetPath, 'less', 'app.buttons.import.less'),
        path.join(assetPath, 'less', 'app.colors.import.less'),
        path.join(assetPath, 'less', 'app.layout.import.less'),
        path.join(assetPath, 'less', 'app.mixins.import.less'),
        path.join(assetPath, 'less', 'app.nav.import.less'),
        path.join(assetPath, 'less', 'app.plugin.import.less'),
        path.join(assetPath, 'less', 'app.reset.import.less'),
        path.join(assetPath, 'less', 'app.switch.import.less'),
        path.join(assetPath, 'less', 'app.utilities.import.less'),
        path.join(assetPath, 'less', 'app.variables.import.less'),
        path.join(assetPath, 'less', 'app.widgets.import.less'),
        path.join(assetPath, 'less', 'app.custom.import.less'),
        path.join(assetPath, 'less', 'app.less'),
        path.join(assetPath, 'js', 'fullcalendar', 'fullcalendar.css'),
        path.join(assetPath, 'js', 'fullcalendar', 'theme.css'),
        path.join(assetPath, 'js', 'datatables', 'datatables.css'),
        // JS
        path.join(assetPath, 'js', 'bootstrap.js'),
        path.join(assetPath, 'js', 'app.js'),
        path.join(assetPath, 'js', 'slimscroll', 'jquery.slimscroll.min.js'),
        path.join(assetPath, 'js', 'app.plugin.js'),
        path.join(assetPath, 'js', 'fuelux', 'fuelux.js'),
        path.join(assetPath, 'js', 'wysiwyg', 'bootstrap-wysiwyg.js'),
        path.join(assetPath, 'js', 'wysiwyg', 'jquery.hotkeys.js'),
        //  path.join(assetPath, 'js', 'parsley', 'parsley.min.js'),
        //  path.join(assetPath, 'js', 'file-input', 'bootstrap-filestyle.min.js'),
        //  path.join(assetPath, 'js', 'datepicker', 'bootstrap-datepicker.js'),
        //  path.join(assetPath, 'js', 'libs', 'moment.min.js'),
        //  path.join(assetPath, 'js', 'combodate', 'combodate.js'),
        path.join(assetPath, 'js', 'select2', 'select2.min.js'),
        //  path.join(assetPath, 'js', 'slider', 'bootstrap-slider.js'),
        //  path.join(assetPath, 'js', 'jquery.ui.touch-punch.min.js'),
        //  path.join(assetPath, 'js', 'jquery-ui-1.10.3.custom.min.js'),
        path.join(assetPath, 'js', 'fullcalendar', 'fullcalendar.min.js'),
        path.join(assetPath, 'js', 'charts', 'easypiechart', 'jquery.easy-pie-chart.js'),
        path.join(assetPath, 'js', 'charts', 'flot', 'jquery.flot.min.js'),
        path.join(assetPath, 'js', 'charts', 'flot', 'jquery.flot.pie.min.js')
    ], 'client');

    api.addAssets([
        // Fonts
        path.join(assetPath, 'fonts', 'opensans', 'opensans-bold-webfont.woff'),
        path.join(assetPath, 'fonts', 'opensans', 'opensans-light-webfont.woff'),
        path.join(assetPath, 'fonts', 'opensans', 'opensans-webfont.woff'),
        path.join(assetPath, 'fonts', 'FontAwesome.otf'),
        path.join(assetPath, 'fonts', 'fontawesome-webfont.eot'),
        path.join(assetPath, 'fonts', 'fontawesome-webfont.svg'),
        path.join(assetPath, 'fonts', 'fontawesome-webfont.ttf'),
        path.join(assetPath, 'fonts', 'fontawesome-webfont.woff')
    ], 'client');
});