BaseSchema = new SimpleSchema({
    createdById: {
        type: String,
        index: true,
        autoValue: function() {
            var userId = Meteor.userId();
            if (this.isInsert) {
                return userId;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: userId
                };
            } else {
                this.unset();
            }
        },
        denyUpdate: false
    },
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: new Date()
                };
            } else {
                this.unset();
            }
        },
        denyUpdate: false
    },
    updatedById: {
        type: String,
        autoValue: function() {
            if (this.isUpdate) {
                return Meteor.userId();
            }
        },
        denyInsert: true,
        optional: true
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        },
        denyInsert: true,
        optional: true
    }
});


var SemesterPlanInlineModulesSchema = new SimpleSchema({
    moduleId: {
        type: String
    },
    lectureIds: {
        type: [String],
        optional: true
    }
});

SemesterPlanSchema = new SimpleSchema([
    BaseSchema, {
        userId: {
            type: String,
            label: i18n('userId'),
            index: 1,
            autoValue: function() {
                var userId = Meteor.userId();
                if (this.isInsert) {
                    return userId;
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: userId
                    };
                } else {
                    this.unset();
                }
            }
        },
        semesterId: {
            type: String,
            index: 1
        },
        modules: {
            type: [SemesterPlanInlineModulesSchema],
            optional: true
        }

    }
]);

var listedFbFriendSchema = new SimpleSchema({
    facebookId: {
        type: String
    },
    facebookName: {
        type: String
    },
    createdAt: {
        type: Date
    }
});

var registeredFriendsSchema = new SimpleSchema({
    userId: {
        type: String
    }
});



FBPermissionSchema = new SimpleSchema([
    BaseSchema, {
        userId: {
            type: String,
            label: i18n('userId'),
            index: 1,
            autoValue: function() {
                var userId = Meteor.userId();
                if (this.isInsert) {
                    return userId;
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: userId
                    };
                } else {
                    this.unset();
                }
            }
        },
        facebookId: {
            type: String
        },
        shibbolethId: {
            type: String
        }
    }

]);

FBInfoSchema = new SimpleSchema([
    BaseSchema, {
        userId: {
            type: String,
            label: i18n('userId'),
            index: 1,
            autoValue: function() {
                var userId = Meteor.userId();
                if (this.isInsert) {
                    return userId;
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: userId
                    };
                } else {
                    this.unset();
                }
            }
        },
        facebookId: {
            type: String,
            index: 1
        },
        friends: {
            type: [listedFbFriendSchema]
        },
        firstName: {
            type: String,
            optional: true
        },
        lastName: {
            type: String,
            optional: true
        },
        email: {
            type: String,
            optional: true
        },
        gender: {
            type: String,
            optional: true
        },
        locale: {
            type: String,
            optional: true
        },
        ageRange: {
            type: String,
            optional: true
        },
        totalFriends: {
            type: Number,
            optional: true
        },
        timezone: {
            type: Number,
            optional: true
        },
        verified: {
            type: Boolean,
            optional: true
        }
    }

]);

RecommendationSchema = new SimpleSchema({
    userId: {
        type: String
    },
    order: {
        type: Number
    },
    moduleId: {
        type: String
    },
    title: {
        type: String
    },
    uzhUrl: {
        type: String
    },
    rating: {
        type: Number,
        decimal: true
    }
});

Task2SurveyOneSchema = new SimpleSchema({
    answ1: {
        type: String
    },
    answ2: {
        type: Number
    },
    answ3: {
        type: Number
    }
});

Task2SurveyTwoSchema = new SimpleSchema({
    answ4: {
        type: String
    },
    answ5: {
        type: String
    },
    answ6: {
        type: String
    },
    answ7: {
        type: String
    },
    answ8: {
        type: String
    },
    answ9: {
        type: String
    }
});

Task3Survey2Schema = new SimpleSchema({
    answ1: {
        type: Number
    },
    answ2: {
        type: Number
    },
    answ3: {
        type: Number
    },
    answ4: {
        type: Number
    },
    answ5: {
        type: Number
    },
    answ6: {
        type: Number
    },
    answ7: {
        type: Number
    },
    answ8: {
        type: Number
    },
    answ9: {
        type: Number
    },
    answ10: {
        type: Number
    },
    answ11: {
        type: Number
    },
    answ12: {
        type: Number
    }
});

Task3Survey3Schema = new SimpleSchema({
    answ1: {
        type: Number
    },
    answ2: {
        type: Number
    },
    answ3: {
        type: Number
    },
    answ4: {
        type: Number
    },
    answ5: {
        type: Number
    },
    moduleId: {
        type: String
    },
    userId: {
        type: String
    }
});

Task3Survey4Schema = new SimpleSchema({
    answ1: {
        type: String
    },
    answ2: {
        type: String
    },
    answ3: {
        type: String
    },
    answ4: {
        type: String
    },
    answ5: {
        type: String
    },
    answ6: {
        type: String
    },
    answ7: {
        type: String
    }
});

ExperimentSubjectSchema = new SimpleSchema([
    BaseSchema, {
        userId: {
            type: String,
            label: i18n('userId'),
            index: 1,
            autoValue: function() {
                var userId = Meteor.userId();
                if (this.isInsert) {
                    return userId;
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: userId
                    };
                } else {
                    this.unset();
                }
            }
        },
        hs16Degree: {
            type: String,
            optional: true
        },
        completedRegistration: {
            type: Number,
            optional: true
        },
        registeredFriends: {
            type: [registeredFriendsSchema]
        },
        matchPartnerId: {
            type: String,
            optional: true
        },
        matchPartnerFbId: {
            type: String,
            optional: true
        },
        matchPartnerCallName: {
            type: String,
            optional: true
        },
        recBehavior: {
            type: [String],
            optional: true
        },
        algRec: {
            type: [RecommendationSchema],
            optional: true
        },
        humRec: {
            type: [RecommendationSchema],
            optional: true
        },
        humRecSortList: {
            type: [String],
            optional: true
        },
        humRecBinList: {
            type: [String],
            optional: true
        },
        task2SurveyOne: {
            type: Task2SurveyOneSchema,
            optional:true
        },
        task2SurveyTwo: {
            type: Task2SurveyTwoSchema,
            optional: true
        },
        Task2Stage: {
            type: Number,
            optional: true
        },
        recommendationMatrix: {
            type: [RecommendationSchema],
            optional: true
        },
        task3Survey1: {
            type: Number,
            optional: true
        },
        task3Survey2Hum: {
            type: Task3Survey2Schema,
            optional: true
        },
        task3Survey2Alg: {
            type: Task3Survey2Schema,
            optional: true
        },
        task3Survey2FreeText1: {
            type: String,
            optional: true
        },
        task3Survey2FreeText2: {
            type: String,
            optional: true
        },
        task3Survey2HumanListEstimated: {
            type: String,
            optional: true
        },
        task3Survey2Lookups: {
            type: [String],
            optional: true
        },
        task3Survey3: {
            type: [Task3Survey3Schema],
            optional: true
        },
        task3Survey4: {
            type: Task3Survey4Schema,
            optional: true
        },
        task3State: {
            type: Number,
            optional: true
        },
        personalizedTreatment: {
            type: Number,
            optional: true
        },
        humanListIsRight: {
            type: Number,
            optional: true
        }
    }

]);
