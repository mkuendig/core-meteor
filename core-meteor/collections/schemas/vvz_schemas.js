OrganizationalUnitSchema = new SimpleSchema({
    title: {
        type: String
    },
    url: {
        type: String,
        optional: true
    }
});

SemesterSchema = new SimpleSchema({
    title: {
        type: String
    },
    semesterStart: {
        type: String
    },
    semesterEnd: {
        type: String
    }
});

CategorySchema = new SimpleSchema({
    title: {
        type: String,
        optional: true
    },
    slug: {
        type: String
    },
    moduleType: {
        type: String
    },
    children: {
        type: [String],
        optional: true
    }
});

CategoryInlineSchema = new SimpleSchema({
    title: {
        type: String
    },
    slug: {
        type: String
    },
    moduleType: {
        type: String
    }
});

PersonSchema = new SimpleSchema({
    uzhId: {
        type: String,
        index: true
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    position: {
        type: String,
        optional: true
    }
});

RoomSchema = new SimpleSchema({
    uzhId: {
        type: String,
        index: true
    },
    name: {
        type: String,
        max: 20
    },
    street: {
        type: String,
        optional: true
    },
    address: {
        type: String,
        optional: true
    }
});

ScheduleItemsSchema = new SimpleSchema({
    startDatetime: {
        type: String
    },
    endDatetime: {
        type: String
    },
    lecturers: {
        type: [PersonSchema]
    },
    rooms: {
        type: [RoomSchema]
    }

});

LectureSchema = new SimpleSchema({
    title: {
        type: String
    },
    uzhId: {
        type: String
    },
    scheduleItems: {
        type: [ScheduleItemsSchema]
    },
    lectureCategory: {
        type: String
    }

});

CommentSchema = new SimpleSchema({
    createdById: {
        type: String,
        index: true
    },
    createdAt: {
        type: Date
    },
    comment: {
        type: String,
        label: i18n('title')
    },

    upvotes: {
        type: [String],
        defaultValue: []
    },

    downvotes: {
        type: [String],
        defaultValue: []
    }
});

RatingSchema = new SimpleSchema({
    userId: {
        type: String,
        index: true
    },
    rating: {
        index: true,
        type: Number,
        label: i18n('rating'),
        optional: true,
        min: 1,
        max: 5
    },
    createdAt: {
        type: Date,
        optional: false
    }
});

PredictionSchema = new SimpleSchema({
    userId: {
        type: String,
        index: true
    },
    prediction: {
        index: true,
        type: Number,
        label: i18n('prediction'),
        optional: true,
        decimal: true,
        min: 0,
        max: 5
    },
    isNotInterested: {
        type: Boolean,
        index: true,
        defaultValue: false,
        optional: true
    }
});

StatsSchema = new SimpleSchema({
    avgRating: {
        index: true,
        type: Number,
        optional: true,
        decimal: true,
        min: 0,
        max: 5
    },
    prevAvgRating: {
        index: true,
        type: Number,
        optional: true,
        decimal: true,
        min: 0,
        max: 5
    },
    trend: {
        index: true,
        type: Number,
        optional: true,
        decimal: true,
        min: 0,
        max: 5
    },
    numberOfRatings: {
        index: true,
        type: Number,
        optional: true
    }
});

ModuleSchema = new SimpleSchema({
    isActive: {
        type: Boolean,
        index: true,
        defaultValue: true
    },
    moduleId: {
        type: String,
        index: true,
        unique: true
    },
    uzhUrl: {
        type: String,
        optional: true
    },
    semester: {
        type: SemesterSchema
    },
    title: {
        type: String
    },
    categories: {
        type: [CategoryInlineSchema]
    },
    description: {
        type: String,
        optional: true
    },
    ects: {
        type: Number,
        decimal: true
    },
    bookDeadline: {
        type: String
    },
    cancelDeadline: {
        type: String
    },
    personInCharge: {
        type: [PersonSchema]
    },
    lectures: {
        type: [LectureSchema],
        optional: true
    },
    organizationalUnits: {
        type: [OrganizationalUnitSchema],
        optional: true
    },
    language: {
        type: String,
        optional: true
    },
    lecturers: {
        type: [PersonSchema]
    },
    comments: {
        type: [CommentSchema],
        optional: true
    },
    ratings: {
        type: [RatingSchema],
        optional: true,
        defaultValue: []
    },
    predictions: {
        type: [PredictionSchema],
        optional: true,
        defaultValue: []
    },
    stats: {
        type: StatsSchema,
        optional: true
    },
    weekdays: {
        type: [Number],
        optional: true
    }
});
