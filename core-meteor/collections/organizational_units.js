OrganizationalUnits = new Meteor.Collection('organizationalUnits');
OrganizationalUnits.attachSchema(OrganizationalUnitSchema);

OrganizationalUnits.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});

OrganizationalUnits.createOrUpdate = function(orgUnit) {
    OrganizationalUnits.upsert({
        title: orgUnit.title
    }, {
        $set: {
            title: orgUnit.title,
            url: orgUnit.url
        }
    });
};