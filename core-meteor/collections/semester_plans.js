SemesterPlans = new Meteor.Collection('semesterPlans');
SemesterPlans.attachSchema(SemesterPlanSchema);

SemesterPlans.allow({
    insert: Permission.userIsLoggedIn,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});

/**
 * @param  {String} semesterId
 * @param  {String} userId
 * @return {String} semesterPlanId
 */
SemesterPlans.findOrCreateSemesterPlan = function(semesterId, userId) {
    check(semesterId, String);
    check(userId, String);
    var semesterPlan = SemesterPlans.findOne({
        userId: userId,
        semesterId: semesterId
    });
    if (_.isUndefined(semesterPlan)) {
        return SemesterPlans.insert({
            userId: userId,
            semesterId: semesterId,
            modules: []
        });
    } else {
        return semesterPlan._id;
    }
};

/**
 * @param  {String}     semesterPlanId
 * @return {[String]}   Array with moduleIds
 */
SemesterPlans.getModuleIds = function(semesterPlanId) {
    check(semesterPlanId, String);
    var semesterPlan = SemesterPlans.findOne(semesterPlanId);
    if (!_.isUndefined(semesterPlan)) {
        return _.pluck(semesterPlan.modules, 'moduleId');
    }
};

SemesterPlans.getCurrentSemesterPlanNames = function() {
    var semesters = [{}, {}];
    //var currentMonth = moment().format('MM');
    //if (_.contains(['08', '09', '10', '11', '12', '01'], currentMonth)) {
    //    semesters[0].name = 'hs';
    //    semesters[1].name = 'fs';
    //} else {
    //    semesters[0].name = 'fs';
    //    semesters[1].name = 'hs';
    //}
    //semesters[0].name += moment().format('YY');
    //semesters[1].name += moment().add(6, 'months').format('YY');
    semesters[0].name = 'fs16';
    semesters[1].name = 'hs15';
    return semesters;
};

Meteor.methods({

    setSelectedLectures: function(semesterPlanId, moduleId, lectureIds) {
        SemesterPlans.update({
            _id: semesterPlanId,
            'modules.moduleId': moduleId
        }, {
            $set: {
                'modules.$.lectureIds': lectureIds
            }
        });
    },

    addModuleToSemesterPlan: function(semesterPlanId, moduleId) {
        var lectureIds;
        var lectures = Modules.findOne({
            moduleId: moduleId
        }).lectures;

        if (lectures && lectures.length < 2) {
            lectureIds = _.pluck(lectures, 'uzhId');
        } else if (lectures) {
            var exercises = _.filter(lectures, function(lecture) {
                return lecture.lectureCategory !== 'Vorlesung';
            });
            if (exercises.length < 2) {
                lectureIds = _.pluck(lectures, 'uzhId');
            } else {
                lectureIds = _.pluck(_.filter(lectures, function(lecture) {
                    return lecture.lectureCategory === 'Vorlesung';
                }), 'uzhId');
            }
        }
        SemesterPlans.update({
            _id: semesterPlanId
        }, {
            $addToSet: {
                modules: {
                    moduleId: moduleId,
                    lectureIds: lectureIds
                }
            }
        });
        Meteor.call('setRating', moduleId, null);
    },

    removeModuleFromSemesterPlan: function(semesterPlanId, moduleId) {
        SemesterPlans.update({
            _id: semesterPlanId
        }, {
            $pull: {
                modules: {
                    moduleId: moduleId
                }
            }
        });
        var module = Modules.findOne({
            moduleId: moduleId
        });
        if (RatingController.getRating(module) === null) {
            Meteor.call('removeRating', moduleId);
        }
    }
});