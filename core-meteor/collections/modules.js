var minAvgRatingThreshold = 3;
var minNumberOfRatingsThreshold = 5;

Modules = new Meteor.Collection('modules');
Modules.attachSchema(ModuleSchema);

Modules.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});

Modules.getSemesterPlanModules = function(semesterPlanId) { // return modules from a semesterplan with lectureIds as selectedLectures array
    var semesterPlan = SemesterPlans.findOne(semesterPlanId);
    var semesterPlanModules = [];
    if (semesterPlan) {
        _.each(semesterPlan.modules, function(module) {
            var semesterPlanModule = Modules.findOne({
                moduleId: module.moduleId
            });
            if (semesterPlanModule) {
                semesterPlanModule.selectedLectures = module.lectureIds;
                semesterPlanModules.push(semesterPlanModule);
            }
        });
    }
    return semesterPlanModules;
};

Modules.getModulesForSemester = function(semester) {
    semester = semester.toUpperCase();
    return Modules.find({
        'semester.title': semester
    });
};

Modules.getModulesFromCurrentUser = function() {
    return Modules.find({
        'ratings.userId': Meteor.userId()
    }, {
        sort: {
            moduleId: 1
        }
    }).fetch();
};

Modules.getHS16ModulesFromCurrentUser = function() {
    return Modules.find({
        'ratings.userId': Meteor.userId(),
        'semester.title': 'HS16'
    }, {
        sort: {
            moduleId: 1
        }
    }).fetch();
};

Modules.getTopRecommendations = function() {
    var semesterId = Session.get('semesterId');
    var semesterRegExp = semesterId ? new RegExp(semesterId.toUpperCase()) : new RegExp('');
    return Modules.find({
        'predictions.userId': Meteor.userId(),
        'predictions.isNotInterested': {
            $ne: true
        },
        'semester.title': semesterRegExp,
        'ratings.userId': {
            $nin: [Meteor.userId()]
        }
    }, {
        sort: {
            'predictions.prediction': -1,
            'moduleId': -1
        },
        limit: 100
    });
};

Modules.getHS16RecommendationsByUserId = function(id) {
    var semesterId = 'hs16';
    var semesterRegExp = semesterId ? new RegExp(semesterId.toUpperCase()) : new RegExp('');
        return Modules.find({
            'predictions.userId': id,
            'semester.title': semesterRegExp,
            'ratings.userId': {
                $nin: [id]
            }
        }, {fields: 
                {'moduleId': 1, 'title':1, 'uzhUrl':1, 'predictions': {$elemMatch : {userId: id}}, 'categories': 1}
        });
};

Modules.getHS16MissingRecommendationsByUserId = function(id){
    var semesterId = 'hs16';
    var semesterRegExp = semesterId ? new RegExp(semesterId.toUpperCase()) : new RegExp('');
        return Modules.find({
            'predictions.userId': {
                $nin: [id]
            },
            'semester.title': semesterRegExp,
            'ratings.userId': {
                $nin: [id]
            },
        }, {fields: 
                {'moduleId': 1, 'title':1, 'uzhUrl':1, 'predictions': {$elemMatch : {userId: id}}, 'categories': 1}
        });
};

Modules.getBestRatedModulesByCategory = function(categorySlug, n) {
    return Modules.find({
        'categories.slug': categorySlug,
        'stats.avgRating': {
            $gte: minAvgRatingThreshold
        },
        'stats.numberOfRatings': {
            $gte: minNumberOfRatingsThreshold
        }
    }, {
        sort: {
            'stats.avgRating': -1
        },
        limit: n,
        reactive: false
    });
};

Modules.getBestRatedModulesByOrganizationalUnit = function(organizationalUnit, n) {
    return Modules.find({
        'organizationalUnits.title': organizationalUnit,
        'stats.avgRating': {
            $gte: minAvgRatingThreshold
        },
        'stats.numberOfRatings': {
            $gte: minNumberOfRatingsThreshold
        }
    }, {
        sort: {
            'stats.avgRating': -1
        },
        limit: n,
        reactive: false
    });
};

Modules.getTrendingModules = function(n) {
    return Modules.find({
        'stats.avgRating': {
            $gte: minAvgRatingThreshold
        },
        'stats.numberOfRatings': {
            $gte: minNumberOfRatingsThreshold
        },
        'stats.trend': {
            $gt: 0
        }
    }, {
        sort: {
            'stats.trend': -1
        },
        limit: n,
        reactive: false
    });
};

Modules.getWorstRatedModules = function(n) {
    return Modules.find({
        'stats.avgRating': {
            $gte: minAvgRatingThreshold
        },
        'stats.numberOfRatings': {
            $gte: minNumberOfRatingsThreshold
        }
    }, {
        sort: {
            'stats.avgRating': 1
        },
        limit: n,
        reactive: false
    });
};

Modules.getEctsSum = function(moduleIds) {
    if (!moduleIds) {
        return 0;
    }
    var total = 0;
    Modules.find({
        moduleId: {
            $in: moduleIds
        }
    }).forEach(function(module) {
        total += module.ects;
    });
    return total;
};
