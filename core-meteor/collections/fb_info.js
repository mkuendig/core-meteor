FBInfo = new Meteor.Collection('fbInfo');
FBInfo.attachSchema(FBInfoSchema);

FBInfo.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});


FBInfo.findOrCreateFBInfo = function(coId, fbId) {
    check(fbId, String);
    check(coId, String);
    var fbInfo = FBInfo.findOne({
        userId: coId,
        facebookId: fbId
    });
    if (_.isUndefined(fbInfo)) {
        return FBInfo.insert({
            userId: coId,
            facebookId: fbId,
            friends: []
        });
    } else {
        return fbInfo._id;
    }
};