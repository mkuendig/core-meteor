AlgRecommendations = new Meteor.Collection('algRecommendations');
//HumanRecommendations = new Meteor.Collection('humanRecommendations');
TutorialRecommendations = new Meteor.Collection('tutorialRecommendations');

AlgRecommendations.attachSchema(RecommendationSchema);
//HumanRecommendations.attachSchema(RecommendationSchema);
TutorialRecommendations.attachSchema(RecommendationSchema);

AlgRecommendations.allow({
    update: Permission.denyAlways,
    insert: Permission.denyAlways,
    remove: Permission.denyAlways
});

TutorialRecommendations.allow({
    update: Permission.denyAlways,
    insert: Permission.denyAlways,
    remove: Permission.denyAlways
});

/*
HumanRecommendations.allow({
    update: function () {
        return true;
    },
    insert: Permission.denyAlways,
    remove: function () {
        return true;
    }
});
*/

AlgRecommendations.clearAlgRecommendations = function(_userId) {
        AlgRecommendations.remove({
            userId: _userId
        });
};
/*
HumanRecommendations.clearHumanRecommendations = function(_userId) {
        HumanRecommendations.remove({
            userId: _userId
        });
};
*/

Sortable.collections = ['algRecommendations', 'tutorialRecommendations'];
