Persons = new Meteor.Collection('persons');
Persons.attachSchema(PersonSchema);

Persons.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});