ExperimentSubject = new Meteor.Collection('experimentSubject');
ExperimentSubject.attachSchema(ExperimentSubjectSchema);

ExperimentSubject.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});

ExperimentSubject.findOrCreateExperimentSubject = function(_userId) {
    var subject = ExperimentSubject.findOne({
        userId: _userId
    });

    if (_.isUndefined(subject)) {
        return ExperimentSubject.insert({
            userId: _userId,
            completedRegistration: 0,
            hs16Degree: "undefined",
            registeredFriends: []
        });
    } else {
        return subject._id;
    }
};
