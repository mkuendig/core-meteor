Categories = new Meteor.Collection('categories');
Categories.attachSchema(CategorySchema);

Categories.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});

/**
 * Return all categorySlugs of all children
 * of a category (subcategory)
 * @param  {String} categorySlug
 * @return {[String]} Array of categorySlugs
 */
Categories.getAllChildren = function(categorySlug) {
    var addAllChildren = function(slug) {
        var category = Categories.findOne({
            slug: slug
        });
        children.push({
            title: category.title,
            slug: category.slug,
            moduleType: category.moduleType
        });
        _.each(category.children, addAllChildren);
    };

    var children = [];
    var category = Categories.findOne({
        slug: categorySlug
    });
    _.each(category.children, addAllChildren);
    return children;
};

/**
 * Return the categorySlugs of the direct child / subcategory
 * @param  {String} categorySlug
 * @return {[String]} Array of categorySlugs
 */
Categories.getDirectChildren = function(categorySlug) {
    var children = [];
    var category = Categories.findOne({
        slug: categorySlug
    });
    if (!_.isUndefined(category)) {
        _.each(category.children, function(childSlug) {
            var child = Categories.findOne({
                slug: childSlug
            });
            children.push({
                title: child.title,
                slug: child.slug,
                moduleType: child.moduleType,
                selected: false
            });
        });
    }
    return children;
};