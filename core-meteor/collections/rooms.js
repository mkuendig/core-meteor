Rooms = new Meteor.Collection('rooms');
Rooms.attachSchema(RoomSchema);

Rooms.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});