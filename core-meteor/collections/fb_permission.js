FBPermission = new Meteor.Collection('fbPermission');
FBPermission.attachSchema(FBPermissionSchema);

FBPermission.allow({
    insert: Permission.denyAlways,
    update: Permission.denyAlways,
    remove: Permission.denyAlways
});

FBPermission.findOrCreateFBPermission = function(_fbId, _shibbolethId, _userId) {
    var perm = FBPermission.findOne({
        userId: _userId,
        facebookId: _fbId
    });

    if (_.isUndefined(perm)) {
        return FBPermission.insert({
            userId: _userId,
            facebookId: _fbId,
            shibbolethId: _shibbolethId
        });
    } else {
        FBPermission.remove({
            userId: _userId,
            facebookId: _fbId
        });
        return FBPermission.insert({
            userId: _userId,
            facebookId: _fbId,
            shibbolethId: _shibbolethId
        });

    }
};
