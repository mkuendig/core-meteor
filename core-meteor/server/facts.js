Facts.setUserIdFilter(function() {
    return true;
});

Meteor.methods({

    getNumberOfRatings: function() {
        var totalRatingCount = 0;
        Modules.find().forEach(function(module) {
            _.each(module.ratings, function(rating) {
                if (rating.rating !== null) {
                    totalRatingCount += 1;
                }
            });
        });
        return totalRatingCount;
    },

    getNumberOfUsersWithRating: function() {
        var users = {};
        Modules.find().forEach(function(module) {
            _.each(module.ratings, function(rating) {
                if (rating.rating !== null) {
                    users[rating.userId] = 1;
                }
            });
        });
        return Object.keys(users).length;
    },

    getNumberOfUsers: function() {
        return Meteor.users.find().count();
    },

    getNumberOfOecStudents: function() {
        return Meteor.users.find({
            'profile.studyProfile.current.studyCourse': 'economics'
        }).count();
    },

    getNumberOfIfiStudents: function() {
        return Meteor.users.find({
            'profile.studyProfile.current.studyCourse': 'informatics'
        }).count();
    }

});