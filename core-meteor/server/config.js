Accounts.config({
    sendVerificationEmail: false,
    forbidClientAccountCreation: true,
    loginExpirationInDays: 10
});

ServiceConfiguration.configurations.remove({
    service: 'facebook'
});

ServiceConfiguration.configurations.insert({
    service: 'facebook',
    appId: '120646968314854',
    secret: '70baeb00ae08465541dba6fbac6d6119'
});