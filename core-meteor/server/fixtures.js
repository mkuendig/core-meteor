//If meteor was started with the settings file where production=true we don't want to run some fixtures (like test users)
if (DEBUG()) {
    console.log('In development mode');
} else {
    console.log('In production mode');
}