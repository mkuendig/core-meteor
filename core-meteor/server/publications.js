Meteor.publish('categories', function() {
    return Categories.find();
});

Meteor.publish('modules', function() {
    return Modules.find({ // minimum required module information for home route
        isActive: true
    }, {
        fields: {
            categories: 1,
            moduleId: 1,
            organizationalUnits: 1,
            semester: 1,
            stats: 1,
            title: 1
        }
    });
});

Meteor.publish('modulesAdditionalFields', function() {
    return Modules.find({
        isActive: true
    }, {
        fields: {
            ects: 1,
            lecturers: 1,
            weekdays: 1
        }
    });
});

Meteor.publish('moduleDetails', function(moduleId) {
    check(moduleId, String);
    return Modules.find({
        isActive: true,
        moduleId: moduleId
    }, {
        fields: {
            isActive: 0
        }
    });
});

Meteor.publish('moduleLectures', function(moduleIds) {
    return Modules.find({
        isActive: true,
        moduleId: {
            $in: moduleIds
        }
    }, {
        fields: {
            lectures: 1
        }
    });
});

Meteor.publish('organizationalUnits', function() {
    return OrganizationalUnits.find();
});

Meteor.publish('persons', function() {
    return Persons.find();
});

Meteor.publish('predictions', function() {
    if (!this.userId) {
        return;
    }
    return Modules.find({
        isActive: true,
        'predictions.userId': this.userId
    }, {
        fields: {
            'predictions.$': 1
        }
    });
});

Meteor.publish('ratings', function() {
    //TODO Not sure if this works, it looks like this has to be something special for meteor to stop waiting
    if (!this.userId) {
        return;
    }
    return Modules.find({
        isActive: true,
        'ratings.userId': this.userId
    }, {
        fields: {
            'ratings.$': 1
        }
    });
});

Meteor.publish('semesterPlans', function() {
    return SemesterPlans.find({
        userId: this.userId
    });
});

Meteor.publish('fbPermission', function() {
    if (!this.userId) {
        return;
    }
    return FBPermission.find({
        userId: this.userId
    }, {
        fields: {
            'userId': 1,
            'facebookId': 1
        }
    });
});

Meteor.publish('fbInfo', function() {
    if (!this.userId) {
        return;
    }
    return FBInfo.find({
        userId: this.userId
        //userId: 'dcws2uR7DYvdnKY7A'
    }, {
        fields: {
            'facebookId': 1
        }
    });
});

Meteor.publish('experimentSubject', function() {
    if (!this.userId) {
        return;
    }
    return ExperimentSubject.find({
        userId: this.userId
        //userId: 'tqdNoPzqnPtvfYdTo'
        //matchPartnerCallName: 'Johannes Hool'   
    }, {
        fields: {
            'userId': 1,
            'completedRegistration': 1,
            'matchPartnerId': 1,
            'matchPartnerFbId': 1,
            'matchPartnerCallName': 1,
            'Task2Stage': 1,
            'algRec': 1,
            'humRec' : 1,
            'recommendationMatrix' : 1,
            'task3State': 1,
            'personalizedTreatment': 1,
            'humanListIsRight': 1
        }
    });
});

Meteor.publish('tutorialRecommendations', function() {
    if (!this.userId){
        return;
    }
    return TutorialRecommendations.find({userId: 'tutorial'});
});
Meteor.publish('algRecommendations', function() {
    if (!this.userId){
        return;
    }
    //var matchId = ExperimentSubject.findOne({userId: 'YJNDbL57gh7Df7gKx'}).matchPartnerId;
    var matchId = ExperimentSubject.findOne({userId: this.userId}).matchPartnerId;
    return AlgRecommendations.find({

        //userId: find matching id in ExperimentSubject Collection
        //userId: 'dcws2uR7DYvdnKY7A'
        userId: matchId
    });

});
