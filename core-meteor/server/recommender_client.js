RecommenderClient = (function() {
    var self = {};

    var ratingURL = 'http://localhost:8090/meteor/ratings';
    var notInterestedURL = 'http://localhost:8090/meteor/not-interested';

    self.exportRatings = function(ratings, onSuccess) {
        HTTP.post(ratingURL, {
            data: ratings
        }, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                console.log('status: ' + result.statusCode + ' - ' + result.content);
                if (!_.isUndefined(onSuccess)) {
                    onSuccess();
                }
            }
        });
    };

    self.exportNotInterested = function(notInterestedModules) {
        HTTP.post(notInterestedURL, {
            data: notInterestedModules
        }, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                console.log('status: ' + result.statusCode + ' - ' + result.content);
            }
        });
    };

    self.deleteRating = function(userId, moduleId) {
        check(userId, String);
        check(moduleId, String);

        var options = {
            meteorUserId: userId,
            meteorModuleId: moduleId,
            studyCourse: '',
            rating: 0,
            createdAt: 0
        };

        HTTP.call('DELETE', ratingURL, {
            data: options
        }, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                console.log('status: ' + result.statusCode + ' - ' + result.content);
            }
        });
    };

    self.receivePredictions = function(userId, predictionUrl, onReceived) {
        check(userId, String);
        var options = {
            'meteorUserId': userId
        };

        HTTP.get(predictionUrl, {
            params: options
        }, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                var data = _.uniq(result.data, function(item) {
                    return item.meteorModuleId;
                });
                onReceived(data);
            }
        });
    };

    self.exportAll = function() {
        var allRatings = [];
        Modules.find().forEach(function(module) {
            _.each(module.ratings, function(rating) {
                if (rating.rating !== null) {
                    var user = Meteor.users.findOne({
                        _id: rating.userId
                    });
                    if (_.isUndefined(user)) {
                        console.log(rating.userId);
                    }
                    var studyCourse = ((((user || {}).profile || {}).studyProfile || {}).current || {}).studyCourse;
                    allRatings.push({
                        meteorUserId: rating.userId,
                        meteorModuleId: module.moduleId,
                        studyCourse: studyCourse ? studyCourse : '',
                        rating: rating.rating,
                        createdAt: rating.createdAt.getTime()
                    });
                }
            });
        });

        var allNotInterested = [];
        Modules.find().forEach(function(module) {
            _.each(module.predictions, function(prediction) {
                if (prediction.isNotInterested) {
                    allNotInterested.push({
                        meteorUserId: prediction.userId,
                        meteorModuleId: module.moduleId
                    });
                }
            });
        });

        self.exportRatings(allRatings, function() {
            // export allNotInterested as soon all ratings have been exported
            self.exportNotInterested(allNotInterested);
        });
    };

    return self;
})();

Meteor.methods({

    exportRating: function(moduleId, rating, createdAt) {
        var currentUser = Meteor.user();
        var studyCourse = (((currentUser.profile || {}).studyProfile || {}).current || {}).studyCourse;
        var ratingObj = {
            meteorUserId: currentUser._id,
            meteorModuleId: moduleId,
            studyCourse: studyCourse ? studyCourse : '',
            rating: rating,
            createdAt: createdAt.getTime()
        };
        RecommenderClient.exportRatings([ratingObj]);
    },

    exportNotInterested: function(moduleId) {
        var notInterestedObj = {
            meteorUserId: Meteor.userId(),
            meteorModuleId: moduleId
        };
        RecommenderClient.exportNotInterested([notInterestedObj]);
    },

    deleteRatingFromRecommender: function(moduleId) {
        RecommenderClient.deleteRating(Meteor.userId(), moduleId);
    },

    updateTopPredictionsFromRecommender: function() {
        var updateTopPredictionsAsync = function(callback) {
            RecommenderClient.receivePredictions(Meteor.userId(), 'http://localhost:8090/predictions/primary', function(predictions) {
                PredictionController.updatePredictions(predictions.slice(0, 51));
                console.log('Top 50 predictions updated');
                callback();
            });
        };
        var updateTopPredictionsSync = Meteor.wrapAsync(function(callback) {
            updateTopPredictionsAsync(callback);
        });
        updateTopPredictionsSync();
    },

    updateAllPredictionsFromRecommender: function() {
        RecommenderClient.receivePredictions(Meteor.userId(), 'http://localhost:8090/predictions/primary', function(predictions) {
            var userId = Meteor.userId();
            console.log('updating all predictions for user ' + userId);
            var updatePredictions = function(startIndex, stopIndex) {
                Meteor.setTimeout(function() {
                    PredictionController.updatePredictions(predictions.slice(startIndex, stopIndex), userId);
                }, startIndex * 100);
            };
            for (var index = 0; index < predictions.length; index += 50) {
                updatePredictions(index, index + 50);
            }
            
        });
    },
    updateAllPredictionsFromRecommenderById: function(usrId) {
        var userId = usrId;
        RecommenderClient.receivePredictions(userId, 'http://localhost:8090/predictions/primary', function(predictions) {
            console.log('updating all predictions for user ' + userId);
            var updatePredictions = function(startIndex, stopIndex) {
                Meteor.setTimeout(function() {
                    PredictionController.updatePredictions(predictions.slice(startIndex, stopIndex), userId);
                }, startIndex * 100);
            };
            for (var index = 0; index < predictions.length; index += 50) {
                updatePredictions(index, index + 50);
            }
            console.log('Finished updating modules for user ' + userId);
        });
    }

});
