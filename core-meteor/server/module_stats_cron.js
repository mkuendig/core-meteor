ModuleStatsController = function() {
    var self = this;

    self.average = function(data) {
        if (!data.length) {
            return 0;
        }
        var sum = _.reduce(data, function(sum, value) {
            return sum + value;
        }, 0);

        return sum / data.length;
    };

    self.pluckRatings = function(module) {
        var ratings = _.pluck(module.ratings, 'rating');
        return _.filter(ratings, Boolean);
    };

    self.calcAverageRating = function(module) {
        var ratings = self.pluckRatings(module);
        if (ratings.length > 0) {
            var avgRating = self.average(ratings);
            return Math.round(avgRating * 10) / 10;
        } else {
            return 0;
        }
    };

    self.getNumberOfRatings = function(module) {
        var ratings = self.pluckRatings(module);
        return ratings.length;
    };


    /**
     * Calculates and updates the average rating of a module
     * @param module
     * @returns {number}
     */
    self.updateAverageRating = function(module) {
        var numberOfRatings = self.getNumberOfRatings(module);
        var avgRating = self.calcAverageRating(module);

        Modules.update({
            moduleId: module.moduleId
        }, {
            $set: {
                'stats.avgRating': avgRating,
                'stats.numberOfRatings': numberOfRatings
            }
        });
    };

    self.updateTrend = function(module) {
        var currentAvgRating = self.calcAverageRating(module);
        var prevAvgRating = 0;
        if (module.stats.prevAvgRating) {
            prevAvgRating = module.stats.prevAvgRating;
        }
        var trend = currentAvgRating - prevAvgRating;

        Modules.update({
            moduleId: module.moduleId
        }, {
            $set: {
                'stats.prevAvgRating': currentAvgRating,
                'stats.trend': trend
            }
        });
    };

    /**
     * Calculates and updates the average rating of all modules
     */
    self.updateAvgRatingOfAllModules = function() {
        Modules.find({
            isActive: true
        }).forEach(function(module) {
            self.updateAverageRating(module);
        });
    };

    /**
     * Calculates and updates the stats of all modules
     */
    self.updateTrendOfAllModules = function() {
        Modules.find({
            isActive: true
        }).forEach(function(module) {
            self.updateTrend(module);
        });
    };
};

var moduleStatsController = new ModuleStatsController();

SyncedCron.add({
    name: 'Average ratings calculator',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 30 minutes');
    },
    job: function() {
        moduleStatsController.updateAvgRatingOfAllModules();
    }
});

SyncedCron.add({
    name: 'Trend calculator',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('on the first day of the week');
    },
    job: function() {
        moduleStatsController.updateTrendOfAllModules();
    }
});