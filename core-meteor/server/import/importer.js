/*jshint camelcase: false */
Importer = function() {
    var self = this;
    self.categories = [];

    self.extractLecturers = function(lectures) {
        var moduleLecturers = [];
        _.filter(lectures, function(lecture) {
            _.each(lecture.scheduleItems, function(scheduleItem) {
                _.each(scheduleItem.lecturers, function(lecturer) {
                    var inModuleLecturers = _.find(moduleLecturers, function(moduleLecturer) {
                        if (moduleLecturer.uzhId === lecturer.uzhId) {
                            return true;
                        }
                    });
                    if (!inModuleLecturers) {
                        moduleLecturers.push(lecturer);
                    }
                });
            });
        });
        return moduleLecturers;
    };

    self.parseCategories = function(category) {
        _.each(category.fields, function(field) {
            if (!_.isNull(field) && !_.isNull(field.slug) && !_.isNull(field.module_type)) {
                if ((_.where(self.categories, {
                        slug: field.slug
                    })).length < 1) {
                    // check if self.categories already contains the field
                    self.categories.push({
                        title: field.title,
                        slug: field.slug,
                        moduleType: field.module_type
                    });
                }
            }
        });
        _.each(category.parents, self.parseCategories);
    };

    self.createOrUpdateModules = function(modules) {
        // first deactivate all modules
        Modules.update({}, {
            $set: {
                'isActive': false
            }
        }, {
            multi: true
        });


        for (var i = 0; i < modules.length; i += 1) {
            var moduleJSON = modules[i];

            // parse categories of module
            self.categories = [];
            _.each(moduleJSON.categories, self.parseCategories);

            var module = Modules.findOne({
                moduleId: moduleJSON.moduleId
            });

            // set all modules to inactive --> only new imported modules will be active
            Modules.update({}, {
                $set: {
                    isActive: false
                }
            });
            // check if module exists - if not, create a new one
            if (module) {
                module.lectures = moduleJSON.lectures;
                module.uzhUrl = moduleJSON.uzhUrl;
                module.title = moduleJSON.title;
                module.semester = moduleJSON.semester;
                module.categories = self.categories;
                module.description = moduleJSON.description;
                module.ects = parseFloat(moduleJSON.ects);
                module.language = moduleJSON.language;
                module.bookDeadline = moduleJSON.bookDeadline;
                module.cancelDeadline = moduleJSON.cancelDeadline;
                module.personInCharge = moduleJSON.personInCharge;
                module.organizationalUnits = moduleJSON.organizationalUnits;
                module.lectures = moduleJSON.lectures;
                module.lecturers = self.extractLecturers(moduleJSON.lectures);
                module.isActive = true;
                Modules.remove(module._id);
                Modules.insert(module);
                console.log('Updated Module ' + moduleJSON.moduleId);
            } else { // insert new semester
                Modules.insert({
                    moduleId: moduleJSON.moduleId,
                    uzhUrl: moduleJSON.uzhUrl,
                    title: moduleJSON.title,
                    semester: moduleJSON.semester,
                    categories: self.categories,
                    description: moduleJSON.description,
                    ects: parseFloat(moduleJSON.ects),
                    language: moduleJSON.language,
                    bookDeadline: moduleJSON.bookDeadline,
                    cancelDeadline: moduleJSON.cancelDeadline,
                    personInCharge: moduleJSON.personInCharge,
                    organizationalUnits: moduleJSON.organizationalUnits,
                    lectures: moduleJSON.lectures,
                    lecturers: self.extractLecturers(moduleJSON.lectures),
                    isActive: true
                });
                _.each(moduleJSON.organizationalUnits, OrganizationalUnits.createOrUpdate);
                console.log('Inserted Module ' + moduleJSON.moduleId);
            }
        }
    };

    self.updateCategory = function(category, childSlug) {
        Categories.update({
            slug: category.slug
        }, {
            $set: {
                title: category.title,
                moduleType: category.module_type
            },
            $addToSet: {
                children: childSlug
            }
        });
    };

    self.updateChild = function(category, childSlug) {
        if (!_.isNull(category.slug)) {
            if (_.isUndefined(Categories.findOne({
                    slug: category.slug
                }))) {
                Categories.insert({
                    title: category.title,
                    slug: category.slug,
                    moduleType: category.module_type
                }, function() {
                    self.updateCategory(category, childSlug);
                });
            } else {
                self.updateCategory(category, childSlug);
            }
        }
        _.each(category.parents, function(parent) {
            self.updateChild(parent, category.slug);
        });
    };

    self.createOrUpdateCategories = function(categories) {
        Categories.remove({});
        _.each(categories, function(category) {
            if (_.isUndefined(Categories.findOne({
                    slug: category.slug
                }))) {
                Categories.insert({
                    title: category.title,
                    slug: category.slug,
                    moduleType: category.module_type
                }, function() {
                    _.each(category.parents, function(parent) {
                        self.updateChild(parent, category.slug);
                    });
                });
            }
            console.log('Inserted category ' + category.title);
        });
    };

    self.createOrUpdatePersons = function(persons) {
        for (var i = 0; i < persons.length; i += 1) {
            var personJSON = persons[i];
            if (Persons.findOne({
                    uzhId: personJSON.uzhId
                })) {
                Persons.update({
                    uzhId: personJSON.uzhId
                }, {
                    $set: {
                        firstname: personJSON.firstname,
                        lastname: personJSON.lastname,
                        position: personJSON.position
                    }
                });
                console.log('Updated Person ' + personJSON.uzhId);
            } else {
                Persons.insert(personJSON);
                console.log('Inserted new Person: ' + personJSON.uzhId);
            }
        }
    };

    self.createOrUpdateRooms = function(rooms) {
        for (var i = 0; i < rooms.length; i += 1) {
            var roomJSON = rooms[i];
            if (Rooms.findOne({
                    uzhId: roomJSON.uzhId
                })) {
                Rooms.update({
                    uzhId: roomJSON.uzhId
                }, {
                    $set: {
                        name: roomJSON.name,
                        street: roomJSON.street,
                        address: roomJSON.address
                    }
                });
                console.log('Updated Room ' + roomJSON.uzhId);
            } else {
                Rooms.insert(roomJSON);
                console.log('Inserted new Room: ' + roomJSON.uzhId);
            }
        }
    };

    self.calculateWeekdays = function() {
        var total = 0;
        Modules.find().forEach(function(module) {
            var weekdays = [];
            _.each(module.lectures, function(lecture) {
                _.each(lecture.scheduleItems, function(scheduleItem) {
                    var date = new Date(scheduleItem.startDatetime);
                    weekdays = _.union(weekdays, [date.getDay()]); // add value if not already in array
                });
            });
            Modules.update({
                moduleId: module.moduleId
            }, {
                $set: {
                    weekdays: weekdays
                }
            });
            console.log('updated weekdays field - ' + module.moduleId);
            total += 1;
        });
        console.log('finished - updated ' + total + ' module weekdays');
    };

    self.receiveDataFromCrawler = function(data) {
        var jsonData = JSON.parse(data);

        if (PROD() && jsonData.password !== 'fzvMojS7ug1xrZ%!vbayFawd!je@YH') {
            throw new Meteor.Error('Unauthorized');
        }

        console.log('Receiving data from django');
        console.log('Password correct');

        if (jsonData.persons) {
            self.createOrUpdatePersons(jsonData.persons);
        }
        if (jsonData.rooms) {
            self.createOrUpdateRooms(jsonData.rooms);
        }
        if (jsonData.categories) {
            self.createOrUpdateCategories(jsonData.categories);
        }
        if (jsonData.modules) {
            self.createOrUpdateModules(jsonData.modules);
            self.calculateWeekdays();
        }

        return 'Data received';
    };

};

Meteor.methods({
    receiveDataFromCrawler: function(data) {
        var importer = new Importer();
        importer.receiveDataFromCrawler(data);
    }
});