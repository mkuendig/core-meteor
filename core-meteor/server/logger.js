var LOG_DIRECTORY = '/home/coreadm/logs/';

Meteor.methods({
    logEvent: function(type, message) {
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'events.log', new Date().toString() + ' - ' + type + ': ' + message + '\n');
        } else {
            console.log(new Date().toString() + ' - ' + type + ': ' + message);
        }
    },

    logRating: function(type, userId, moduleId, date, rating) {
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'ratings.log', date + ' - User: ' + userId + ' - Module: ' + moduleId + ' - ' + type + ': ' + rating + '\n');
        } else {
            console.log(date + ' - User: ' + userId + ' - Module: ' + moduleId + ' - ' + type + ': ' + rating);
        }
    },

    logComment: function(type, userId, moduleId, date, comment) {
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'comments.log', date + ' - User: ' + userId + ' - Module: ' + moduleId + ' - ' + type + ': ' + comment + '\n');
        } else {
            console.log(date + ' - User: ' + userId + ' - Module: ' + moduleId + ' - ' + type + ': ' + comment);
        }
    },

    logImportMethod: function(message, userId, date) {
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'importMethod.log', date + ' - User: ' + userId + ' - ' + message + '\n');
        } else {
            console.log(date + ' - User: ' + userId + ' - ' + message);
        }
    },
    logFBInfoAdded: function(type, userId, date) {
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'FBInfo.log', date + ' - User: ' + userId + ' facebook info added\n');
        } else {
            console.log(date + ' - User: ' + userId + ' facebook info added');
        }
    },
    logConnectingAAIFB: function(type, userId, date) {
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'FBAAI.log', date + ' - User: ' + userId + ' AAI and FB connected\n');
        } else {
            console.log(date + ' - User: ' + userId + ' AAI and FB connected');
        }
    },
    registrationStart: function(id, account){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'Registration.log', date.toString() + '\tUser:\t' + id + '\tlogin with\t' + account +'\n');
        } else {
            console.log( date.toString() + '\tUser:\t' + id + '\tlogin with\t' + account +'\n');
        }
    },
    registrationFbUpdate: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'Registration.log', date.toString() + '\tUser:\t' + id + '\tfacebook update\n');
        } else {
            console.log( date.toString() + '\tUser:\t' + id + '\tfacebook update\n');
        }
    },
    registrationComplete: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'Registration.log', date.toString() + '\tUser:\t' + id + '\tregistration complete\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tregistration complete\n');
        }
    },
    logLogout: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'Registration.log', date.toString() + '\tUser:\t' + id + '\tlogout\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tlogout\n');
        }

    },
    task2Login: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\tLogin\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tLogin\n');
        }
    },
    task2TutorialStart: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\ttutorial start\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\ttutorial start\n');
        }
    },
    task2RecommendStart: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\trecommend start\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\trecommend start\n');
        }
    },
    task2SurveyOneStart: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\tsurvey one start\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tsurvey one start\n');
        }
    },
    task2SurveyTwoStart: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\tsurvey two start\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tsurvey two start\n');
        }
    },
    task2CompleteStart: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\tcomplete\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tcomplete\n');
        }
    },
    task2Logout: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\tlogout\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tlogout\n');
        }
    },
    task2NotAccepted: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task2.log', date.toString() + '\tUser:\t' + id + '\tnot accepted\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tnot accepted\n');
        }
    },
    task3NotAccepted: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tnot accepted\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tnot accepted\n');
        }        
    },
    task3Login: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tlogin\n');
            console.log(date.toString() + '\tUser:\t' + id + '\tlogin\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tlogin\n');
        }        
    },
    task3StartSurvey1: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tstart survey 1\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tstart survey 1\n');
        }        
    },
    task3StartSurvey2: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tstart survey 2\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tstart survey 2\n');
        }        
    },
    task3StartSurvey3: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tstart survey 3\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tstart survey 3\n');
        }        
    },
    task3StartSurvey4: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tstart survey 4\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tstart survey 4\n');
        }        
    },
    task3Complete: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tcomplete\n');
            console.log(date.toString() + '\tUser:\t' + id + '\tcomplete\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tcomplete\n');
        }        
    },
    task3Logout: function(id){
        var date = new Date();
        if (PROD()) {
            var fs = Npm.require('fs');
            fs.appendFile(LOG_DIRECTORY + 'task3.log', date.toString() + '\tUser:\t' + id + '\tlogout\n');
        } else {
            console.log(date.toString() + '\tUser:\t' + id + '\tlogout\n');
        }        
    }
});