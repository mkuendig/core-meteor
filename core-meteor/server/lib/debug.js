DEBUG = function () {
    return _.isUndefined(process.env.PRODUCTION);
};

PROD = function () {
    return !_.isUndefined(process.env.BRANCH) && process.env.BRANCH === 'prod';
};