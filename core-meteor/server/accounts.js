Meteor.methods({
    'createLogin': function(shibbolethId, email, language, name) {
        var hashedUsername = CryptoJS.SHA256(shibbolethId).toString();
        var user = Meteor.users.findOne({
            'username': hashedUsername
        });

        //If no user exists with this id yet, we create one
        if (typeof user === 'undefined') {
            var createdUser = Accounts.createUser({
                username: hashedUsername,
                email: email,
                password: shibbolethId,
                profile: {
                    bsc: {
                        name: 'Bachelor',
                        regulations: []
                    },
                    msc: {
                        name: 'Master',
                        regulations: []
                    },
                    callName: name,
                    preferredLanguage: language,
                    userAgreement: null
                }
            });
            Meteor.call('logEvent', 'INFO', 'Created new user: ' + createdUser);
        }
    },
    'addCallName': function(name){
        Meteor.users.update(this.userId, {$set: {'profile.callName': name}});
    },

    'hasRatedEnoughModules': function() {
        var ratedModulesCount = 0;
        Modules.find({
            isActive: true,
            'ratings.userId': this.userId
        }, {
            fields: {
                'ratings.$': 1
            }
        }).forEach(function(module) {
            var userRating = module.ratings[0];
            if (userRating && userRating.rating > 0) {
                ratedModulesCount += 1;
            }
        });
        return ratedModulesCount > 3;
    }
});


if (!PROD()) {
    Meteor.methods({
        'createDummyLogin': function() {
            var id = Random.id(20);
            var dummyMail = id + 'Mail@uzh.ch';
            var dummyName = id + ' ' + 'lastname';
            Accounts.createUser({
                username: id,
                email: dummyMail,
                password: id,
                profile: {
                    bsc: {
                        name: 'Bachelor',
                        regulations: []
                    },
                    msc: {
                        name: 'Master',
                        regulations: []
                    },
                    callName: dummyName,
                    preferredLanguage: 'de',
                    userAgreement: null
                }
            });
            console.log(id);
            return id
        },

        'deleteUser': function() {
            Meteor.users.allow({
                remove: function() {
                    return true;
                }
            });
            var userId = Meteor.userId();
            // remove all user ratings
            Modules.update({}, {
                $pull: {
                    ratings: {
                        userId: userId
                    }
                }
            }, {
                multi: true
            });
            // remove user
            Meteor.users.remove({
                _id: userId
            });

            FBInfo.remove({
                userId: userId
            });

            FBPermission.remove({
                userId: userId
            });
            ExperimentSubject.remove({
                userId: userId
            });

            console.log('Deleted user ' + userId);
        },

        'deleteFBInformation': function() {
            var userId = Meteor.userId();

            FBInfo.remove({
                userId: userId
            });

            FBPermission.remove({
                userId: userId
            });

            console.log('Deleted FB information for user ' + userId);
        },

        'deleteAllFBInformation': function() {
            FBPermission.remove({});
            FBInfo.remove({});
            console.log('Deleted all FB information');
        }
    });
}