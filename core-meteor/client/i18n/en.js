i18n.map('en', {
    /*
     ****** General ******
     */
    'prediction': 'Our best guess for ',
    'predictionShort': 'Prediction',
    'title': 'Title',
    'lecturer': 'Lecturer',
    'lectures': 'Courses',
    'ects': 'ECTS',
    'vvz': 'Course Catalog',
    'faculty': 'Faculty',
    'studyCourse': 'Program',
    'level': 'Degree',
    'field': 'Focus',
    'semester': 'Semester ',
    'recommendations': 'Recommendations',
    'noRecommendations': 'Unfortunately there are no recommendations available yet. Please <a href="/profile/module-rating">rate</a> more courses.',
    'calculatingRecommendations': 'Recommendations are being recomputed...',
    'notInterested': 'Not interested',
    'markedAsNotInterested': 'Course will not be recommended in the future',
    'home': 'Home',
    'modules': 'Modules',
    'rooms': 'Rooms',
    'userId': 'User ID',
    'moduleId': 'Module code',
    'type': 'Type',
    'semesterPlanning': 'Semester Plan',
    'studyPlanning': 'Study Plan',
    'overview': 'Overview',
    'help': 'Help',
    'profile': 'Profile',
    'about': 'About',
    'faq': 'FAQ',
    'settings': 'Settings',
    'logout': 'Logout',
    'contact': 'Contact',
    'create': 'Create',
    'save': 'Save',
    'saved': 'Your changes have been saved',
    'finish': 'Fertig',
    'sortBy': 'Sort by',
    'back': 'Back',
    'addMoreModules' : 'Add more modules',
    'next': 'Next',
    'remove': 'Remove',
    'edit': 'Edit',
    'yes': 'Yes',
    'no': 'No',
    'more': 'Further information...',
    'less': 'Close',
    'cancel': 'Cancel',
    'weekday': 'Weekday',
    'bestRated': 'Top rated',
    'mostRecent': 'Newest',
    'pageNotFound': 'Page not found',
    'inputTooShort': 'Input is too short',
    'selectedLectures': 'Selected sub-modules',
    'selectModules': 'Choose modules',
    'icsExport': 'Export (ICS)',
    'total': 'Total',
    'passed': 'Passed',
    'bachelor': 'Bachelor',
    'master': 'Master',
    'noModulesPlanned': 'There are no modules planned yet.',
    'bsc': 'Bachelor',
    'msc': 'Master',
    'select': 'Choose',
    'regulation': 'Area',
    'your': 'Your',
    'you': 'you',
    'study': 'Studies',
    'createNewRegulation': 'Create new area',
    'createNewSelfDefinedModule': 'Define your own module',
    'addModule': 'Add module',
    'addModules': 'Add my modules',
    'editRegulation': 'Edit area',
    'removeRegulation': 'Remove area',
    'editModule': 'Edit module',
    'removeModule': 'Remove module',
    'name': 'Label',
    'regulationNameExample': 'e.g. mandatory modules',
    'moduleNameExample': 'e.g. Bachelor thesis',
    'planYourBscStudy': 'Study plan for Bachelor',
    'planYourMscStudy': 'Study plan for Master',
    'showMore': 'Show more',
    'showLessModules': 'Show fewer recommendations',
    'showMoreModules': 'Show more recommendations',
    'studyPlanIntroduction': 'Here, you can create your personal study plan. Create different areas (e.g. mandatory modules) to map the rules of your study regulation. You can then add the modules that you have attended to these areas. If you cannot find a module in CoRe (e.g. Bachelor Thesis), you can define it by yourself and add it to a areas.',
    'studyPlanTooltip': 'Example of a study plan',
    'example': 'Example picture',
    'studyPlanTooltip2': 'e.g. mandatory modules',
    'hs': 'Fall semester',
    'fs': 'Spring semester',
    'copyRight': '&copy; 2015 <a href="http://www.ifi.uzh.ch/ce.html", target="_blank">Computation & Economics Research Group</a> - University of Zurich',
    'acceptButton': 'I accept',
    'acceptButtonOptional': 'I accept (optional)',
    'denyButton': 'I decline',

    /*
     ******** Toast *******
     */
    'user_module_relations_toggle': 'Entry updated',
    'user_module_relations_rating_added': 'Rating saved',
    'user_module_relations_rating_deleted': 'Rating removed',
    'user_module_relations_selection_added': 'Module saved',
    'user_module_relations_selection_deleted': 'Module removed',


    /*
     ******** Modul-Types *******
     */
    'bwl_bsc_economics': 'Business Administration, Bachelor',
    'vwl_bsc_economics': 'Economics, Bachelor',
    'bf_bsc_economics': 'Banking & Finance, Bachelor',
    'me_bsc_economics': 'Management & Economics, Bachelor',
    'wi_bsc_informatics': 'Information Systems, Bachelor',
    'ss_bsc_informatics': 'Software Systems, Bachelor',
    'ani_bsc_informatics': 'Neuroinformatics, Bachelor',
    'abi_bsc_informatics': 'Bioinformatics, Bachelor',
    'agi_bsc_informatics': 'Geoinformatics, Bachelor',
    'ami_bsc_informatics': 'Mediainformatics, Bachelor',
    'aci_bsc_informatics': 'Computational Linguistics, Bachelor',
    'bwl_msc_economics': 'Business Administration, Master',
    'vwl_msc_economics': 'Economics, Master',
    'bf_msc_economics': 'Banking & Finance, Master',
    'me_msc_economics': 'Management & Economics, Master',
    'wi_msc_informatics': 'Information Systems, Master',
    'ss_msc_informatics': 'Software Systems, Master',
    'cs_msc_informatics': 'Multimodale & Cognitive Systems, Master',


    /*
     ******* Semesters ********
     */
    'fs11': 'Spring Semester 2011',
    'hs11': 'Fall Semester 2011',
    'fs12': 'Spring Semester 2012',
    'hs12': 'Fall Semester 2012',
    'fs13': 'Spring Semester 2013',
    'hs13': 'Fall Semester 2013',
    'fs14': 'Spring Semester 2014',
    'hs14': 'Fall Semester 2014',
    'fs15': 'Spring Semester 2015',
    'hs15': 'Fall Semester 2015',
    'fs16': 'Spring Semester 2016',
    'hs16': 'Fall Semester 2016',
    'fs17': 'Spring Semester 2017',
    'planYour': 'Plan your',

    /*
     ****** Home ******
     */
    'homeTabTitle': 'Your top recommendations:',
    'notifications': 'Notifications',
    'bestRatedModulesWWF': 'Top rated modules of the Faculty of Economics',
    'bestRatedModulesIFI': 'Top rated modules at the Department of Informatics',
    'bestRatedModulesBWL': 'Top rated modules at the Department of Business Administration',
    'bestRatedModulesVWL': 'Top rated modules at the Department of Economics',
    'bestRatedModulesBF': 'Top rated modules at the Department of Banking and Finance',
    'trendingModules': 'Most improved modules of last week',
    'ratingScale': 'Rating progress',
    'openModuleRating': 'Rate modules',
    'openPlanner': 'Plan your',
    'warningOnlyWWF': 'Please note: The recommendations made by CoRe are currently only optimized for <strong>students of the Faculty of Business, Economics and Informatics</strong> of the University of Zurich.',
    'loginInfo': 'Use Facebook to log in to CoRe',
    'alternativeLoginButton': 'Log in without Facebook',
    'fbLoginButton': 'Log in with Facebook',
    'aaiWithFBWelcomePre': 'Hello',
    'aaiWithFBWelcome': 'please confirm your student status with AAI (only once per semester). Only students from UZH can get access to CoRe.',
    'aaiWelcome': 'Use AAI to log in to CoRe. Only students from the University of Zurich can get access to CoRe.',
    'optimizedForAllStudent': 'For the fall semester of 2016, the recommendations will be optimized for all students of the Faculty of Economics.',
    'missingSocialNetwork': 'Missing social network information!',
    'facebookReminderHeaderTitleHome': 'Social network information',

    /*
     ****** Facebook Reminder
     */
    'facebookReminderHeaderTitle': 'Are you sure?',
    'facebookReminderText': 'If you log in with Facebook we can improve your recommendations based on your social network!',
    'facebookReminderReassurance': 'If you have used CoRe before, then we will automatically import your account settings from previous semesters',
    'sayNoToFb': 'No, thanks',

    /*
     ****** Semesterplan ******
     */
    'titlePlannedModules': 'Your planned modules',
    'titleSchedule': 'Your timetable',
    'titleSelectModulesPlanner': 'List of all modules',
    'titleRecommendationsPlanner': 'Your top recommendations',
    'mandatoryModules': 'Mandatory Modules',
    'recommendationsFor': 'Recommendations for ',
    'newSemester': 'Plan new semester',
    'moduleRemovedFromSemester': 'Module removed',
    'moduleAddedToSemester': 'Module added',
    'textSemesterPlanCalendar': 'You can add and remove courses. If a course has several exercise sections, you can choose your preferred exercise section from the <strong>Selected sub-modules</strong> column below.',
    'placeHolderLecture': 'Please choose a lesson',

    /*
     ****** Studiumsplanner ******
     */
    'tableOverview': 'Spreadsheet view',
    'csvExport': 'Export (CSV)',
    'studyOverviewFileName': 'Study overview',
    'yourRegulation': 'Your areas',
    'numberOfECTS': 'Number of ECTS Credits',
    'unassigned': 'Unassigned',

    /*
     ****** Meine Module ******
     */
    'rateModules': 'Rate modules',
    'moduleRatingTitle': 'Rate modules',
    'titleModuleRatingLeft': 'Ratings',
    'titleModuleRatingRight': 'Assigning modules to areas',
    'textModuleRatingLeft': 'Please rate your attended modules with the <strong>stars</strong>.',
    'textModuleRatingRight': 'Once you have created areas (e.g. mandatory modules) for your <a href="/study-plan/bsc">Bachelor</a> or <a href="/study-plan/msc">Master study</a>, you can move the ẗitles of the modules with your mouse via <strong>drag and drop</strong> into the corresponding areas.',

    /*
     ****** Modules ******
     */
    'textmodules': 'Here, you find all modules that are available to the students of the Faculty of Economics. You can sort the list and use filters to refine it.',
    'browseModules': 'Overview of all modules',

    /*
     ****** Details zu den Modulen ******
     */
    'information': 'Information',
    'removeComment': 'Remove comment',
    'writeComment': 'Write a comment',
    'comments': 'Comments',
    'commonInformation': 'General information',
    'moduleno': 'Module code',
    'ectsP': 'ECTS',
    'description': 'General description',
    'precognition': 'Prerequisites',
    'requirements': 'Requirements',
    'teachingMaterials': 'Teaching materials',
    'goals': 'Learning goals',
    'targetAudience': 'Target audience',
    'exam': 'Performance assessment',
    'gradingScale': 'Grading scale',
    'repeatability': 'Repeatability',
    'language': 'Language',
    'date': 'Date',
    'lecturers': 'Lecturers',
    'room': 'Room',
    'editComment': 'Edit comment',
    'openvvz': 'Open course catalog',
    'noComments': 'There are no comments yet',
    'notInCurrentSemester': 'This module is not available in your current semester',
    'alreadyInSemesterPlan': 'This module is planned for your',
    'notInSemesterPlan': 'Add this module to your semester plan',
    'addToSemesterPlan': 'Add to',
    'removeFromSemesterPlan': 'Remove from your semester plan',
    'notInStudyPlan': 'This module is not part of your study plan',
    'addToStudyPlan': 'Add to study plan',
    'removeFromStudyPlan': 'Remove from study plan',
    'showAll': 'Show all',

    /*
     ****** Impressum ******
     */
    'projectInfo': 'About the project',
    'textProjectInfo': '<strong>CoRe</strong> stands for <strong>Course Recommender</strong> and is a web-based recommendation system for courses, which helps you to plan your studies. CoRe is being developed and continuously improved by the <a href="http://www.ifi.uzh.ch/ce.html", target="_blank">Computation & Economics Research Group</a> at the <a href="http://www.ifi.uzh.ch", target="_blank">Department of Informatics</a> (IFI) at the <a href="http://www.uzh.ch", target="_blank">University of Zurich</a> (UZH)',
    'projectTeamTitle': 'Team',
    'projectLead': '<strong>Project lead</strong>',
    'projectSeuken': '<a href="http://www.ifi.uzh.ch/ce/people/seuken.html", target="_blank">Prof. Dr. Sven Seuken</a>',
    'projectShann': '<a href="http://www.ifi.uzh.ch/ce/people/shann.html", target="_blank">Mike Shann</a>',
    'projectMennle': '<a href="http://www.ifi.uzh.ch/ce/people/mennle.html", target="_blank">Timo Mennle</a>',
    'projectTeam': '<strong>Projekt team</strong>',
    'projectHee': 'Andras Hee',
    'projectPhilipp': 'Basil Philipp',
    'projectHediger': 'Jessica Hediger',
    'projectHool': 'Johannes Hool',
    'projectKuendig': 'Michael Kündig',
    'projectBadran': 'Zyad Badran',
    'textContact': 'If you have any questions or suggestions, please write an email to <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>',
    'disclaimer': 'Disclaimer',
    'textDisclaimer': 'CoRe is an additional service that does not replace the module booking, the semester enrollment, the transcript, or any other service of the UZH. All modules listed in CoRe are based on the official UZH course catalog. However, there is no guarantee that the displayed information is complete and up-to-date. For the official information, please see the UZH course catalog.',

    /*
     ****** Wizard ******
     */
    'continue': 'Continue',
    'manualSelectionTitle': 'Add your modules manually',
    'manualSelectionButton': 'Add modules manually',
    'copyRecordButton': 'Copy your transcript',
    '1-3min': '1-3 min',
    '5-10min': '5-10 min',
    'minSelection': 'at least',
    'minSelectionModulePlural': 'more',
    'minSelectionModuleSingular': 'more',
    'minRating': 'at least',
    'minRatingModulePlural': 'more',
    'minRatingModuleSingular': 'more',

    /*
     ****** Tooltips ******
     */
    'toolTipRating': 'Rate modules based on your overall impression',
    'tooltipSelectAll': 'Select all',
    'tooltipUnselectAll': 'Unselect all',
    'tooltipLanguage': 'Language',
    'exportTooltip': 'If you are using Safari, you have to save the file which opens in a new tab, in *.ics format.',
    'multipleLecturesTooltip': 'This course has several exercise sections. Please choose one exercise lesson.',
    'multipleLecturesInfo': 'In the following table you can select and deselect sub-modules that belong to a module. If a module has several exercise sections, you can choose your preferred exercise section from the <strong>Selected sub-modules</strong> column below.',
    'tootltipStars': 'Click, to rate the module',
    'tooltipDelete': 'Delete module selection',
    'tooltipAdd': 'Add module',
    'tooltipSettings': 'Edit area',
    'tooltipDeleteRegulation': 'Remove area',
    'tooltipEdit': 'Edit',
    'tooltipDeleteSelection': 'Remove',
    'tooltipSelectCourses': 'Edit the displayed sub-modules',
    'tooltipModuleRated': 'Rated module',
    'tooltipNotInterested': 'Remove the module from your recommendations',
    'noPrediction': 'Not yet enough data to compute a guess',

    /*
     ****** Wizard steps******
     */
    'selectModuleSelectionMethod': 'Options for adding modules',
    'moduleSelectionStep': 'Add my modules',
    'rateModulesStep': 'Rate modules',

    /*
     ****** Welcome und AAI Page ******
     */
    'welcomeAAI': 'Study about Course Recommendation Systems',
    'introductionWelcomePage': 'We first need some information regarding your studies such that CoRe is able to provide you with some initial recommendations.<br><br>Required information:',
    'welcomePageSubmitButton': 'Accept Participation Agreement',
    'userAgreementAcceptCheckbox': 'I understand and accept the <a id="showUserAgreement">user agreement</a>.',
    'networkUserAgreementAcceptCheckbox': 'I understand and accept the <a id="showNetworkUserAgreement">social network information usage agreement</a> (optional). If you accept, we can improve your recommendations based on your social network information!',

    'step1': 'Your program',
    'step2': 'The modules you have already completed',
    'step3': 'Your ratings of the modules you have completed',

    // features of CoRe
    'featureList': 'CoRe offers you the following features:',
    'feature01': 'Find personalized course recommendations',
    'feature02': 'Plan your future semesters',
    'feature03': 'Efficiently search for modules',
    'feature04': 'Create your timetable',
    'feature05': 'Rate courses',
    'feature06': 'Comment on courses',
    'feature07': 'Create your study overview',

    // IE warning
    'ieWarning': 'Warning: CoRe does not support Internet Explorer. Please use Chrome, Firefox or Safari.',

    /*
     ****** Study Profile Wizard ******
     */
    'studyProfile': 'Select your study profile',
    'studyProfileTitle': 'Study Profile',
    'currentStudyProfile': 'Please select your current program',
    'studyProfileFaculty': 'Faculty',
    'studyProfileStudyCourse': 'Program',
    'studyProfileLevel': 'Degree',
    'studyProfileField': 'Focus Area',

    /*
     ****** Module Import Method Wizard ******
     */
    'importMethodPrompt': 'Add the modules that you have already attended. To do so, select one of the two options below.',
    'copyRecordArguments': 'Automatically add modules by copying and pasting your transcript. We will ignore your grades and only identify the modules you have taken.',
    'manualSelectionArguments': 'Manually add the modules that you have taken from lists. The time you need this option depends on the number of modules you have taken.',

    /*
     ****** Module Import Method Wizard ******
     */
    'importMethodProfile': 'There are two different methods for adding your modules.',

    /*
     ****** Module Rating Wizard ******
     */
    'ratingInstructionWizard': 'Please rate as many modules as possible by using the stars.',
    'endRatings': 'Finish rating modules',
    'endRegistration': 'Finish rating modules & submit registration',

    /*
     ****** Module Table Template ******
     */
    'search': 'Search',
    'organizationalUnit': 'Organizational unit',
    'rating': 'Rating',
    'avgRating': 'Average',
    'ratings': 'Ratings',
    'notEnoughRatings': 'Unfortunately, there are no recommendations available yet',
    'infoRating': 'Rate the module using the stars',
    'page': 'page',
    'perPage': 'per page',
    'of': 'of',
    'itemText': 'Module',
    'itemsText': 'Modules',
    'allLabel': '______________',
    'moduleType': 'Module type',
    'yourRating': 'Your rating: ',

    // Calendar
    'Sunday': 'Sunday',
    'Monday': 'Monday',
    'Tuesday': 'Tuesday',
    'Wednesday': 'Wednesday',
    'Thursday': 'Thursday',
    'Friday': 'Friday',
    'Saturday': 'Saturday',

    /*
     ****** Module Import Wizard ******
     */
    'insertAttendedModules': 'Copy your transcript',
    'changeToManualSelectionButton': 'Add modules manually',

    /*
     ****** Module Import ******
     */
    'copyPastePromptShortcutKeys': '<ol><li>Open <a href="http://www.students.uzh.ch/en/record.html", target="blank"><strong>transcript</strong></a></li><li>To mark everything: In Windows use Ctrl+A; on Mac use ⌘A</li><li>To copy to clipboard: In Windows use Ctrl+C; on Mac use ⌘C</li><li>To paste the text: In Windows use Ctrl+V; on Mac use ⌘V</li></ol><strong>Note: Grades will be ignored and are not stored.</strong>',
    'furtherInformationText': 'CoRe processes your transcript and stores a list of the modules you attended. This method saves you the manual selection of the modules you have attended. It is possible that not all modules are listed in the database and that therefore your list will be incomplete.<br>If you do not want to paste your transcript into CoRe, you can switch to the manual selection of modules.<br>Modules that you have already imported and added will be ignored when you import your transcript again.',
    'insertArea': 'Please paste your transcript here',
    'previewRecognisedModules': 'The following modules have been found and added:',
    'previewUnknownModules': 'The following modules could not be identified and will be ignored:',
    'copy': 'Copy',
    'toRecord': 'Open your transcript',
    'nextModuleImport': 'Next (rate modules)',
    'warningImportTablet': '<strong>Attention:</strong> The import does not work on tablets and mobile phones.',

    /*
     ****** Module Selection Wizard ******
     */
    'moduleSelectionTitle': 'Add my modules',
    'moduleSelectionPrompt': 'Please add all modules that you have taken by clicking the \'+\'-sign. You can always switch back to the automatic import of your transcript.',
    'endSelection': 'Finish adding modules',
    'allModules': 'All modules',
    'backToImportSelection': 'Back to adding options',

    /*
     ****** Module Selection Profile ******
     */
    'moduleSelectionProfile': 'Please select the modules that you have attended from the list.',

    /*
     ****** Navigation ******
     */
    'navAddModule': 'Add my modules',
    'navRateModules': 'Rate modules',
    'navChangeStudyProfile': 'Change your study profile',
    'restartWizard': 'Restart set-up wizard',
    'navbarWarning': 'Social network',

    /*
     ****** Select2 Translation ******
     */
    'select2NoMatches': 'No matches found',
    'select2MoreCharacters': 'Please enter more characters',
    'select2LessCharacters': 'Please enter fewer characters',
    'footer_uzh_ifi_logo': '/images/uzh_ifi_logo/uzh_ifi_logo_e_pos.png',

    /*
     ****** FAQ ******
     */
    'faqHeaderTitle': 'Frequently Asked Questions',
    'faq01Title': 'How does CoRe compute my course recommendations?',
    'faq01Text': 'CoRe recommends courses based on your ratings and the ratings of all other students who are already using CoRe. It recommends courses to you which other students who have similar interests as you have already rated highly. The more ratings you provide and the more students are using CoRe, the better the recommendations get over time. The algorithm implemented in CoRe is a variant of <a href="https://en.wikipedia.org/wiki/Collaborative_filtering", target="_blank">Collaborative Filtering</a>.',
    'faq02Title': 'How often does CoRe compute my recommendations?',
    'faq02Text': 'Every time you rate a course your recommendations are re-computed. Furthermore, at every login the recommendations are re-computed.',
    'faq03Title': 'Do other users see my ratings?',
    'faq03Text': 'No. You are the only one who sees your ratings.',
    'faq04Title': 'Are the comments of the courses anonymous?',
    'faq04Text': 'Yes. The comments, which can be written in the course detail view are posted anonymously. Only the CoRe administrators are able to trace back their authors. When you write a comment, please follow the rules which are stated in the user agreement.',
    'faq05Title': 'Do professors have access to CoRe?',
    'faq05Text': 'No. Professors do not have access to CoRe. Even the project lead Prof. Dr. Sven Seuken does not have direct access to CoRe, and he will only be able to analyze the data after it has been anonymized.',
    'faq06Title': 'Who has access to CoRe?',
    'faq06Text': 'Currently, all students of the University of Zurich have access to CoRe. Student status is verified via a Switch-AAI login once per semester. CoRe denies access to students of other universities.',
    'faq07Title': 'Why do you need my Facebook login, and what will you do with my Facebook data?',
    'faq07Text': 'We are working on improved recommender algorithms that make use of your social network information. For this we need the list of your Facebook friends who are also using CoRe. All other Facebook data (such as information about gender and language) will only be used in an anonymous form for research purposes.',
    'faq08Title': 'Where does CoRe store my data (e.g. ratings, comments, etc.)?',
    'faq08Text': 'The data is stored on a server of the Department of Informatics (IFI) at the University of Zurich.',
    'faq09Title': 'What happens to my data?',
    'faq09Text': 'All data will be anonymized before being analyzed for research purposes. We do not disclose any personally identifiable information we gather about our users to any third parties for any purpose.',
    'faq10Title': 'How can I deactivate my account?',
    'faq10Text': 'Please write an email to <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a> with the subject <em>"Deactivate CoRe account"</em> from your UZH email account. We will send you an email that you have to confirm.',

    /*
     ****** User Agreement ******
     */
    'userAgreement': 'User agreement',
    'newUserAgreement': 'The user agreement has changed',
    'openUserAgreement': 'For further information please read the user agreement.',
    'userAgreementContent': 'CoRe is being developed by the Computation & Economics Research Group at the Department of Informatics (IFI) at the University of Zurich (UZH). CoRe stands for "Course Recommender" and is a web-based recommendation system for modules based on the course catalog of the UZH.' +

        '<br><br><strong>Infrastructure and privacy policy</strong>' +
        '<br><br>CoRe runs on a server of the UZH. Therefore, your data is stored on a server within the UZH. You can access CoRe via Facebook or using your Switch-AAI login. Once per semester you must verify your student status using your Switch-AAI login.' +

        '<br><br>The following data from Switch-AAI is transmitted and stored in CoRe:' +
        '<ul>' +
        '<li>Your name</li>' +
        '<li>Your UZH email address</li>' +
        '<li>Your matriculation number</li>' +
        '</ul>' +

        'When you log in via Facebook, the following data is transmitted and stored in CoRe:' +
        '<ul>' +
        '<li>Your public Facebook profile, i.e., your Facebook ID, name, age range, gender, country, language, time zone, and verification status</li>' +
        '<li>List of your Facebook friends, who are also using CoRe through Facebook</li>' +
        '</ul>' +

        'Depending on which CoRe functions you use, you can store additional data in your CoRe profile. This input of additional data is optional. The following data is affected:' +
        '<ul><li>The modules that you have attended and your ratings thereof</li>' +
        '<li>Your written comments</li></ul>' +
        '<br>You can change or delete this data at any time.' +

        '<br><br>The modules that you already attended can be added either manually or using an automatic import function that processes the data from your transcript. During this automatic import only the module codes are stored; no other data is stored. You can rate these modules. A certain minimum number of ratings is necessary to be able to use CoRe. CoRe uses your ratings to offer you recommendations for modules that you might like. No user can see any other user\'s ratings or modules, apart from his/her own.' +

        '<br><br><strong>Availability and Maintenance</strong>' +
        '<br><br>The Computation & Economics Research Group reserves the right to perform maintenance on CoRe at any time due to which CoRe may not be available for certain periods of time. Furthermore, the research group reserves the right to completely shut down CoRe at any point in time. By accepting this user agreement, you accept these limitations regarding CoRe\'s availability.' +

        '<br><br><strong>Your obligations</strong>' +
        '<br><br>By accepting the user agreement you oblige that you do not post any personal attacks, insults, name-calling, accusations, deliberate misinformation, deception, spam, pornographic / obscene / radical right-wing / inhuman / discriminatory / racist or infringing content.' +
        '<br>We reserve the right to remove any content or users from the system at any time and without notice.' +

        '<br>CoRe assists you in finding suitable modules, but accepts no responsibility for the correct declaration of modules. CoRe does not verify whether you select modules that comply with your study regulations. You are responsible for ensuring that you meet the relevant study regulations.<br>' +

        '<br><strong>Research purposes</strong>' +
        '<br><br>CoRe is a service that is operated by the Computation & Economics Research Group at UZH. The research group reserves the right to conduct scientific experiments using CoRe for research purposes at any time and without notice. In this context, you also allow the research group to send you emails. For example, emails may be sent to conduct a survey among CoRe users. CoRe generates log files for the actions of the users on CoRe, in particular regarding system login, comments, and ratings. The log files and survey results will be analyzed for research purposes. However, all data will be anonymized before being analyzed. We do not disclose any personally identifiable information we gather about our users to any third parties for any purpose.' +

        '<br><br><strong>Repudiation of liability</strong>' +
        '<br><br>CoRe cannot be held liable for inaccuracies, such as unsuitable recommendations, incomplete or missing modules.' +

        '<br><br>CoRe is an additional service that does not replace the module booking, the semester enrollment, the transcript, or any other service of the UZH. All modules listed in CoRe are based on the official UZH course catalog. However, there is no guarantee that the displayed information is complete and up-to-date. For the official information, please see the UZH course catalog (<a href="http://www.vorlesungen.uzh.ch" target="_blank">http://www.vorlesungen.uzh.ch</a>).',

    'networkUserAgreement': 'Social network information usage agreement',
    'newNetworkUserAgreement': 'The social network information usage agreement has changed',
    'networkUserAgreementContent': '<b>In the future, CoRe will try to improve your recommendations based information from your social network.</b> To do this, CoRe will ask other CoRe users to adjust, and/or evaluate recommendations for you. These users will learn that you are a user of CoRe and which courses the recommendation algorithm is considering for you. They will not be told which courses you have rated and what ratings you submitted for these courses.<br><br>' +
        'You can use CoRe without accepting to the social network information usage agreement. To revoke your acceptance please send an email to <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',
    /*
     ****** Registration ******
     */

     'informationTitle' : 'Information',
     'studyAdvertisementTitle' : 'Participation in Study',
     'informationContent' : 'The regular CoRe services are suspended for Spring 2017.',
     'studyAdvertisementContent' : 'The <a href="http://www.ifi.uzh.ch/ce.html", target="_blank">Computation and Economics Research Group</a> is currently conducting a study about course recommendation systems. If you participate, you will receive <b>personalized course recommendations for Fall 2016</b> and a compensation of <b>50 CHF</b> for approximately <b>1.5h of effort</b>. All tasks can be completed online. To participate, you must be currently enrolled in a BSc or MSc program with the Faculty of Business, Economics and Informatics (WWF) of the University of Zurich. If you have any questions, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',
     'studyBlocked' : 'The <a href="http://www.ifi.uzh.ch/ce.html", target="_blank">Computation and Economics Research Group</a> is currently conducting a study about course recommendation systems. The registration phase has ended. If you are a registered participant, you will be informed by email as soon as task 2 (give course recommendations) is available.<br><br>If you have any questions, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',
     'startRegistrationButton' : 'Start registration',
     'registrationWelcomePageInformation' : 'Please read the following information before accepting the participation agreement.',
     'agreementRequirements' : '<b>Requirements</b>',
     'agreementRequirementsContent' : 'To participate, you must be:<br>'+
        '<ul><li>enrolled in a BSc or MSc program with the Faculty of Business, Economics and Informatics of the University of Zurich since Spring 2016 or earlier (we reserve the right to verify this information)</li>'+
        '<li>planning to take courses in Fall 2016</li>'+
        '<li>a Facebook user</li></ul>',
     'agreementTasks' : '<b>Tasks</b>',
     'agreementTasksContent' : 'All tasks can be performed online on a computer (<b>not</b> from a smartphone or tablet):<br>'+
        '<ol><li>Registration (~20 min) until August 21, 2016</li>'+
        '<li>Give course recommendations (~30 min) any time between August 22 and September 4, 2016</li>'+
        '<li>Receive course recommendations (~20 min) any time between September 5 and 18, 2016</li>'+
        '<li>Rate modules & fill out survey (~20 min) any time between February 13 and 27, 2017 (after the fall semester 2016)</li></ol>',
     'agreementCompensation' : '<b>Compensation</b>',
     'agreementCompensationContent' : '<ul><li>50 CHF for completing all tasks<br>'+
        'Note: for the tasks 2, 3, and 4, we will match you with another participant. In the unlikely event that we cannot match you, or if the study needs to be aborted for technical reasons, you will not be asked to complete the tasks 2, 3 and 4. In that case, you will only receive a compensation of 10 CHF for registering.</li>'+
        '<li>Payment in <a href="https://www.migros.ch/de/zahlungsmoeglichkeiten/geschenkkarten/geschenkkarten-fuer-privatkunden.html", target="_blank">Migros gift vouchers</a> by email after completion of your assigned tasks; to request payment in cash, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a></li></ul>',
     'agreementPrivacy' : '<b>Data and Privacy</b>',
     'agreementPrivacyContent' : 'In this study, we collect the following information about you: name and email address from Switch/AAI, your public Facebook profile, a list of your Facebook friends who are also registered for this study, and the information provided by you during the study. All information is kept confidential, is processed anonymously, and is used only for research purposes. No personally identifiable information will be published or shared with third parties.<br>'+
     'For tasks 2 and 3, you will be matched with one of your Facebook friends and you will be asked to recommend modules to each other. During these tasks, your match partner will learn that you are participating in this study, which modules our recommendation algorithm would suggest to you, and which modules you eventually recommend to him/her. However, your match partner will <b>not</b> be told which modules you have rated and what ratings you have given these modules.',
     'participationAgreementCheckbox' : 'Yes, I understand and accept the participation agreement.',
     'registrationAAIPageInformation' : 'Please confirm your student status by logging in via AAI.',
     'registrationFacebookInformation' : 'To enable course recomendations by friends, please log in via Facebook.',
     'facebookConnectButton' : 'Connect to Facebook',
     'registrationStudyInformation' : 'Please provide information about your studies.',
     'registrationStudyInformationCurrent' : 'Please select the study program for which you were enrolled until Spring 2016 (before the recent regulation changes to the study programs of the WWF).', 
     'registrationStudyInformationNext' : 'Please select the degree that you will be pursuing in Fall 2016.',
     'registrationImportMethodInformation' : 'Please add all modules that you have taken. You have two options to do this.<br><br><b>Note:</b> If you have previously used CoRe, your prior ratings have been imported into the study. In this case, please add and rate all additional modules that you have taken but not yet rated.', 
     'copyPasteInformation' : 'Please open your transcript, mark the whole transcript and copy it to the clipboard, then paste it into the text field below.',
     'registrationCompleteInformation' : 'You have successfully registered for the study. Thank you for participating.',
     'registrationCompleteContent1' : 'A confirmation email will be sent to ',
     'registrationCompleteContent2' : ' shortly.',
     'registrationCompleteContent3' : 'Shortly after August 21, 2016, <b>we will contact you by email</b> with instructions regarding task 2 (give course recommendations).',
     'registrationCompleteContent4' : 'Currently, ',
     'registrationCompleteContent5' : ' registered for this study. To increase your chances of being matched (and earn the full compensation of 50 CHF), please tell your fellow students about this study.',
     'registrationCompleteContent6' : 'If you have any questions, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a> (e.g., if you are unable to participate or change your mind).',
     'registrationLogoutInfo' : 'Sorry! The system is currently unable to take new registrations. Please return later for more information.',

    /*
     ****** Registration Design ******
     */
     'stepOne' : 'Participation Agreement',
     'stepTwo' : 'Verify Student Status',
     'stepThree' : 'Connect to Facebook',
     'stepFour' : 'Enter Study Information',
     'stepFive' : 'Add Modules',
     'stepSix' : 'Rate Modules',
     'stepSeven' : 'Registration Completed',


    /*
    ******* Task 2 *******
    */
    'task2InformationContent' : 'The regular CoRe services are suspended for Fall 2016.',
    'studyAdvertisementContent' : 'The <a href="http://www.ifi.uzh.ch/ce.html", target="_blank">Computation and Economics Research Group</a> is currently conducting a study about course recommendation systems. The registration phase has ended. If you are a registered participant, please log in to perform task 2 (give course recommendations). You must complete this task on a computer, <b>not</b> on a mobile phone or tablet.',
    'task2InstructionsInformationOne' : 'Welcome, ',
    'task2InstructionsInformationTwo' : '. Please read the instructions of task 2.',
    'task2InstructionsContentOne' : 'In this task you will create a list of course recommendations for your match partner ',
    'task2InstructionsContentTwo' : 'You were matched with ',
    'task2InstructionsContentThree' : ' is your match partner because you are friends on Facebook and you are both registered for this study. Please make recommendations that you believe will be useful for ',
    'task2InstructionsContentFour' : ', even if you may not know him/her that well. Note: to maintain the integrity of the study, please do not talk to ',
    'task2InstructionsContentFive' : ' about the recommendations you provide until the study is over.',
    'notAllowedInfo': 'Sorry, you cannot complete task 3.',
    'notAllowedContent' : 'Possible reasons include:<br><br>1. You did not register for this study or are not eligible to participate.<br><br>2. You registered for this study but we could not find a match partner for you; in this case, you will receive the compensation for registering shortly.<br><br>3. We are unable to make recommendations for you for other reasons; in this case, we will contact you shortly regarding your compensation.<br><br>If you believe, this is a mistake or if you have any questions, please send an email to <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',

    /*
    ******* Task 2 Tutorial *******
    */
    'task2TutorialState0InformationZero' : 'This step-by-step tutorial explains how to make your recommendations for ',
    'task2TutorialState0InformationZeroTwo': '.<br><b>Click \"next\" to continue</b>.',
    'task2TutorialState0InformationOne' : 'Below you see the interface through which you will make your recommendations.<br><b>Click \"next\" to continue</b>.',
    'task2TutorialState0InformationTwo' : '. Use the \"next\" and \"back\" buttons to navigate through the tutorial.',
    'task2TutorialState1InformationOne' : 'You begin with an automatically generated list of modules that ',
    'task2TutorialState1InformationTwo' : ' may find interesting (highlighted in red below). <b>Click \"next\" to continue</b>.',
    'task2TutorialState2InformationOne' : '', 
    'task2TutorialState2InformationTwo' : ' will receive the top-10 modules as a rank-ordered list as recommendations. You can improve this list by changing the position of modules and by adding or removing modules. <b>Click \"next\" to continue</b>.',
    'task2TutorialState3Information' : 'To change the position of a module, drag and drop the <i style="padding-left: 5px; padding-right: 5px;" class="fa fa-arrows"></i> icon on its left to a new position.<br><b>To continue, please change the position of at least one module in the list on the left, then click \"next.\"',
    'task2TutorialState4Information' : 'To add a new module, enter part of its name in the search field. Once the module appears, drag and drop it into any position in the list. <b>To continue, please add the module named \"Module Title TEST\", then click \"next.\"</b>',
    'task2TutorialState5Information' : 'To remove a module, click on the <i style="padding-left: 5px; padding-right: 5px;" class="fa fa-trash-o"></i> symbol on its right.<br><b>To continue, please remove at least 5 modules from the list, then click \"next.\"</b>',
    'task2TutorialState6Information' : 'Removed modules are placed in the recycling bin. To recover a module fom the recycling bin, drag and drop it to any position in the list. <b>To continue, please recover a module from the recycling bin, then click \"next.\"',
    'task2TutorialState7Information' : 'You can open the course catalogue entry of a module by clicking the <i style="padding-left: 5px; padding-right: 5px;" class="fa fa-info-circle"></i> symbol on its right.<br><b>Click \"next\" to continue.',
    'task2TutorialState8InformationOne' : 'You have completed the tutorial.<br><b>Click \"start\" to begin recommending modules to </b>',

    /*
    ******* Task 2 Human Recommender ******
    */
    'task2HumanRecommenderInformationOne' : 'You start with an automatically generated list of modules that ',
    'task2HumanRecommenderInformationTwo' : ' may find interesting. Please improve the list of top-10 modules by adding and removing modules and by changing their position. When you believe that you cannot improve the recommendations any further, click \"Submit.\"',
    'task2HumanRecommenderInformationThree' : 'Do you want to go back to the tutorial? All changes you have made will be lost.',
    'task2HumanRecommenderInformationFour' : 'Do you want to submit the list of recommendations for ',

    /*
    ****** Task 2 Survey ******
    */
    'task2SurveyOneInformation' : 'Your match partner is ',
    'task2SurveyOneInformationTwo' : '. Please answer the following questions as accurately as possible.',



    'task2SurveyInformation' : 'Thank you very much for recommending this list of modules to ',
    'task2SurveyInformationTwo' : '. As a last step, we would like to ask you a few survey questions. Please answer all questions as best as you can.',
    'task2SurveyQuestionOne' : '3. Please estimate how well you are able to give good/useful course recommendations for ',
    'task2SurveyQuestionTwo' : '2. How well do you know ',
    'task2SurveyQuestionThree' : '1. How do you know ',
    'task2SurveyQuestionFourOne' : '1. Please describe your strategy (i.e., your process) for putting together the list of recommendations for',
    'task2SurveyQuestionFourTwo' : '. Note that there are no wrong answers.',
    'task2SurveyQuestionFiveOne' : '2. What information that you have about ',
    'task2SurveyQuestionFiveTwo' : ' has helped you when making the recommendations?',
    'task2SurveyQuestionSix' : '3. What additional information would have helped you to give even better recommendations?',
    'task2SurveyQuestionSeven' : '4. What was good about the recommendation tool and should definitely be kept?',
    'task2SurveyQuestionEight' : '5. What was bad about the recommendation tool and should be improved in the future?',
    'task2SurveyQuestionNine' : '6. Do you have any other comments?',
    
    /*
    *** Task 2 Complete ******
    */
    'task2CompleteInformation' : 'You have successfully completed task 2 (give course recommendations). Thank you for participating.',
    'task2CompleteContentOne' : 'Reminder: to maintain the integrity of the study, please do not talk to ',
    'task2CompleteContentTwo' : ' about the recommendations you have just made until the study is over.<br><br>',
    'task2CompleteContentThree' : 'Shortly after September 4, 2016 <b>we will contact you by email</b> with instructions regarding task 3 (receive course recommendations).<br><br>'+
        'If you have any questions, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',

    /*
    ******* Task 2 Design ********
    */
    'task2LayoutTitle' : 'Study about Course Recommendation Systems – Recommend Modules',
    'task2StepOne' : 'Instructions',
    'task2StepTwo' : 'Tutorial',
    'task2StepThree' : 'Recommend Modules',
    'task2StepFour' : 'Survey',
    'task2StepFive' : 'Complete',

    /*
    ******* Task 3 Design *********
    */
    'task3LayoutTitle' : 'Study about Course Recommendation Systems – Receive Recommendations',
    'task3StepOne' : 'Instructions',
    'task3StepTwo' : 'Part 1',
    'task3StepThree' : 'Part 2',
    'task3StepFour' : 'Part 3',
    'task3StepFive' : 'Part 4', 
    'task3StepSix' : 'Complete',

    /*
    ******* Task 3 *********
    */
    'studyAdvertisementContent' : 'The Computation and Economics Research Group is conducting a study about course recommendation systems. If you are a registered participant, please log in to perform task 3 (receive course recommendations). You must complete this task on a computer, <b>not</b> on a mobile phone or tablet.',
    'studyBlocked' : 'The Computation and Economics Research Group is conducting a study about course recommendation systems. Task 3 has ended. If you are a registered participant, you will be informed by email as soon as task 4 (rate modules) is available.',
    'task2LayoutTitle' : 'Study about Course Recommendation Systems – Receive Recommendations',
    'task3InstructionsInformationOne' : 'Welcome, ',
    'task3InstructionsInformationTwo' : '. Please read the instructions of task 3.',
    'task3InstructionsContent1' : 'In this task you will receive two lists of course recommendations that were generated for you for the Fall Semester 2016. One of the lists was generated by a computer algorithm, and the other list was generated by your match partner, ',
    'task3InstructionsContent2' : '. We will ask you a series of survey questions about those lists. Completing this task will take about 20-30 minutes.<br><br>Note: the information you provide during this task will <b>not</b> be shown to other participants; your answers will have <b>no influence</b> on the compensation of you or anyone else.',
    'task3InstructionsContent3' : ' will receive from this study.',
    'task3SurveyOneInformation' : 'Please answer the question as well as you can.',
    'task3Survey1Question1' : 'Please estimate how well you think ',
    'task3Survey1Question2' : ' was able to give good/useful course recommendations for you.',
    'task3Survey3Information' : 'Here you see all courses that have been recommended to you (randomly ordered). For each course, please answer the following questions as well as you can.',
    'task3CompleteInformation' : 'You completed Task 3. Thank you very much.',

    
    /*
    ******* Task 4 ********
    */
    'studyAdvertisementContent' : 'The Computation and Economics Research Group is conducting a study about course recommendation systems. If you are a registered participant, please log in to perform task 4 (rate modules). You must complete this task on a computer, <b>not</b> on a mobile phone or tablet. <br><br>If you have any questions, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',
    'task4LayoutTitle' : 'Study about Course Recommendation Systems – Rate Modules',
    'task4InstructionsInformationOne' : 'Welcome, ',
    'task4InstructionsInformationTwo' : '. Please read the instructions of task 4.',
    'task4CompleteContent' : 'You completed all tasks of our study.<br><br>Shortly after February 27, 2017 we will contact you about your compensation.<br><br>If you have any questions, please contact us at <a href="mailto:core@ifi.uzh.ch">core@ifi.uzh.ch</a>.',
});
