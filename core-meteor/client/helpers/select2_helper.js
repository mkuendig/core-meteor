Select2Helper = {

    filterEctsSelector: function() {
        return {
            id: 'filter-ects',
            placeholder: 'ECTS',
            multiple: 'multiple'
        };
    },

    filterLecturersSelector: function() {
        return {
            id: 'filter-lecturers',
            placeholder: i18n('lecturer'),
            multiple: 'multiple',
            options: {
                minimumInputLength: 3
            }
        };
    },

    filterModuleCategorySelector: function() {
        return {
            id: 'filter-module-categories',
            placeholder: i18n('moduleType'),
            options: {
                allowClear: true
            }
        };
    },

    filterOrgUnitSelector: function() {
        return {
            id: 'filter-orgs',
            placeholder: i18n('organizationalUnit'),
            multiple: 'multiple'
        };
    },

    filterSemesterSelector: function() {
        return {
            id: 'filter-semesters',
            placeholder: i18n('semester'),
            options: {
                allowClear: true
            }
        };
    },

    filterStudyFieldSelector: function() {
        return {
            id: 'filter-study-field',
            placeholder: i18n('studyCourse'),
            options: {
                allowClear: true
            }
        };
    },

    filterWeekdaysSelector: function() {
        return {
            id: 'filter-weekdays',
            placeholder: i18n('weekday'),
            multiple: 'multiple'
        };
    },


    filterEctsSelectorOptions: function(selectedEcts) {
        var options = '<optgroup label="">';
        _.each([3, 6, 9], function(ects) {
            options += '<option value="' + ects + '">' + ects + '</option>';
        });
        options += '</optgroup><optgroup label="' + i18n('allLabel') + '">';
        _.each([0, 1, 1.5, 2, 3, 4, 4.5, 5, 6, 8, 9, 15], function(ects) {
            var selected = _.contains(selectedEcts, ects) ? 'selected' : '';
            options += '<option value="' + ects + '" ' + selected + '>' + ects + '</option>';
        });
        options += '</optgroup>';
        return options;
    },

    filterLecturersSelectorOptions: function(selectedLecturers) {
        var options = '';
        Persons.find().forEach(function(lecturer) {
            var selected = _.contains(selectedLecturers, lecturer.uzhId) ? 'selected' : '';
            options += '<option value="' + lecturer.uzhId + '" ' + selected + '>' + lecturer.firstname + ' ' + lecturer.lastname + '</option>';
        });
        return options;
    },

    filterModuleCategorySelectorOptions: function(categorySlug) {
        var options = '';
        var optionCategories = Categories.getDirectChildren(categorySlug);
        _.each(optionCategories, function(category) {
            options += '<option value="' + category.slug + '">' + category.title + '</option>';
        });
        return options;
    },

    filterOrgUnitSelectorOptions: function(selectedOrgUnits) {
        var options = '';
        OrganizationalUnits.find({}, {
            fields: {
                title: 1
            },
            sort: {
                title: 1
            }
        }).forEach(function(orgUnit) {
            var selected = _.contains(selectedOrgUnits, orgUnit.title) ? 'selected' : '';
            options += '<option value="' + orgUnit.title + '" ' + selected + '>' + orgUnit.title + '</option>';
        });
        return options;
    },

    filterSemesterSelectorOptions: function(selectedSemester) {
        var options = '';
        _.each(['HS', 'FS'], function(semester) {
            var selected = selectedSemester === semester ? 'selected' : '';
            options += '<option value="' + semester + '" ' + selected + '>' + semester + '</option>';
        });
        return options;
    },

    filterStudyFieldSelectorOptions: function(selectedStudyField) {
        var options = '';
        _.each(['vwl_bsc_economics', 'bwl_bsc_economics', 'bf_bsc_economics', 'me_bsc_economics', 'wi_bsc_informatics', 'ss_bsc_informatics', 'ani_bsc_informatics', 'abi_bsc_informatics', 'agi_bsc_informatics', 'ami_bsc_informatics', 'aci_bsc_informatics', 'bwl_msc_economics', 'vwl_msc_economics', 'bf_msc_economics', 'me_msc_economics', 'wi_msc_informatics', 'ss_msc_informatics', 'cs_msc_informatics'], function(option) {
            var selected = selectedStudyField === option ? 'selected' : '';
            options += '<option value="' + option + '" ' + selected + '>' + i18n(option) + '</option>';
        });
        return options;
    },

    filterWeekdaysSelectorOptions: function(selectedWeekdays) {
        var options = '';
        _.each([{
            label: i18n('Monday'),
            value: 1
        }, {
            label: i18n('Tuesday'),
            value: 2
        }, {
            label: i18n('Wednesday'),
            value: 3
        }, {
            label: i18n('Thursday'),
            value: 4
        }, {
            label: i18n('Friday'),
            value: 5
        }, {
            label: i18n('Saturday'),
            value: 6
        }], function(weekday) {
            var selected = _.contains(selectedWeekdays, weekday.value) ? 'selected' : '';
            options += '<option value="' + weekday.value + '" ' + selected + '>' + weekday.label + '</option>';
        });
        return options;
    }
};