UIHelper = {
    userName: function() {
        var name = '';
        if(Meteor.user() && Meteor.user().profile.callName){
            name = Meteor.user().profile.callName;
        }
        return name ? name.split(' ')[0] : '';
    },
    /**
     * Refresh rateit plugin because stars are not visible when not refreshing
     */
    refreshRateit: function() {
        var rateIt = $('.rateit');
        if (rateIt) {
            rateIt.unbind();
            rateIt.rateit({
                step: 1,
                starwidth: 20,
                starheight: 20
            }).bind('rated', function(event, value) {
                // save rating
                var element = $(this);
                Meteor.call('setRating', element.attr('id'), value);
                Toast.success(i18n('user_module_relations_rating_added'));
                element.attr('data-rateit-value', value);
            }).bind('reset', function() {
                // delete rating
                var element = $(this);
                Meteor.call('resetRating', element.attr('id'));
                Toast.success(i18n('user_module_relations_rating_deleted'));
            }).bind('over', function(event, value) {
                var element = $(this);
                if (value > 0) {
                    element.attr('data-rateit-hover-value', value);
                }
            });
        }
    },

    initiateDrop: function() {
        var dropItems = $('.drop-item');
        _.each(dropItems, function(dropItem) {
            var counter = 0; // to ignore ondragleave event on child elements
            dropItem.ondrop = function(event) {
                event.preventDefault();
                $(dropItem).removeClass('highlight');
                counter = 0;
                var moduleId = event.dataTransfer.getData('text');
                var studyId = dropItem.getAttribute('data-study-id');
                var regulationId = dropItem.getAttribute('data-regulation-id');
                if (moduleId.substring(0, 5) === 'self-') {
                    moduleId = moduleId.slice(5);
                    Meteor.call('addSelfDefinedModuleToRegulation', moduleId, studyId, regulationId);
                } else {
                    Meteor.call('addModuleToRegulation', moduleId, studyId, regulationId);
                }
            };
            dropItem.ondragover = function(event) {
                event.preventDefault();
            };
            dropItem.ondragenter = function() {
                counter += 1;
                $(dropItem).addClass('highlight');
            };
            dropItem.ondragleave = function() {
                counter -= 1;
                if (counter < 1) {
                    $(dropItem).removeClass('highlight');
                }
            };
        });
    },

    initiateDrag: function(scrollTop) {
        var dragItems = $('.drag-item');
        _.each(dragItems, function(dragItem) {
            dragItem.ondragstart = function(event) {
                if (scrollTop) {
                    $('.scrollable').animate({
                        scrollTop: 0
                    }, 500);
                }
                $('.drop-area').addClass('show');
                if (dragItem.getAttribute('data-module-id')) {
                    event.dataTransfer.setData('text', dragItem.getAttribute('data-module-id'));
                } else if (dragItem.getAttribute('data-self-module-id')) {
                    event.dataTransfer.setData('text', 'self-' + dragItem.getAttribute('data-self-module-id'));
                }
            };
            dragItem.ondragend = function() {
                $('.drop-area').removeClass('show');
            };
        });

    },

    completedEcts: function(regulation) {
        var ectsModules = Modules.getEctsSum(regulation.modules);
        var ectsSelfDefinedModules = 0;
        _.each(Meteor.user().profile.selfDefinedModules, function(selfDefinedModule) {
            if (_.contains(regulation.selfDefinedModules, selfDefinedModule.id)) {
                ectsSelfDefinedModules += selfDefinedModule.ects;
            }
        });
        return ectsModules + ectsSelfDefinedModules;
    },

    getRegulations: function(studyId) {
        var regulations = [];
        if (Meteor.user().profile[studyId]) {
            regulations = Meteor.user().profile[studyId].regulations;
            _.each(regulations, function(regulation) {
                regulation.studyId = studyId;
                regulation.last = false;
            });
            if (regulations.length > 0) {
                regulations[regulations.length - 1].last = true;
            }
        }
        return regulations;
    },

    getSumTotalEcts: function(studyId) {
        var sum = 0;
        _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
            sum += regulation.ects;
        });
        return sum;
    },

    regulationSelfDefinedModules: function(selfDefinedModuleIds) {
        if (!selfDefinedModuleIds) {
            return;
        }
        var selfDefinedModules = [];
        _.each(Meteor.user().profile.selfDefinedModules, function(selfDefinedModule) {
            if (_.contains(selfDefinedModuleIds, selfDefinedModule.id)) {
                selfDefinedModules.push(selfDefinedModule);
            }
        });
        return selfDefinedModules;
    },

    getArea: function(moduleId, regulations) {
        var reg = _.find(regulations, function(regulation) {
            return _.find(regulation.modules, function(id) {
                return id === moduleId;
            });
        });
        if (reg) {
            return reg.name;
        }
    },

    totalEctsRegulation: function(studyId) {
        var sum = 0;
        _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
            sum += regulation.ects;
        });
        return sum;
    },

    regulationModules: function(moduleIds) {
        return moduleIds ? Modules.find({
            moduleId: {
                $in: moduleIds
            }
        }) : undefined;
    }
};