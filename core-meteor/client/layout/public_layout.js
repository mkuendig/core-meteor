Template.publicLayout.helpers({
    currentLanguage: function() {
        var language = Session.get('language');
        if (language === 'undefined') {
            LanguageConfig.initializeLanguage();
        }
        if (language === 'en') {
            return 'En';
        } else {
            return 'De';
        }
    }
});

Template.publicLayout.events({
    'click #language-de': function() {
        LanguageConfig.setLanguage('de', false);
    },

    'click #language-en': function() {
        LanguageConfig.setLanguage('en', false);
    }
});