var reminderDep = new Tracker.Dependency();
Template.navbar.rendered = function() {
    $('[data-toggle="tooltip"]').tooltip();
};

Template.navbar.helpers({
    currentSemesterPlans: function() {
        return SemesterPlans.getCurrentSemesterPlanNames();
    },

    currentLanguage: function() {
        var language = Session.get('language');
        if (language) {
            return language.charAt(0).toUpperCase() + language.slice(1);
        }
    },

    userName: function() {
        var userName = UIHelper.userName();
        var currentLanguage = i18n.getLanguage();
        if (_.isEmpty(userName) || userName.length > 14) {
            return i18n('your');
        }
        var lastLetter = userName[userName.length - 1];
        if (currentLanguage === 'de') {
            if (lastLetter === 's' || lastLetter === 'z' || lastLetter === 'x' || (userName[lastLetter - 1 === 'c'] && lastLetter === 'e')) {
                userName += '\'';
            } else {
                userName += 's';
            }
        } else if (currentLanguage === 'en') {
            if (lastLetter === 's') {
                userName += '\'';
            } else {
                userName += '\'s';
            }
        }
        return userName;
    },

    userNameTooLong: function() {
        var userName = UIHelper.userName();
        return userName.length > 13;
    },

    ifFbInfoPresent: function() {
        reminderDep.depend();
        if (Session.get('dataReady') === undefined) {
            return false;
        } else{
            return Session.get('dataReady');
        }
    },
    fbNotConnected: function() {
        return !Session.get('fbPermissionExists');
    }
});

Template.navbar.events({
    'click #language-de': function() {
        LanguageConfig.setLanguage('de', true);
    },

    'click #language-en': function() {
        LanguageConfig.setLanguage('en', true);
    },

    'click #logout-button': function() {
        Router.go('logout');
        Meteor.logout(function() {
            //Navigat to Shibboleth Logout URL on success
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.logout(function() {
                        window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                    });
                } else {
                    window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                }
            });
        });
    },

    'click #missingFBInfo': function() {
        $('#facebookReminder-modal').modal();
    },

    'click #FBLogin': function() {
        $('#facebookReminder-modal').modal('hide');
        FBController.loginToFB();
    },

    'click #noFBLogin': function() {
        $('#facebookReminder-modal').modal('hide');
    }
});

Template.navbar.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('fbPermission', function() {
            Tracker.autorun(function() {
                var res = FBPermission.findOne({
                    userId: Meteor.userId()
                    });
                Session.set('dataReady', res);
                reminderDep.changed();
            });
        });
    });
});