Template.wizardLayout.helpers({
    currentLanguage: function() {
        var language = Session.get('language');
        if (language) {
            return language.charAt(0).toUpperCase() + language.slice(1);
        }
    }
});

Template.wizardLayout.events({
    'click #language-de': function() {
        LanguageConfig.setLanguage('de', false);
    },

    'click #language-en': function() {
        LanguageConfig.setLanguage('en', false);
    }
});