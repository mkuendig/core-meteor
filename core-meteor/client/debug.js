DEBUG = function() {
    // Return TRUE if we are on a url with a port (e.g localhost:3000)
    return Boolean(location.port);
};