Meteor.startup(function() {
    $('html').addClass('app');

    // Setting current semesters
    Session.set('currentFallSemester', 'hs15');
    Session.set('currentSpringSemester', 'fs16');

    Session.set('fbPermissionExists', true);

    LanguageConfig.initializeLanguage();

    var select2i18n = new Select2i18n();
    select2i18n.init();
});
