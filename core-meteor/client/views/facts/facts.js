Template.facts.created = function() {
    Meteor.call('getNumberOfRatings', function(error, result) {
        Session.set('numberOfRatings', result);
    });

    Meteor.call('getNumberOfUsers', function(error, result) {
        Session.set('numberOfUsers', result);
    });

    Meteor.call('getNumberOfOecStudents', function(error, result) {
        Session.set('numberOfOecStudents', result);
    });

    Meteor.call('getNumberOfIfiStudents', function(error, result) {
        Session.set('numberOfIfiStudents', result);
    });

    Meteor.call('getNumberOfUsersWithRating', function(error, result) {
        Session.set('numberOfUsersWithRating', result);
    });
};

Template.facts.helpers({

    numberOfRatings: function() {
        return Session.get('numberOfRatings');
    },

    numberOfUsers: function() {
        return Session.get('numberOfUsers');
    },

    numberOfUsersWithRating: function() {
        return Session.get('numberOfUsersWithRating');
    },

    numberOfOecStudents: function() {
        return Session.get('numberOfOecStudents');
    },

    numberOfIfiStudents: function() {
        return Session.get('numberOfIfiStudents');
    }
});