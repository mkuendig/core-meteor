Template.home.rendered = function() {
    $('.easypiechart').easyPieChart();
    $('[data-toggle="tooltip"]').tooltip();
    $('.carousel').carousel();
};

Template.home.helpers({
    getRatingPercent: function() {
        return RatingController.getRatingPercentage();
    },

    bestRatedModulesWWF: function() {
        return Modules.getBestRatedModulesByCategory('wwf', 5);
    },

    bestRatedModulesIFI: function() {
        return Modules.getBestRatedModulesByOrganizationalUnit('Institut für Informatik', 5);
    },

    bestRatedModulesBWL: function() {
        return Modules.getBestRatedModulesByOrganizationalUnit('Institut für Betriebswirtschaftslehre', 5);
    },

    bestRatedModulesVWL: function() {
        return Modules.getBestRatedModulesByOrganizationalUnit('Institut für Volkswirtschaftslehre', 5);
    },

    bestRatedModulesBF: function() {
        return Modules.getBestRatedModulesByOrganizationalUnit('Institut für Banking und Finance', 5);
    },

    hasTrends: function() {
        if (!Modules.getTrendingModules(5).length || Modules.getTrendingModules(5).length === 0) {
            return false;
        }
        return true;
    },

    currentFS: function() {
        return Session.get('currentSpringSemester');
    },

    currentHS: function() {
        return Session.get('currentFallSemester');
    },

    currentSemester: function() {
        return Session.get('semesterId');
    },

    avgRating: function() {
        return RatingController.getAvgRating(this);
    },

    numberRatings: function() {
        return RatingController.getNumberRatings(this);
    },

    i18nratings: function() {
        if (RatingController.getNumberRatings(this) < 2) {
            return i18n('rating');
        } else {
            return i18n('ratings');
        }
    },

    fbNotConnected: function() {
        return !Session.get('fbPermissionExists');
    }
});

Template.home.events({
    'click #openSemesterPlan': function() {
        Router.go('semesterPlanRecommendations', {
            _semesterId: Session.get('semesterId')
        });
    },

    'click #goToModuleRating': function() {
        Router.go('moduleRatingProfile');
    },

    'click #fs-recommendations-tab': function() {
        Session.set('semesterId', Session.get('currentSpringSemester'));
    },

    'click #hs-recommendations-tab': function() {
        Session.set('semesterId', Session.get('currentFallSemester'));
    },
    'click #networkInfo': function() {
        $('#facebookReminder-modal').modal();
    },
    'click #FBLogin': function() {
        $('#facebookReminder-modal').modal('hide');
        FBController.loginToFB();
    },
    'click #noFBLogin': function() {
        $('#facebookReminder-modal').modal('hide');
    }
});
