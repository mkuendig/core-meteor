Template.task3Instructions.rendered = function() {
    Meteor.call('updateTask3State', 1);
    if(_.isUndefined(ExperimentSubject.findOne()) || _.isUndefined(ExperimentSubject.findOne().recommendationMatrix)){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().task3State > 5){
        Router.go('task3Complete');
    } else if(ExperimentSubject.findOne().task3State > 4){
        Router.go('task3SurveyFour');        
    } else if(ExperimentSubject.findOne().task3State > 3){
        Router.go('task3SurveyThree');       
    } else if(ExperimentSubject.findOne().task3State > 2){
        Router.go('task3SurveyTwo');       
    } else if(ExperimentSubject.findOne().task3State > 1){
        Router.go('task3SurveyOne');
    }
}
Template.task3Instructions.helpers({
    'firstName' : function(){
        if(Meteor.user().profile.callName && Meteor.user().profile.callName.split(' ')[0]){
            return Meteor.user().profile.callName.split(' ')[0];
        } else{
            return 'participant';
        }
    },
    'matchPartnerFirstName': function(){
        return ExperimentSubject.findOne().matchPartnerCallName.split(' ')[0];
        return "asdf";
    },
    'matchPartnerFullName' : function(){
        return ExperimentSubject.findOne().matchPartnerCallName;
        return "asdf";
    }
});

Template.task3Instructions.events({
    'click #continue': function(event) {
        Router.go('task3SurveyOne');
    }
});
