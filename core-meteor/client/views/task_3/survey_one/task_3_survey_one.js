var reminderDep = new Tracker.Dependency();
var missing = 1;
Template.task3SurveyOne.rendered = function() {
    Meteor.call('updateTask3State', 2);
    Meteor.call('task3StartSurvey1', Meteor.userId());
    if(_.isUndefined(ExperimentSubject.findOne()) || _.isUndefined(ExperimentSubject.findOne().recommendationMatrix)){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().task3State > 5){
        Router.go('task3Complete');
    } else if(ExperimentSubject.findOne().task3State > 4){
        Router.go('task3SurveyFour');        
    } else if(ExperimentSubject.findOne().task3State > 3){
        Router.go('task3SurveyThree');       
    } else if(ExperimentSubject.findOne().task3State > 2){
        Router.go('task3SurveyTwo');       
    }
}

Template.task3SurveyOne.helpers({
    'matchFirstName' : function(){
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
    },
    'continueDisabled' : function(){
    	reminderDep.depend();
    	return missing == 1;
    }
});

Template.task3SurveyOne.events({
    'click #submit': function(event) {
    	var answer;
    	var radio = document.getElementsByName('RadioGroup');
    	for(var i = 0; i < radio.length; i++){
		    if(radio[i].checked){
		        answer = radio[i].value;
		    }
		}
		if(answer){
            document.getElementById('submit').value = 'sending...';
            document.getElementById('submit').disabled = true;
           
			Meteor.call('task3StoreSurvey1', answer, function(error){
                if(error){
                    document.getElementById('submit').value = 'Submit';
                    document.getElementById('submit').disabled = false;
                    window.alert(error);
                } else{
                    Router.go('task3SurveyTwo');
                }
            });
			
		}
    },
    'change .radGr' : function(){
        missing = 0;
        reminderDep.changed();
    }
});
