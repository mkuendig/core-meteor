Template.task3SurveyFour.rendered = function() {
    Meteor.call('updateTask3State', 5);
    Meteor.call('task3StartSurvey4', Meteor.userId());
    if(_.isUndefined(ExperimentSubject.findOne()) || _.isUndefined(ExperimentSubject.findOne().recommendationMatrix)){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().task3State > 5){
        Router.go('task3Complete');
    }
}

Template.task3SurveyFour.helpers({
    'matchFirstName' : function(){
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
    }
});

Template.task3SurveyFour.events({
    'click #continue': function(event) {
            document.getElementById('continue').value = 'sending...';
            document.getElementById('continue').disabled = true;
           
            answ1 = 'none';
            answ2 = 'none';
            answ3 = 'none';
            answ4 = 'none';
            answ5 = 'none';
            answ6 = 'none';
            answ7 = 'none';

            if(document.getElementById('element1').value){
                answ1 = document.getElementById('element1').value;
            }
            if(document.getElementById('element2').value){
                answ2 = document.getElementById('element2').value;
            }
            if(document.getElementById('element3').value){
                answ3 = document.getElementById('element3').value;
            }
            if(document.getElementById('element4').value){
                answ4 = document.getElementById('element4').value;
            }
            if(document.getElementById('element5').value){
                answ5 = document.getElementById('element5').value;
            }
            if(document.getElementById('element6').value){
                answ6 = document.getElementById('element6').value;
            }
            if(document.getElementById('element7').value){
                answ7 = document.getElementById('element7').value;
            }

            Meteor.call('task3StoreSurvey4', answ1, answ2, answ3, answ4, answ5, answ6, answ7, function(error){
                if(error){
                    document.getElementById('continue').value = 'Submit';
                    document.getElementById('continue').disabled = false;
                    window.alert(error);
                } else{
                	Router.go('task3Complete');
                }
            });
    }
});
