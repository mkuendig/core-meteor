Template.notAllowed.rendered = function() {
    Meteor.call('task3NotAccepted', Meteor.userId());
}

Template.notAllowed.helpers({

});
Template.notAllowed.events({
	'click #logout': function() {
        Meteor.call('task3Logout', Meteor.userId());
        Router.go('home');
        Meteor.logout(function() {
            window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
        });
    }
});
