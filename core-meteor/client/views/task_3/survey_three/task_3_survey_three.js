var isMissing = 1;
var reminderDep = new Tracker.Dependency();

Template.task3SurveyThree.rendered = function() {
    Meteor.call('updateTask3State', 4);
    Meteor.call('task3StartSurvey3', Meteor.userId());
    if(_.isUndefined(ExperimentSubject.findOne()) || _.isUndefined(ExperimentSubject.findOne().recommendationMatrix)){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().task3State > 5){
        Router.go('task3Complete');
    } else if(ExperimentSubject.findOne().task3State > 4){
        Router.go('task3SurveyFour');        
    }
}

Template.task3SurveyThree.helpers({
    'header' : function(){
    	answ = [];
    	answ.push(1);
        return answ;
    },
    'modules' : function(){
    	return ExperimentSubject.findOne().recommendationMatrix;
    },
    'missingAnswers' : function(){
        reminderDep.depend();
        return isMissing == 1;
    }
});

Template.task3SurveyThree.events({
    'click #continue': function(event) {
        var table = document.getElementById('all-modules-table');
        var results = [];
        var missing = [];
        for(var i = 0; i < table.rows.length; i++){
            var col1 = table.rows[i].getElementsByClassName('surveyCheckbox')[0];
            var col2 = document.getElementsByName('RadioGroup1' + (i+1));
            var col3 = document.getElementsByName('RadioGroup2' + (i+1));
            var col4 = document.getElementsByName('dropdown' + (i+1))[0];
            var col5 = document.getElementsByName('RadioGroup3' + (i+1));

            var ans1 = 2;
            var ans2 = 2;
            var ans3 = 2;
            var ans4 = 0;
            var ans5 = 2;
            if(col1.checked){
                ans1 = 1;
            } else{
                ans1 = 0;
            }
            for(var j = 0; j < col2.length; j++){
                if(col2[j].checked){
                    ans2 = col2[j].value;
                }
            }
            for(var j = 0; j < col3.length; j++){
                if(col3[j].checked){
                    ans3 = col3[j].value;
                }
            }
            if(col4.value){
                if(col4.value == '1 = Not at all'){
                    ans4 = 1;
                } else if(col4.value == '5 = Very well'){
                    ans4 = 5;
                }else{
                    ans4 = col4.value;
                }
            }
             for(var j = 0; j < col5.length; j++){
                if(col5[j].checked){
                    ans5 = col5[j].value;
                }
            }
            if((ans2 == 2 || (ans3 == 2 && ans2 == 1) || ans4 == 0 || ans5 == 2) && ans1 == 0){
                missing.push(i);
            } else{
                results.push({answ1: ans1, answ2: ans2, answ3: ans3, answ4: ans4, answ5: ans5, moduleId: table.rows[i].getAttribute('data-moduleId')});
            }            
        }
        if(missing.length > 0){
            /*
            for(var j = 0; j < table.rows.length; j++){
                if(j%2 != 0){
                    table.rows[j].style.backgroundColor = '#f2f2f2';
                } else{
                    table.rows[j].style.backgroundColor = 'transparent';
                }   
            }
            for(var j = 0; j < missing.length; j++){
                table.rows[missing[j]].style.backgroundColor = '#FFDE46';
            }
            isMissing = 1;
            reminderDep.changed();
            */
        } else{
            document.getElementById('continue').value = 'sending...';
            document.getElementById('continue').disabled = true;
            Meteor.call('task3StoreSurvey3', results, function(error){
                if(error){
                    document.getElementById('continue').value = 'Submit';
                    document.getElementById('continue').disabled = false;
                    window.alert(error);
                } else{
                    Router.go('task3SurveyFour');  
                }

            })
        }        
    },
    'change .surveyCheckbox' : function(event){
        var rowId = parseInt(event.currentTarget.getAttribute('data-rowId'));
        var row = document.getElementById('all-modules-table').rows[rowId-1];

        if(row.getElementsByClassName('surveyCheckbox')[0].checked){
            var containers = row.getElementsByClassName('question-container');
            for(var i = 0; i < containers.length; i++){
                containers[i].style.visibility = 'hidden';
            }
        } else{
            var containers = row.getElementsByClassName('question-container');
            for(var i = 0; i < containers.length; i++){
                containers[i].style.visibility = 'visible';
            }            
        }

    },
    'click .getInfo' : function(event){
        var win = window.open(event.currentTarget.getAttribute('data-url'), '_blank');
        win.focus();
    },
    'change .rad1' : function(event){
        var rowId = parseInt(event.currentTarget.getAttribute('data-rowId'));
        var row = document.getElementById('all-modules-table').rows[rowId-1];
        var rads = row.getElementsByClassName('rad1');
        var didNotKnow = false;
        var didKnow = false;
        for(var i = 0; i < rads.length; i++){
            if(rads[i].checked && rads[i].value == 0){
                didNotKnow = true;
            }
        }
        if(didNotKnow){
            var container = row.getElementsByClassName('question-container-2')[0];
            container.style.visibility = 'hidden';
        } else{
            var container = row.getElementsByClassName('question-container-2')[0];
            container.style.visibility = 'visible';            
        }
    },
    'change .surveyCheckbox, change .rad1, change .rad2, change .rad3, change .sel1' : function(){
        var table = document.getElementById('all-modules-table');
        var results = [];
        var missing = [];
        for(var i = 0; i < table.rows.length; i++){
            var col1 = table.rows[i].getElementsByClassName('surveyCheckbox')[0];
            var col2 = document.getElementsByName('RadioGroup1' + (i+1));
            var col3 = document.getElementsByName('RadioGroup2' + (i+1));
            var col4 = document.getElementsByName('dropdown' + (i+1))[0];
            var col5 = document.getElementsByName('RadioGroup3' + (i+1));

            var ans1 = 2;
            var ans2 = 2;
            var ans3 = 2;
            var ans4 = 0;
            var ans5 = 2;
            if(col1.checked){
                ans1 = 1;
            } else{
                ans1 = 0;
            }
            for(var j = 0; j < col2.length; j++){
                if(col2[j].checked){
                    ans2 = col2[j].value;
                }
            }
            for(var j = 0; j < col3.length; j++){
                if(col3[j].checked){
                    ans3 = col3[j].value;
                }
            }
            if(col4.value){
                ans4 = col4.value;
            }
             for(var j = 0; j < col5.length; j++){
                if(col5[j].checked){
                    ans5 = col5[j].value;
                }
            }
            if((ans2 == 2 || (ans3 == 2 && ans2 == 1) || ans4 == 0 || ans5 == 2) && ans1 == 0){
                missing.push(i);
            } else{
                results.push({answ1: ans1, answ2: ans2, answ3: ans3, answ4: ans4, answ5: ans5, moduleId: table.rows[i].getAttribute('data-moduleId')});
            }            
        }
        if(missing.length == 0){
            isMissing = 0;
            reminderDep.changed();
        } else{
            isMissing = 1;
            reminderDep.changed();
        } 
    }
});
