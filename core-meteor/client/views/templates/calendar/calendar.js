var self = {};
self.loadCalendarEvents = function() {
    var modules = Modules.getSemesterPlanModules(self.semesterPlanId);
    var calendarEvents = [];
    var begin = moment().startOf('isoWeek').isoWeekday(1);
    var d = begin.date();
    var m = begin.month();
    var y = begin.year();
    var colors = ['#A7D0E6', '#88C6A6', '#4E9F96', '#037382', '#008CA2', '#0078BB', '#3E509C', '#685494', '#763C87'];
    _.each(modules, function(module, index) {
        var color = index < colors.length ? colors[index] : randomColor();
        _.each(module.lectures, function(lecture) {
            if (_.contains(module.selectedLectures, lecture.uzhId)) {
                var uniqueScheduleItems = _.uniq(lecture.scheduleItems, function(item) {
                    var startDatetime = moment(item.startDatetime);
                    var endDatetime = moment(item.endDatetime);
                    return startDatetime.weekday() + '' + startDatetime.hour() + '' + startDatetime.minute() + '' + endDatetime.weekday() + '' + endDatetime.hour() + '' + endDatetime.minute();
                });
                _.each(uniqueScheduleItems, function(scheduleItem) {
                    var startDatetime = moment(scheduleItem.startDatetime);
                    var endDatetime = moment(scheduleItem.endDatetime);
                    calendarEvents.push({
                        allDay: false,
                        start: new Date(y, m, d + startDatetime.isoWeekday() - 1, startDatetime.hour(), startDatetime.minute()),
                        end: new Date(y, m, d + endDatetime.isoWeekday() - 1, endDatetime.hour(), endDatetime.minute()),
                        title: lecture.title,
                        lectureId: lecture.uzhId,
                        scheduleItems: lecture.scheduleItems,
                        color: color
                    });
                });
            }
        });
    });
    return calendarEvents;
};

Template.calendar.refetchEvents = function() {
    $('#calendar').fullCalendar('refetchEvents');
};

Template.calendar.helpers({
    multipleLectures: function() {
        var hasLecturesToChoose = false;
        var modules = Modules.getSemesterPlanModules(this.semesterPlanId);
        _.each(modules, function(module) {
            var exercises = _.filter(module.lectures, function(lecture) {
                return lecture.lectureCategory !== 'Vorlesung';
            });
            if (exercises.length > 1) {
                hasLecturesToChoose = true;
            }
        });
        return hasLecturesToChoose;
    }
});

Template.calendar.rendered = function() {
    self.semesterPlanId = this.data.semesterPlanId;
    $('#calendar').fullCalendar({
        header: {
            left: '',
            center: '',
            right: ''
        },
        columnFormat: {
            month: 'ddd', // Mon
            week: 'dddd', // Mon
            day: 'dddd' // Monday
        },
        dayNames: [i18n('Sunday'), i18n('Monday'), i18n('Tuesday'), i18n('Wednesday'), i18n('Thursday'), i18n('Friday'), i18n('Saturday')],
        hiddenDays: [0],
        allDaySlot: false,
        axisFormat: 'HH:mm',
        slotEventOverlap: false,
        editable: false,
        firstDay: 1,
        defaultView: 'agendaWeek',
        height: 1500,
        eventTextColor: '#2E2E2E',
        events: function(start, end, callback) {
            callback(self.loadCalendarEvents());
        },
        timeFormat: '',
        minTime: 8,
        maxTime: 20,
        eventRender: function(event, element) {
            element.click(function() {
                //set the modal values and open
                $('#calendarModalLabel').html(event.title);
                $('#scheduleTable').empty();
                _.each(event.scheduleItems, function(schedule) {
                    var rooms = _.pluck(schedule.rooms, 'name').join(', ');
                    var persons = _.map(schedule.lecturers, function(lecturer) {
                        return lecturer.firstname + ' ' + lecturer.lastname;
                    }).join(', ');
                    Blaze.renderWithData(Template.scheduleItem, {
                        startDatetime: moment(schedule.startDatetime).format('DD.MM.YYYY HH:mm'),
                        endDatetime: moment(schedule.endDatetime).format('HH:mm'),
                        rooms: rooms,
                        persons: persons
                    }, document.getElementById('scheduleTable'));
                });
                $('#calendarModal').modal();
            });
        }
    });
};