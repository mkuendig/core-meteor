Template.title.helpers({
    translatedTitle: function () {
        if(this.i18nTitle) {
            return i18n(this.i18nTitle);
        } else {
            return this.title;
        }
    }
});