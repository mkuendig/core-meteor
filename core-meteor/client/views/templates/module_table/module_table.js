// The modules variable is set according to the modules that should be displayed
// The datagrid reads the modules from this variable when it is rendered
var self = {};
var categorySelectedDep = new Tracker.Dependency();


self.columns = [];
self.modules = [];
self.semesterId = '';
self.checkedModules = [];
self.selectedStudyField = '';
self.orgsFilter = [];
self.ectsFilter = [];
self.semesterFilter = '';
self.weekdaysFilter = [];
self.lecturersFilter = [];
self.moduleCategoryFilter = [];

self.resetFilters = function() {
    self.orgsFilter = [];
    self.ectsFilter = [];
    self.semesterFilter = '';
    self.weekdaysFilter = [];
    self.lecturersFilter = [];
    self.moduleCategoryFilter = [];
};


self.moduleTable = {
    columns: function() {
        var availableColumns = {
            'moduleId': {
                label: i18n('moduleId'),
                property: 'detail',
                sortable: true
            },
            'moduleIdWizard': {
                label: i18n('moduleId'),
                property: 'moduleId',
                sortable: true
            },
            'title': {
                label: i18n('title'),
                property: 'title',
                sortable: true
            },
            'ects': {
                label: i18n('ects'),
                property: 'ects',
                sortable: true
            },

            'rating': {
                label: i18n('rating') + '<i class="fa fa-question-circle m-l-md pointer" data-toggle="tooltip" data-placement="left" title="' + i18n('toolTipRating') + '">',
                property: 'ratingColumn'
            },
            'prediction': {
                label: i18n('predictionShort'),
                property: 'prediction',
                sortable: true
            },
            'moduleSelector': {
                label: '',
                property: 'selectionColumn'
            },
            'moduleSelectorWizard': {
                label: '',
                property: 'selectionColumnWizard'
            }
        };
        var displayedColumns = [];
        _.each(self.columns, function(column) {
            displayedColumns.push(availableColumns[column]);
        });
        return displayedColumns;
    },

    data: function(options, callback) {
        _.each(self.modules, function(module) {
            module.lecturerString = ''; // to enable searching for lecturers
            _.each(module.lecturers, function(lecturer) {
                module.lecturerString += lecturer.firstname + ' ' + lecturer.lastname + ' ';
            });
            var moduleRating = RatingController.getRating(module);
            var prediction = PredictionController.getPrediction(module);
            module.ratingColumn = '<div class="rateit svg pointer" id="' + module.moduleId + '" data-rateit-value="' + moduleRating + '" data-rateit-ispreset="true" data-toggle="tooltip" data-placement="left" data-original-title="' + i18n('tootltipStars') + '"></div>';
            module.detail = '<a href="/module-detail/' + module.moduleId + '">' + module.moduleId + '</a>';
            if (_.contains(self.checkedModules, module.moduleId)) {
                module.selectionColumn = '<i class="fa fa-check text-success pointer addModule study-planer-checkbox" data-module-id="' + module.moduleId + '" data-toggle="tooltip" data-placement="right" data-original-title="' + i18n('tooltipDelete') + '"/>';
                module.selectionColumnWizard = '<i class="fa fa-check text-success pointer addModule module-selection-checkbox" data-module-id="' + module.moduleId + '" data-toggle="tooltip" data-placement="right" data-original-title="' + i18n('tooltipDelete') + '" />';
            } else {
                module.selectionColumn = '<i class="fa fa-plus pointer addModule study-planer-checkbox" data-module-id="' + module.moduleId + '" data-toggle="tooltip" data-placement="right" data-original-title="' + i18n('tooltipAdd') + '" />';
                module.selectionColumnWizard = '<i class="fa fa-plus pointer addModule module-selection-checkbox" data-module-id="' + module.moduleId + '" data-toggle="tooltip" data-placement="right" data-original-title="' + i18n('tooltipAdd') + '" />';
            }
            module.prediction = (function() {
                if (moduleRating) {
                    return '<div data-toggle="tooltip" data-placement="left" data-original-title="' + i18n('yourRating') + moduleRating + '"> <div class="' + moduleRating + ' rateit svg readonly rated" id="' + module.moduleId + '" data-rateit-value="' + moduleRating + '" data-rateit-ispreset="true" data-rateit-readonly="true"></div></div>';
                } else {
                    if (prediction > 0) {
                        return '<div data-toggle="tooltip" data-placement="left" data-original-title="' + i18n('prediction') + UIHelper.userName() + ': ' + prediction + '"> <div class="' + prediction + ' rateit svg readonly" id="' + module.moduleId + '" data-rateit-value="' + prediction + '" data-rateit-ispreset="true" data-rateit-readonly="true"></div></div>';
                    }
                    return '<div data-toggle="tooltip" data-placement="left" data-original-title="' + i18n('noPrediction') + '"> <div class="' + prediction + ' rateit svg readonly" id="' + module.moduleId + '" data-rateit-value="' + prediction + '" data-rateit-ispreset="true" data-rateit-readonly="true"></div></div>';
                }
            }());
        });
        var modulesSubset = self.modules.slice(0); // clone modules array

        // SEARCHING
        if (options.search) {
            modulesSubset = _.filter(modulesSubset, function(module) {
                var match = false;
                _.each(module, function(prop) {
                    if (_.isString(prop) || _.isFinite(prop)) {
                        if (prop.toString().toLowerCase().indexOf(options.search.toLowerCase()) !== -1) {
                            match = true;
                        }
                    }
                });
                return match;
            });
        }

        // FILTERING
        if (self.orgsFilter.length > 0) {
            modulesSubset = FilterController.filterByOrganizationalUnit(modulesSubset, self.orgsFilter);
        }
        if (self.ectsFilter.length > 0) {
            modulesSubset = FilterController.filterByEcts(modulesSubset, self.ectsFilter);
        }
        if (self.semesterFilter) {
            modulesSubset = FilterController.filterBySemester(modulesSubset, self.semesterFilter);
        }
        if (self.weekdaysFilter.length > 0) {
            modulesSubset = FilterController.filterByWeekdays(modulesSubset, self.weekdaysFilter);
        }
        if (self.lecturersFilter.length > 0) {
            modulesSubset = FilterController.filterByLecturers(modulesSubset, self.lecturersFilter);
        }
        if (self.moduleCategoryFilter.length > 0) {
            modulesSubset = FilterController.filterByModuleCategories(modulesSubset, [self.moduleCategoryFilter]);
        }
        if (self.selectedStudyField) {
            modulesSubset = FilterController.filterByModuleCategories(modulesSubset, [self.selectedStudyField]);
        }
        var count = modulesSubset.length;

        // SORTING
        if (options.sortProperty) {
            modulesSubset = _.sortBy(modulesSubset, options.sortProperty);
            if (options.sortDirection === 'desc') {
                modulesSubset.reverse();
            }
        } else {
            modulesSubset = _.sortBy(modulesSubset, 'title');
        }

        // PAGING
        var startIndex = options.pageIndex * options.pageSize;
        var endIndex = startIndex + options.pageSize;
        var end = (endIndex > count) ? count : endIndex;
        var pages = Math.ceil(count / options.pageSize);
        var page = options.pageIndex + 1;
        var start = startIndex + 1;

        callback({
            data: modulesSubset.slice(startIndex, endIndex),
            start: start,
            end: end,
            count: count,
            pages: pages,
            page: page
        });
    }
};

Template.moduleTable.created = function() {
    var semesterId = Session.get('semesterId');
    if (semesterId) {
        self.semesterFilter = semesterId.substr(0, 2).toUpperCase();
    }
};

Template.moduleTable.destroyed = function() {
    Session.set('moduleTableUrl', window.location.pathname);
};

Template.moduleTable.setCheckedModules = function(checkedModules) {
    self.checkedModules = checkedModules;
};

Template.moduleTable.resetTable = function(modulesSubset, columnSubset, semesterId, checkedModules) {
    self.modules = modulesSubset;
    self.columns = columnSubset;
    self.semesterId = semesterId ? semesterId : '';
    self.checkedModules = checkedModules ? checkedModules : [];
    self.resetFilters();
};

Template.moduleTable.rendered = function() {
    var moduleTableDiv = $('#module-table');
    moduleTableDiv.datagrid({
        dataSource: self.moduleTable,
        itemsText: i18n('itemsText'),
        itemText: i18n('itemText')
    });
    moduleTableDiv.datagrid('reload');
    moduleTableDiv.bind('loaded', function() {
        UIHelper.refreshRateit();
        $('[data-toggle="tooltip"]').tooltip();
    });
    UIHelper.refreshRateit();
    $('[data-toggle="tooltip"]').tooltip();
};

Template.moduleTable.events({
    'click .study-planer-checkbox': function(event) {
        var targetElement = $(event.target);
        var semesterPlanId = Session.get('semesterPlanId');
        var moduleId = targetElement.attr('data-module-id');
        if (targetElement.hasClass('text-success')) {
            targetElement.attr('data-original-title', i18n('tooltipAdd'));
            targetElement.removeClass('text-success');
            targetElement.removeClass('fa-check').addClass('fa-plus');
            Meteor.call('removeModuleFromSemesterPlan', semesterPlanId, moduleId);
            Toast.success(i18n('moduleRemovedFromSemester'));
            self.checkedModules = _.without(self.checkedModules, moduleId);
        } else {
            targetElement.attr('data-original-title', i18n('tooltipDelete'));
            targetElement.addClass('text-success');
            targetElement.removeClass('fa-plus').addClass('fa-check');
            Meteor.call('addModuleToSemesterPlan', semesterPlanId, moduleId);
            Toast.success(i18n('moduleAddedToSemester'));
            self.checkedModules.push(moduleId);
        }
    },

    'click .module-selection-checkbox': function(event) {
        var targetElement = $(event.target);
        var moduleId = targetElement.attr('data-module-id');
        Meteor.call('toggleRating', moduleId);
        if (targetElement.hasClass('text-success')) {
            targetElement.removeClass('text-success');
            targetElement.removeClass('fa-check').addClass('fa-plus');
            Toast.success(i18n('user_module_relations_selection_deleted'));
            self.checkedModules = _.without(self.checkedModules, moduleId);
        } else {
            targetElement.addClass('text-success');
            targetElement.removeClass('fa-plus').addClass('fa-check');
            Toast.success(i18n('user_module_relations_selection_added'));
            self.checkedModules.push(moduleId);
        }
    },

    'change #filter-orgs': function(event) {
        self.orgsFilter = event.val;
        $('#module-table').datagrid('reload');
    },

    'change #filter-ects': function(event) {
        var ectsList = [];
        _.each(event.val, function(val) {
            ectsList.push(parseFloat(val));
        });
        self.ectsFilter = _.uniq(ectsList);
        $('#module-table').datagrid('reload');
    },

    'change #filter-semesters': function(event) {
        self.semesterFilter = event.val;
        $('#module-table').datagrid('reload');
    },

    'change #filter-weekdays': function(event) {
        var weekdaysFilter = [];
        _.each(event.val, function(val) {
            weekdaysFilter.push(parseFloat(val));
        });
        self.weekdaysFilter = weekdaysFilter;
        $('#module-table').datagrid('reload');
    },

    'change #filter-lecturers': function(event) {
        self.lecturersFilter = event.val;
        $('#module-table').datagrid('reload');
    },

    'change #filter-study-field': function(event) {
        self.selectedStudyField = event.val;
        categorySelectedDep.changed();
        self.moduleCategoryFilter = [];
        $('#module-table').datagrid('reload');
    },

    'change #filter-module-categories': function(event) {
        self.moduleCategoryFilter = event.val;
        $('#module-table').datagrid('reload');
    }
});

Template.moduleTable.helpers({

    nofilters: function() {
        if (this.nofilters) {
            return true;
        }
        return false;
    },

    filterSemesterSelector: function() {
        return Select2Helper.filterSemesterSelector();
    },

    filterSemesterSelectorOptions: function() {
        return Select2Helper.filterSemesterSelectorOptions(self.semesterFilter);
    },

    filterOrgUnitSelector: function() {
        return Select2Helper.filterOrgUnitSelector();
    },

    filterOrgUnitSelectorOptions: function() {
        return Select2Helper.filterOrgUnitSelectorOptions(self.orgsFilter);
    },

    filterEctsSelector: function() {
        return Select2Helper.filterEctsSelector();
    },

    filterEctsSelectorOptions: function() {
        return Select2Helper.filterEctsSelectorOptions(this.ectsFilter);
    },

    filterWeekdaysSelector: function() {
        return Select2Helper.filterWeekdaysSelector();
    },

    filterWeekdaysSelectorOptions: function() {
        return Select2Helper.filterWeekdaysSelectorOptions(self.weekdaysFilter);
    },

    filterLecturersSelector: function() {
        return Select2Helper.filterLecturersSelector();
    },

    filterLecturersSelectorOptions: function() {
        return Select2Helper.filterLecturersSelectorOptions(self.lecturersFilter);
    },

    filterStudyFieldSelector: function() {
        return Select2Helper.filterStudyFieldSelector();
    },

    filterStudyFieldSelectorOptions: function() {
        categorySelectedDep.depend();
        return Select2Helper.filterStudyFieldSelectorOptions(self.selectedStudyField);
    },

    filterModuleCategorySelector: function() {
        return Select2Helper.filterModuleCategorySelector();
    },

    filterModuleCategorySelectorOptions: function() {
        categorySelectedDep.depend();
        return Select2Helper.filterModuleCategorySelectorOptions(self.selectedStudyField);
    },

    studyFieldSelected: function() {
        categorySelectedDep.depend();
        return self.selectedStudyField !== '';
    },

    i18nSearch: function() {
        return i18n('search');
    }
});