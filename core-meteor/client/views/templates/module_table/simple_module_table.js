var starsGolden = false;

Template.simpleModuleTable.created = function() {
    if (this.data.starsGolden) {
        starsGolden = true;
    }
};

Template.simpleModuleTable.helpers({
    avgRating: function() {
        return RatingController.getAvgRating(this);
    },

    numberRatings: function() {
        return RatingController.getNumberRatings(this);
    },

    i18nratings: function() {
        if (RatingController.getNumberRatings(this) < 2) {
            return i18n('rating');
        } else {
            return i18n('ratings');
        }
    },

    golden: function() {
        return starsGolden;
    }
});

Template.simpleModuleTable.rendered = function() {
    $('[data-toggle="tooltip"]').tooltip();
};