Template.comment.helpers({
    createdAt: function() {
        return moment(this.createdAt).format('DD.MM.YYYY HH:mm');
    },

    positiveRating: function() {
        return this.upvotes.length - this.downvotes.length > -1;
    },

    rating: function() {
        return this.upvotes.length - this.downvotes.length;
    },

    upvoted: function() {
        return _.contains(this.upvotes, Meteor.userId()) ? 'bg-success' : '';
    },

    downvoted: function() {
        return _.contains(this.downvotes, Meteor.userId()) ? 'bg-danger' : '';
    },

    username: function() {
        return Meteor.users.findOne(this.createdById).profile.name;
    }, 

    getRating: function(rating) {
        return Math.abs(rating);
    }

});

Template.comment.events({
    'click #upvote-icon': function() {
        if (_.contains(this.upvotes, Meteor.userId())) {
            Meteor.call('removeUpvoteFromComment', this._moduleId, this.createdById);
        } else {
            Meteor.call('upvoteComment', this._moduleId, this.createdById);
        }
    },

    'click #downvote-icon': function() {
        if (_.contains(this.downvotes, Meteor.userId())) {
            Meteor.call('removeDownvoteFromComment', this._moduleId, this.createdById);
        } else {
            Meteor.call('downvoteComment', this._moduleId, this.createdById);
        }
    }
});

Template.editCommentModal.events({
    'click #edit-comment-button': function() {
        Meteor.call('editComment', this._moduleId, Template.wysiwygEditor.getContent());
    }
});

Template.removeCommentModal.events({
    'click #remove-comment-button': function() {
        var _moduleId = this._moduleId;
        // wait 500ms to let the modal close
        setTimeout(function() {
            Meteor.call('removeComment', _moduleId);
        }, 500);
    }
});