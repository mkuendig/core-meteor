var currentStudyProfile = {
    faculty: 'wwf'
};

var currentFieldSelected = false;
var futureFieldSelected = false;

var savedCurrentProfile;


Template.studyProfileTemplate.rendered = function() {
    Meteor.call('getFutureDegree', function(error, result) {
        if(error){
            window.alert(error)
        } else{

    savedCurrentProfile = UserProfileController.getCurrentProfile();
    if (savedCurrentProfile && savedCurrentProfile.faculty !== 'other') {
        currentStudyProfile.studyCourse = savedCurrentProfile.studyCourse;
        currentStudyProfile.level = savedCurrentProfile.level;
        currentStudyProfile.field = savedCurrentProfile.field;
        Blaze.renderWithData(Template.studyCourseSelector, {
            faculty: currentStudyProfile.faculty,
            id: 'current-study-course-selector',
            selected: savedCurrentProfile.studyCourse
        }, document.getElementById('current-dropdown'));
        Blaze.renderWithData(Template.levelSelector, {
            studyCourse: currentStudyProfile.studyCourse,
            id: 'current-level-selector',
            selected: savedCurrentProfile.level
        }, document.getElementById('current-dropdown'));
        Blaze.renderWithData(Template.fieldSelector, {
            level: currentStudyProfile.level,
            id: 'current-field-selector',
            selected: savedCurrentProfile.field
        }, document.getElementById('current-dropdown'));
        currentFieldSelected = true;
    } else {
        Blaze.renderWithData(Template.studyCourseSelector, {
            faculty: currentStudyProfile.faculty,
            id: 'current-study-course-selector'
        }, document.getElementById('current-dropdown'));
    }
            if(typeof result !== "undefined" && result !== "undefined") {
                $('#futureDropDown').select2("val", result);
                Session.set('futureDegree', result);
                futureFieldSelected = true;
                if(currentFieldSelected && futureFieldSelected){
                    Template.registrationStudyInformation.enableButtons();
                }
            }
        }
    });

};

Template.studyProfileTemplate.events({

    'change #current-dropdown #current-faculty-selector': function(event) {
        $('#current-dropdown .faculty-selector').nextAll().remove();
        currentStudyProfile.faculty = event.val;
        if (event.val === 'other') {
            currentStudyProfile.level = 'other';
            currentStudyProfile.field = 'other';
            currentStudyProfile.studyCourse = 'other';
        } else {
            delete currentStudyProfile.level;
            delete currentStudyProfile.field;
            delete currentStudyProfile.studyCourse;
            Blaze.renderWithData(Template.studyCourseSelector, {
                faculty: currentStudyProfile.faculty,
                id: 'current-study-course-selector'
            }, document.getElementById('current-dropdown'));
            Template.registrationStudyInformation.disableButtons();
        }
    },

    'change #current-dropdown #current-study-course-selector': function(event) {
        $('#current-dropdown .study-course-selector').nextAll().remove();
        delete currentStudyProfile.level;
        delete currentStudyProfile.field;
        currentStudyProfile.studyCourse = event.val;
        Blaze.renderWithData(Template.levelSelector, {
            studyCourse: currentStudyProfile.studyCourse,
            id: 'current-level-selector'
        }, document.getElementById('current-dropdown'));
        Template.studyProfileWizard.disableButtons();
    },

    'change #current-dropdown #current-level-selector': function(event) {
        $('#current-dropdown .level-selector').nextAll().remove();
        delete currentStudyProfile.field;
        currentStudyProfile.level = event.val;
        Blaze.renderWithData(Template.fieldSelector, {
            level: currentStudyProfile.level,
            id: 'current-field-selector'
        }, document.getElementById('current-dropdown'));
        Template.studyProfileWizard.disableButtons();
    },

    'change #current-dropdown #current-field-selector': function(event) {
        currentStudyProfile.field = event.val;
        currentFieldSelected = true;
        if(currentFieldSelected && futureFieldSelected){
            Template.registrationStudyInformation.enableButtons();
        }
    }
});

Template.futureLevelSelector.events({
    'change #futureDropDown': function(event){
        Session.set('futureDegree', event.val);
        futureFieldSelected = true;
        if(currentFieldSelected && futureFieldSelected){
            Template.registrationStudyInformation.enableButtons();
        }
    }

});

Template.studyProfileTemplate.saveProfile = function() {
    UserProfileController.updateCurrentProfile(currentStudyProfile);
};

Template.studyProfileTemplate.updateFutureDegree = function(degree) {
    UserProfileController.updateFutureDegree(degree);
};

Template.facultySelector.helpers({
    faculties: function() {
        var wwf = Categories.findOne({
            slug: 'wwf'
        });
        return [{
            title: wwf.title,
            slug: wwf.slug,
            selected: true
        }];
    },
    i18nFaculty: function() {
        return i18n('faculty');
    }
});

Template.studyCourseSelector.helpers({
    studyCourse: function() {
        var directChildren = Categories.getDirectChildren(this.faculty);
        if (this.selected) {
            var selected = this.selected;
            _.each(directChildren, function(directChild) {
                if (directChild.slug === selected) {
                    directChild.selected = true;
                }
            });
        }
        return directChildren;
    },
    i18nStudyCourse: function() {
        return i18n('studyCourse');
    }
});

Template.levelSelector.helpers({
    levels: function() {
        var directChildren = Categories.getDirectChildren(this.studyCourse);
        if (this.selected) {
            var selected = this.selected;
            _.each(directChildren, function(directChild) {
                if (directChild.slug === selected) {
                    directChild.selected = true;
                }
            });
        }
        return directChildren;
    },
    i18nLevel: function() {
        return i18n('level');
    }
});

Template.fieldSelector.helpers({
    fields: function() {
        var directChildren = Categories.getDirectChildren(this.level);
        if (this.selected) {
            var selected = this.selected;
            _.each(directChildren, function(directChild) {
                if (directChild.slug === selected) {
                    directChild.selected = true;
                }
            });
        }
        return directChildren;
    },
    i18nField: function() {
        return i18n('field');
    }
});

Template.futureLevelSelector.helpers({
    levels: function() {
        return [{
            title: 'Bachelor',
            slug: 'Bachelor'
        }, {
            title: 'Master',
            slug: 'Master'
        }];
    }
});