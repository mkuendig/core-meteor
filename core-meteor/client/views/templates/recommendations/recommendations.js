Template.recommendations.created = function() {
    this.defaultModuleTypeFilter = new ReactiveVar(this.data.filter);
    this.moduleTypeFilter = new ReactiveVar(this.data.filter);
    this.numberOfRecommendations = new ReactiveVar(this.data.numberOfRecommendations);
    this.ectsFilter = new ReactiveVar([]);
    this.weekdaysFilter = new ReactiveVar([]);
};

Template.predictionColumn.rendered = function() {
    UIHelper.refreshRateit();
    $('[data-toggle="tooltip"]').tooltip();
};

Template.recommendations.helpers({

    modules: function() {
        var topNRecs = Modules.getTopRecommendations().fetch();

        // Hide bachelor modules for master students
        if (UserProfileController.getCurrentProfile().level.substr(0, 3) === 'msc') {
            topNRecs = FilterController.filterBscModules(topNRecs);
        }

        var moduleTypeFilter = Template.instance().moduleTypeFilter.get();
        if (!_.isUndefined(moduleTypeFilter)) {
            topNRecs = FilterController.filterByModuleCategories(topNRecs, [moduleTypeFilter]);
        }

        var ectsFilter = Template.instance().ectsFilter.get();
        if (ectsFilter.length > 0) {
            topNRecs = FilterController.filterByEcts(topNRecs, ectsFilter);
        }

        var weekdaysFilter = Template.instance().weekdaysFilter.get();
        if (weekdaysFilter.length > 0) {
            topNRecs = FilterController.filterByWeekdays(topNRecs, weekdaysFilter);
        }

        var n = Template.instance().numberOfRecommendations.get();
        return topNRecs.slice(0, n);
    },

    prediction: function() {
        return PredictionController.getPrediction(this);
    },

    isUpdating: function() {
        return Session.get('isUpdatingTopPredictions');
    },

    getTitlePrediction: function() {
        var userName = UIHelper.userName();
        if (_.isEmpty(userName)) {
            userName = i18n('you');
        }
        return i18n('prediction') + userName;
    },

    showLessButton: function() {
        var numberOfRecommendations = Template.instance().numberOfRecommendations.get();
        if (numberOfRecommendations && numberOfRecommendations > 10) {
            return true;
        }
        return false;
    },

    filterWeekdaysSelector: function() {
        return Select2Helper.filterWeekdaysSelector();
    },

    filterWeekdaysSelectorOptions: function() {
        var selectedWeekdays = Template.instance().weekdaysFilter.get();
        return Select2Helper.filterWeekdaysSelectorOptions(selectedWeekdays);
    },

    filterModuleCategorySelector: function() {
        return Select2Helper.filterModuleCategorySelector();
    },

    filterModuleCategorySelectorOptions: function() {
        var studyField = Meteor.user().profile.studyProfile.current.field;
        return Select2Helper.filterModuleCategorySelectorOptions(studyField);
    },

    filterEctsSelector: function() {
        return Select2Helper.filterEctsSelector();
    },

    filterEctsSelectorOptions: function() {
        return Select2Helper.filterEctsSelectorOptions();
    }
});

Template.recommendations.rendered = function() {
    UIHelper.refreshRateit();
};

Template.recommendations.events = {

    'click .not-interested-button': function(event) {
        var moduleId = $(event.target).attr('data-module-id');
        Meteor.call('markModuleAsNotInterested', moduleId);
        $(event.target).parent().parent().remove();
        Toast.success(i18n('markedAsNotInterested'));
    },

    'click #show-more': function(event, template) {
        template.numberOfRecommendations.set(template.numberOfRecommendations.get() + 5);
    },

    'click #show-less': function(event, template) {
        template.numberOfRecommendations.set(template.numberOfRecommendations.get() - 5);
    },

    'click .addModule': function(event) {
        var semesterPlanId = Session.get('semesterPlanId');
        var moduleId = $(event.target).attr('data-module-id');
        Meteor.call('addModuleToSemesterPlan', semesterPlanId, moduleId);
        var badge = $('.badgePlannedModules');
        badge.fadeOut().fadeIn();
        $(event.target).parent().parent().remove();
        Toast.success(i18n('moduleAddedToSemester'));
    },

    'change #filter-ects': function(event, template) {
        var ectsList = [];
        _.each(event.val, function(val) {
            ectsList.push(parseFloat(val));
        });
        template.ectsFilter.set(_.uniq(ectsList));
    },

    'change #filter-weekdays': function(event, template) {
        var weekdaysList = [];
        _.each(event.val, function(val) {
            weekdaysList.push(parseInt(val, 10));
        });
        template.weekdaysFilter.set(weekdaysList);
    },

    'change #filter-module-categories': function(event, template) {
        var moduleTypeFilter = event.val === '' ? template.defaultModuleTypeFilter.get() : event.val;
        template.moduleTypeFilter.set(moduleTypeFilter);
    }
};
