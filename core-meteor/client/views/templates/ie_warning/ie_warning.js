Template.ieWarning.helpers({
    isIE: function () {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');
        return (msie > 0 || trident > 0);
    }
});