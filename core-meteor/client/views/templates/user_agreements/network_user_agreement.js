Template.networkUserAgreement.events({
    'click #acceptButton': function() {
        UserAgreementController.acceptCurrentNetworkUserAgreement();
    },

    'click #denyButton': function() {
        UserAgreementController.denyNetworkUserAgreement();
    }
});
