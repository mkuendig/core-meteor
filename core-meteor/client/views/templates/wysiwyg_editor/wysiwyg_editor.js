Template.wysiwygEditor.rendered = function() {
    $('#wysiwyg-editor').wysiwyg();
    $('#wysiwyg-editor').html(this.data.content);
};

Template.wysiwygEditor.getContent = function() {
    return $('#wysiwyg-editor').html();
};