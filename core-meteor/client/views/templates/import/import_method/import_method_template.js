Template.importMethodTemplate.events({
    'click #step2-next-copyPaste': function() {
        if (this.location === 'registration') {
            Router.go('registrationCopyPasteImport');
        } else {
            Router.go('copyPasteImportProfile');
        }

    },

    'click #step2-next-manualSelection': function() {
        if (this.location === 'registration') {
            Router.go('registrationModuleSelection');
        } else {
            Router.go('moduleSelectionProfile');
        }
    }

});

Template.importMethodTemplate.helpers({
    i18nCopyRecordButton: function() {
        return i18n('copyRecordButton');
    },
    i18nManualSelectionButton: function() {
        return i18n('manualSelectionButton');
    }
});

Template.importMethodTemplate.rendered = function() {
    $('.easypiechart').easyPieChart();
};