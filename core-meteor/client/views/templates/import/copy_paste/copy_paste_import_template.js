var showPreview = function() {
    // get the value of the textfield and parse it
    var modulesString = $('#summaryOfCredits').val();
    var studentRecordsParser = new StudentRecordsParser();
    var modules = studentRecordsParser.parseModulesString(modulesString);

    // filter out all unknown modules
    var recognisedModules = studentRecordsParser.filterUnknownModules(modules);

    // deactivate the save button if there is no module recognised
    if (recognisedModules.length < 1) {
        $('#save').attr('disabled', 'true');
    } else {
        $('#save').removeAttr('disabled');
    }

    // extract all unknown modules
    var unknownModules = _.difference(modules, recognisedModules);

    $('#unknown-modules').empty();
    $('#recognised-modules').empty();

    // styling issues
    var recordCopyPasteColumn = $('.record-copy-paste-column');
    recordCopyPasteColumn.removeClass('col-md-12');
    recordCopyPasteColumn.addClass('col-md-8');
    var recordCopyPastePreviewColumn = $('.record-copy-paste-preview-column');
    recordCopyPastePreviewColumn.removeClass('hidden');

    // preview of the known modules
    _.each(recognisedModules, function(module) {
        Blaze.renderWithData(Template.recordCopyPastePreview, {
            module: module,
            includeMarks: false
        }, document.getElementById('recognised-modules'));
    });

    // preview of the unknown modules
    _.each(unknownModules, function(module) {
        Blaze.renderWithData(Template.recordCopyPastePreview, {
            module: module,
            includeMarks: false
        }, document.getElementById('unknown-modules'));
    });
};

/**Importer Template**/
Template.copyPasteImportTemplate.helpers({
    i18nInsertArea: function() {
        return i18n('insertArea');
    },

    studentRecordUrl: function() {
        if (Session.get('language') === 'en') {
            return 'http://www.students.uzh.ch/en/record.html';
        } else {
            return 'http://www.students.uzh.ch/de/record.html';
        }
    }
});

Template.copyPasteImportTemplate.save = function() {
    // textarea with record string
    var modulesString = $('#summaryOfCredits').val();
    // parse string
    var studentRecordsParser = new StudentRecordsParser();
    var modules = studentRecordsParser.parseModulesString(modulesString);
    if (typeof modules !== 'undefined') {
        studentRecordsParser.saveEntries(modules);
        Meteor.call('logImportMethod', 'Imported ' + modules.length + ' via copy paste', Meteor.userId(), new Date().toString());
    }
};

Template.copyPasteImportTemplate.events({
    'paste #summaryOfCredits, keyup #summaryOfCredits': function() {
        showPreview();
    }
});