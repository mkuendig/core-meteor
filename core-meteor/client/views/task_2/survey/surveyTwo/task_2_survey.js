var missing = 0;
var reminderDep = new Tracker.Dependency();

Template.task2Survey.rendered = function() {
    if(ExperimentSubject.find().fetch().length == 0 || ExperimentSubject.findOne().matchPartnerId == undefined || ExperimentSubject.findOne().completedRegistration == 0){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().Task2Stage > 5){
        Router.go('task2Complete');
    }
    Meteor.call('task2SurveyTwoStart', Meteor.userId());
    missing = 0;
    reminderDep.changed();
}

Template.task2Survey.helpers({
    'matchFirstName' : function(){
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
    }
});

Template.task2Survey.events({
    'click #continue': function(event) {
            document.getElementById('continue').value = 'sending...';
            document.getElementById('continue').disabled = true;
           
            answ4 = 'none';
            answ5 = 'none';
            answ6 = 'none';
            answ7 = 'none';
            answ8 = 'none';
            answ9 = 'none';
            if(document.getElementById('element10').value){
                answ4 = document.getElementById('element10').value;
            }
            if(document.getElementById('element11').value){
                answ5 = document.getElementById('element11').value;
            }
            if(document.getElementById('element12').value){
                answ6 = document.getElementById('element12').value;
            }
            if(document.getElementById('element13').value){
                answ7 = document.getElementById('element13').value;
            }
            if(document.getElementById('element14').value){
                answ8 = document.getElementById('element14').value;
            }
            if(document.getElementById('element15').value){
                answ9 = document.getElementById('element15').value;
            }
            Meteor.call('storeSurvey2Two', answ4, answ5, answ6, answ7, answ8, answ9, function(error){
                if(error){
                    document.getElementById('continue').value = 'Submit';
                    document.getElementById('continue').disabled = false;
                    window.alert(error);
                } else{
                    Meteor.call('updateTask2State', 6, function(error){
                        if(error){
                            window.alert(error);
                        }
                            Router.go('task2Complete');
                    });
                    
                }
            });
            
    }
});
