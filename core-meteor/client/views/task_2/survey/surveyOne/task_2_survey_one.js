var missing = 0;
var reminderDep = new Tracker.Dependency();

Template.task2SurveyOne.rendered = function() {
	missing = 0;
	reminderDep.changed();
    if(ExperimentSubject.find().fetch().length == 0 || ExperimentSubject.findOne().matchPartnerId == undefined || ExperimentSubject.findOne().completedRegistration == 0){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().Task2Stage > 5){
        Router.go('task2Complete');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 4){
        Router.go('task2Survey');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 3){
        Router.go('task2RecommendModules');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 2){
        Router.go('task2Tutorial');
    }
    Meteor.call('task2SurveyOneStart', Meteor.userId());
}

Template.task2SurveyOne.helpers({
    'matchFirstName' : function(){
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
    },
    'matchFullName' : function(){
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
    },
    'missingAnswers' : function(){
    	reminderDep.depend();
    	return missing == 1;
    }
});

Template.task2SurveyOne.events({
    'click #continue': function(event) {
    	var answ1, answ2, answ3;
    	var radio1 = document.getElementsByName('RadioGroupOne');
    	for(var i = 0; i < radio1.length; i++){
		    if(radio1[i].checked){
		        answ1 = radio1[i].value;
		    }
		}
    	radio1 = document.getElementsByName('RadioGroupTwo');
    	for(var i = 0; i < radio1.length; i++){
		    if(radio1[i].checked){
		        answ2 = radio1[i].value;
		    }
		}
    	radio1 = document.getElementsByName('RadioGroupThree');
    	for(var i = 0; i < radio1.length; i++){
		    if(radio1[i].checked){
		        answ3 = radio1[i].value;
		    }
		}
		if(answ1 && answ2 && answ3){
            document.getElementById('continue').value = 'sending...';
            document.getElementById('continue').disabled = true;
           
			Meteor.call('storeSurvey2One', answ1, answ2, answ3, function(error){
                if(error){
                    document.getElementById('continue').value = 'Submit';
                    document.getElementById('continue').disabled = false;
                    window.alert(error);
                } else{
                    Meteor.call('updateTask2State', 3, function(error){
                        if(error){
                            window.alert(error);
                        }
                        Router.go('task2Tutorial');
                    });
                    
                }
            });
		} else{
			missing = 1;
			reminderDep.changed();
		}
    }
});
