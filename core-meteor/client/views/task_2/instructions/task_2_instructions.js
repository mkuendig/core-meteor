Template.task2Instructions.rendered = function() {
    if(ExperimentSubject.find().fetch().length == 0 || ExperimentSubject.findOne().matchPartnerId == undefined || ExperimentSubject.findOne().completedRegistration == 0){
        if(!Session.get('debug')){
            Router.go('notAllowed');
        }
    }
    if(ExperimentSubject.findOne().Task2Stage > 5){
        Router.go('task2Complete');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 4){
        Router.go('task2Survey');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 3){
        Router.go('task2RecommendModules');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 2){
        Router.go('task2Tutorial');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 1){
        Router.go('task2SurveyOne');
    }
}
Template.task2Instructions.helpers({
    'firstName' : function(){
        if(Meteor.user().profile.callName && Meteor.user().profile.callName.split(' ')[0]){
            return Meteor.user().profile.callName.split(' ')[0];
        } else{
            return 'participant';
        }
    },
    'matchName' : function(){
        if(ExperimentSubject.find().fetch()[0] && ExperimentSubject.find().fetch()[0].matchPartnerCallName){
            return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
        } else{
            return 'none';
        }   
    },
    'matchFirstName': function(){
        if(ExperimentSubject.find().fetch()[0] && ExperimentSubject.find().fetch()[0].matchPartnerCallName){
            if(ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0]){
                return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
            } else{
                return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
            }
        } else{
            return 'none';
        }   
    },
    'profilePictureLink' : function(){
        var fbId;
        if(ExperimentSubject.find().fetch()[0] && ExperimentSubject.find().fetch()[0].matchPartnerFbId){
            return 'https://graph.facebook.com/' + ExperimentSubject.find().fetch()[0].matchPartnerFbId + '/picture?type=normal';
        } else{
            return null;
        }   
    },
    'i18nStartTutorial' : function() {
        return 'Start';
    }
});

Template.task2Instructions.events({
    'click #startTutorial': function(event) {
        Meteor.call('updateTask2State', 2, function(error){
            if(error){
                window.alert(error);
            }
            Router.go('task2SurveyOne');
        });
    }
});
