var recommendState = 0;
var recommendDep = new Tracker.Dependency();
var searchListLength = 1000;
var recommendationsLength = 1000;
var searchString = '(?=.*)';
var searchDep = new Tracker.Dependency();
var helperLengthDep = new Tracker.Dependency();
var logger = [];

Template.task2RecommendModules.rendered = function() {
    recommendDep.changed();
    if(ExperimentSubject.find().fetch().length == 0 || ExperimentSubject.findOne().matchPartnerId == undefined || ExperimentSubject.findOne().completedRegistration == 0){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().Task2Stage > 5){
        Router.go('task2Complete');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 4){
        Router.go('task2Survey');
    }
    Meteor.call('task2RecommendStart', Meteor.userId());
    recommendState = 0;
    logger = [];
    
    var humRecs = document.getElementById('humRecTable');
    sortableRecommendations = new Sortable.create(humRecs, {
        group: {
            name: 'rec',
            pull: true,
            put: true
        },
        draggable: '.draggable-item',
        sort: true,
        animation: 100,
        handle: '.drag-handle',
        ghostClass: 'ghost',
        onAdd: function(evt){

            var updateModule = evt.item.getAttribute('data-id');
            var original = document.getElementById('humRecTable');
            var removeIndex;

            //Find module if already in recommended list
            for(var i = 0; i < original.rows.length; i++){
                if(updateModule == original.rows[i].getAttribute('data-id') && i != evt.newIndex){
                    removeIndex = i;
                }
            }

            //var clone = original.rows[removeIndex].cloneNode(true);
            var clone = original.rows[evt.newIndex].cloneNode(true);

            //Remove duplicate and replace with clone due to bug in rubaxa sortable library
            if(removeIndex != undefined){
                    original.deleteRow(removeIndex);
                if(removeIndex < evt.newIndex){
                    original.insertBefore(clone, original.childNodes[evt.newIndex -1]);
                    original.deleteRow(evt.newIndex);
                    var date = new Date();
                    logger.push(date.toString() + '\tAdd\t'+ evt.item.getAttribute('data-id')+'\tat\t'+ (evt.newIndex -1)+'\tprevOccurence\t'+ '-1');
                } else{
                    original.insertBefore(clone, original.childNodes[evt.newIndex]);
                    original.deleteRow(evt.newIndex+1); 
                    var date = new Date();
                    logger.push(date.toString() + '\tAdd\t'+ evt.item.getAttribute('data-id')+'\tat\t'+ (evt.newIndex)+'\tprevOccurence\t'+ '+1');     
                }
            } else{
                original.insertBefore(clone, original.childNodes[evt.newIndex]);
                original.deleteRow(evt.newIndex+1);
                var date = new Date();
                logger.push(date.toString() + '\tAdd\t'+ evt.item.getAttribute('data-id')+'\tat\t'+ (evt.newIndex)+'\tprevOccurence\t'+ '0');   
            }
            helperLengthDep.changed(); 
        },
        onUpdate: function(evt){
            var date = new Date();
            logger.push(date.toString() + '\tRearrange\t'+ evt.item.getAttribute('data-id')+'\tfrom\t'+evt.oldIndex+'\tto\t'+evt.newIndex);
        },
        onSort: function(evt){
            if(recommendState != 0){
                recommendState = 0;
                recommendDep.changed();
            }     
        }
    });

    var bin = document.getElementById('binTable');
    sortableBin = new Sortable.create(bin, {
        group: {
            name: 'rec',
            pull: true,
            put: false
        },
        draggable: '.draggable-item',
        sort: true,
        animation: 0,
        handle: '.drag-handle',
        ghostClass: 'ghost',
        onAdd: function(evt){
            //Remove original Entry if in list such that there are no duplicates
            var updateModule = evt.item.getAttribute('data-id');
            var original = document.getElementById('humRecTable');
            var removeIndex;
            for(var i = 0; i < original.rows.length; i++){
                if(updateModule == original.rows[i].getAttribute('data-id') && i != evt.newIndex){
                    removeIndex = i;
                }          
            }
            if(removeIndex != undefined){
                original.deleteRow(removeIndex);
            }
        },
        onUpdate: function(evt){

        },
        onRemove: function(evt){
        },
        onMove: function (evt) {
        },
        onStart: function (evt) {
        }
    });

}

Template.task2RecommendModules.helpers({
    'recommendInformation' : function() {
        recommendDep.depend();
        if(recommendState == 0){
            return i18n('task2HumanRecommenderInformationOne') + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0] + i18n('task2HumanRecommenderInformationTwo');
        }
        if(recommendState == 1){
            return i18n('task2HumanRecommenderInformationThree');
        }
        if(recommendState == 2){
            return i18n('task2HumanRecommenderInformationFour') + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0] + '?';
        }
    },
    'goNowhere' : function(){
        recommendDep.depend();
        return (recommendState == 0);
    },
    'goTutorial' : function(){
        recommendDep.depend();
        return (recommendState == 1);
    },
    'goSubmit' : function(){
        recommendDep.depend();
        return (recommendState == 2);
    },
    'matchFirstName' : function() {
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
    },
    'matchFullName' : function() {
        return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
    },
    'i18nNextButton' : function(){
        tutorialStateDep.depend();
        if(tutorialState == 8){
            return 'Recommend modules to ' + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
        } else{
            return 'next';
        }
        
    },
    'ranks' : function() {
        helperLengthDep.depend();
        var length;
        if(document.getElementById('humRecTable')){
            length = document.getElementById('humRecTable').rows.length;
        } else{
            length = AlgRecommendations.find().count();
        }
        var ranks = [];
        for (var i = 1; i <= length; i++) { 
            ranks.push({number : i});
        }
        return ranks;
     },
     'recommendations' : function() {
        return AlgRecommendations.find({}, {sort: {order: 1}, limit: recommendationsLength});    
     },
     'algRecommendations' : function(){
        searchDep.depend();
        if(searchString == '(?=.*)'){
            if(document.getElementById('algRecTable')){
                var original = document.getElementById('algRecTable');
                for(var i = 0; i < original.rows.length; i++){                
                    original.deleteRow(i);
                }
            }
            return [];
        }
        return AlgRecommendations.find({title: {$regex: searchString, $options: 'i'}},{sort : {order: 1}, limit: searchListLength});
     },
     'algRecommendationsOptions' : function(){
        return {
            group: {
                name: 'rec',
                pull: 'clone',
                put: false
            },
            sort: false,
            ghostClass: 'ghost',
            onMove: function (evt) {
            },
            onStart: function (evt) {
            }
        }
     },
     'bins' : function(){
        return [];
     },
    'profilePictureLink' : function(){
        var fbId;
        if(ExperimentSubject.find().fetch()[0] && ExperimentSubject.find().fetch()[0].matchPartnerFbId){
            return 'https://graph.facebook.com/' + ExperimentSubject.find().fetch()[0].matchPartnerFbId + '/picture?type=normal';
        } else{
            return null;
        }   
    }
});

Template.task2RecommendModules.events({
    'click .getInfo': function(event) {
        var date = new Date();
        logger.push(date.toString() + '\tcall\t'+ event.currentTarget.getAttribute('data-url'));
        var win = window.open(event.currentTarget.getAttribute('data-url'), '_blank');
        win.focus();
    },
    'keyup #searchBox' : function(event) {
            if(recommendState != 0){
                recommendState = 0;
                recommendDep.changed();
            }   
            var duplicateIndex = [];
            var original = document.getElementById('algRecTable');
            var re = new RegExp(searchString, 'i');
            for(var i = 0; i < original.rows.length; i++){                
                if(!re.test(original.rows[i].getAttribute('data-title'))){
                    duplicateIndex.push(i);
                } else{
                    for (var j = i+1; j < original.rows.length; j++){
                        if(original.rows[i].getAttribute('data-id') == original.rows[j].getAttribute('data-id')){
                            duplicateIndex.push(i);
                        }
                    }
                }
            }
            for(var k = duplicateIndex.length -1; k >= 0; k--){
                original.deleteRow(duplicateIndex[k]);
            }
            searchString = '';
            var searchTerms = document.getElementById('searchBox').value.split(' ');
            for(var i = 0; i < searchTerms.length; i++){
                searchString += '(?=.*' + searchTerms[i] + ')';
            }
            if(event.keyCode == 27){
                document.getElementById('searchBox').value = '';
                searchString = '(?=.*)';
            }
            searchDep.changed();                
    },
    'click #searchButton': function(event) {
            var duplicateIndex = [];
            var original = document.getElementById('algRecTable');
            var re = new RegExp(searchString, 'i');
            for(var i = 0; i < original.rows.length; i++){                
                if(!re.test(original.rows[i].getAttribute('data-title'))){
                    duplicateIndex.push(i);
                } else{
                    for (var j = i+1; j < original.rows.length; j++){
                        if(original.rows[i].getAttribute('data-id') == original.rows[j].getAttribute('data-id')){
                            duplicateIndex.push(i);
                        }
                    }
                }
            }
            for(var k = duplicateIndex.length -1; k >= 0; k--){
                original.deleteRow(duplicateIndex[k]);
            }
            searchString = '';
            var searchTerms = document.getElementById('searchBox').value.split(' ');
            for(var i = 0; i < searchTerms.length; i++){
                searchString += '(?=.*' + searchTerms[i] + ')';
            }
            searchDep.changed();        
    },
    'click .removeItem': function(event) {
        if(recommendState != 0){
            recommendState = 0;
            recommendDep.changed();
        }   
        var index = event.currentTarget.getAttribute('data-rank') - 1;
        var original = document.getElementById('humRecTable');
        var binTable = document.getElementById('binTable');
        if(original.rows.length <= 10){
            Toast.warning('List cannot contain less than 10 recommendations');
            return
        }
        if(index < original.rows.length){
            var date = new Date();
            var removedId = original.rows[index].getAttribute('data-id');
            logger.push(date.toString() + '\tRemove\t'+ removedId +'\tfrom\t'+ index);
            var clone = original.rows[index].cloneNode(true);
            binTable.insertBefore(clone, binTable.childNodes[0]);
            original.deleteRow(index);
            helperLengthDep.changed();
            //Remove duplicates in bin
            var binTable = document.getElementById('binTable');
            for(var i = 1; i < binTable.rows.length; i++){
                if(binTable.rows[i].getAttribute('data-id') == removedId){
                    binTable.deleteRow(i);
                }
            }

        } else{
            Toast.warning('No module at this position');           
        }
    },
    'click #submit': function(){
        recommendState = 2;
        recommendDep.changed();
    },
    'click #backToTutorial': function(){
        recommendState = 1;
        recommendDep.changed();
    },
    'click #goNowhereButton': function(){
        recommendState = 0;
        recommendDep.changed();
    },
    'click #goTutorialButton': function(){
        Router.go('task2Tutorial');
    },
    'click #goSubmitButton': function(){
        document.getElementById('goSubmitButton').value = 'sending...';
        document.getElementById('goSubmitButton').disabled = true;
        
        var recList = [];
        var original = document.getElementById('humRecTable');
        for(var i = 0; i < original.rows.length; i++){
            recList.push(original.rows[i].getAttribute('data-id'));
        }

        var binList = [];
        var bin = document.getElementById('binTable');
         for(var i = 0; i < bin.rows.length; i++){
            binList.push(bin.rows[i].getAttribute('data-id'));
        }       

        Meteor.call('storeHumanRecommenderPredictionsAndLog', Meteor.userId(), ExperimentSubject.findOne().matchPartnerId, recList, binList, logger, function(error, result){
            if(error){
                window.alert(error);
                document.getElementById('goSubmitButton').disabled = false;
            } else{
                Meteor.call('updateTask2State', 5, function(error){
                    if(error){
                        window.alert(error);
                        document.getElementById('goSubmitButton').disabled = false;
                    }
                        Router.go('task2Survey');
                });
            }
        });
    }
});
