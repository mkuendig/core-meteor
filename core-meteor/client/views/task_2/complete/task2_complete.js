Template.task2Complete.rendered = function() {
    Meteor.call('task2CompleteStart', Meteor.userId());
    Meteor.call('updateTask2State', 7);
}

Template.task2Complete.helpers({
    'matchFirstName': function(){
        if(ExperimentSubject.find().fetch()[0] && ExperimentSubject.find().fetch()[0].matchPartnerCallName){
            if(ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0]){
                return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
            } else{
                return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
            }
        } else{
            return 'none';
        }   
    }
});
Template.task2Complete.events({
    'click #logout': function() {
        Meteor.call('task2Logout', Meteor.userId());
        Router.go('home');
        Meteor.logout(function() {
            //Navigat to Shibboleth Logout URL on success
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.logout(function() {
                        window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                    });
                } else {
                    window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                }
            });
        });
    },
    'click #debugReset': function() {
        Meteor.call('updateTask2State', 0);
    }
});
