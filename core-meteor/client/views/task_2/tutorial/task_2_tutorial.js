var tutorialState = -1;
var maxTutorialState = -1;
var searchListLength = 1000;
var recommendationsLength = 1000;
var searchString = '(?=.*)';
var searchDep = new Tracker.Dependency();
var helperLengthDep = new Tracker.Dependency();
var tutorialStateDep = new Tracker.Dependency();
var tutorialActionDep = new Tracker.Dependency();
var nextDisabled = false;

Template.task2Tutorial.rendered = function() {
    if(ExperimentSubject.find().fetch().length == 0 || ExperimentSubject.findOne().matchPartnerId == undefined || ExperimentSubject.findOne().completedRegistration == 0){
        Router.go('notAllowed');
    }
    if(ExperimentSubject.findOne().Task2Stage > 5){
        Router.go('task2Complete');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 4){
        Router.go('task2Survey');
    }
    else if(ExperimentSubject.findOne().Task2Stage > 3){
        Router.go('task2RecommendModules');
    }
    Meteor.call('task2TutorialStart', Meteor.userId());
    tutorialState = -1;
    tutorialStateDep.changed();
    
    var humRecs = document.getElementById('humRecTable');
    sortableRecommendations = new Sortable.create(humRecs, {
        group: {
            name: 'tutorialRec',
            pull: true,
            put: true
        },
        draggable: '.draggable-item',
        sort: true,
        animation: 100,
        handle: '.drag-handle',
        ghostClass: "ghost",
        onAdd: function(evt){
            if(tutorialState == 4 && evt.item.getAttribute('data-title') == 'Module Title TEST'){
                nextDisabled = false;
                if(maxTutorialState <= tutorialState){
                    maxTutorialState = tutorialState + 1;
                }
                tutorialActionDep.changed();
            }
            var updateModule = evt.item.getAttribute("data-id");
            var original = document.getElementById('humRecTable');
            var removeIndex;

            //Find module if already in recommended list
            for(var i = 0; i < original.rows.length; i++){
                if(updateModule == original.rows[i].getAttribute('data-id') && i != evt.newIndex){
                    removeIndex = i;
                }
            }

            //var clone = original.rows[removeIndex].cloneNode(true);
            var clone = original.rows[evt.newIndex].cloneNode(true);

            //Remove duplicate (if present)
            if(removeIndex != undefined){
                    original.deleteRow(removeIndex);
                if(removeIndex < evt.newIndex){
                    original.insertBefore(clone, original.childNodes[evt.newIndex -1]);
                    original.deleteRow(evt.newIndex);
                } else{
                    original.insertBefore(clone, original.childNodes[evt.newIndex]);
                    original.deleteRow(evt.newIndex+1);      
                }
            } else{
                original.insertBefore(clone, original.childNodes[evt.newIndex]);
                original.deleteRow(evt.newIndex+1);
            }
            if(tutorialState == 7){
                var elements = document.getElementById('humRecTable').getElementsByClassName('question-handle');
                for(var i = 0; i < elements.length; i++){
                    elements[i].style.borderRight = 'thick solid #FF0000';
                    elements[i].style.borderLeft = 'thick solid #FF0000';
                    elements[i].style.borderTop = 'none';
                    elements[i].style.borderBottom = 'none';                    
                }
                elements[0].style.borderTop = 'thick solid #FF0000';
                elements[elements.length-1].style.borderBottom = 'thick solid #FF0000';
            }
            helperLengthDep.changed(); 
        },
        onUpdate: function(evt){

        },
        onRemove: function(evt){
        },
        onMove: function (evt) {
            if(tutorialState < 3){
                return false;
            }
        },
        onStart: function (evt) {
            if(tutorialState < 3){
                Toast.warning('Not possible at this stage of the tutorial');
            }
        },
        onSort: function(evt){
            if(tutorialState == 3){
                nextDisabled = false;
                if(maxTutorialState <= tutorialState){
                    maxTutorialState = tutorialState + 1;
                }
                tutorialActionDep.changed();
                var elements = document.getElementById('humRecTable').getElementsByClassName('drag-handle');
                for(var i = 0; i < elements.length; i++){
                    elements[i].style.borderRight = 'thick solid #FF0000';
                    elements[i].style.borderLeft = 'thick solid #FF0000';
                    elements[i].style.borderTop = 'none';
                    elements[i].style.borderBottom = 'none';
                }
                elements[0].style.borderTop = 'thick solid #FF0000';
                elements[elements.length-1].style.borderBottom = 'thick solid #FF0000';
            }
            if(tutorialState == 7){
                var elements = document.getElementById('humRecTable').getElementsByClassName('question-handle');
                for(var i = 0; i < elements.length; i++){
                    elements[i].style.borderRight = 'thick solid #FF0000';
                    elements[i].style.borderLeft = 'thick solid #FF0000';
                    elements[i].style.borderTop = 'none';
                    elements[i].style.borderBottom = 'none';                    
                }
                elements[0].style.borderTop = 'thick solid #FF0000';
                elements[elements.length-1].style.borderBottom = 'thick solid #FF0000';
            }
        }
    });

    var bin = document.getElementById('binTable');
    sortableBin = new Sortable.create(bin, {
        group: {
            name: 'tutorialRec',
            pull: true,
            put: false
        },
        draggable: '.draggable-item',
        sort: true,
        animation: 0,
        handle: '.drag-handle',
        ghostClass: 'ghost',
        onAdd: function(evt){
            //Remove original Entry if in list such that there are no duplicates
            var updateModule = evt.item.getAttribute("data-id");
            var original = document.getElementById('humRecTable');
            var removeIndex;
            for(var i = 0; i < original.rows.length; i++){
                if(updateModule == original.rows[i].getAttribute("data-id") && i != evt.newIndex){
                    removeIndex = i;
                }          
            }
            if(removeIndex != undefined){
                original.deleteRow(removeIndex);
            }
        },
        onUpdate: function(evt){

        },
        onRemove: function(evt){
            if(tutorialState ==6){
                nextDisabled = false;
                if(maxTutorialState <= tutorialState){
                    maxTutorialState = tutorialState + 1;
                }
                tutorialActionDep.changed();
            }
        },
        onMove: function (evt) {
            if(tutorialState < 6){
                return false;
            }
        },
        onStart: function (evt) {
            if(tutorialState < 6){
                Toast.warning('Not possible at this stage of the tutorial');
            }
        },
        onSort: function(evt) {
        }
    });

}

Template.task2Tutorial.helpers({
    'isFirst' : function() {
        tutorialStateDep.depend();
        return tutorialState == -1;
    },
    'tutorialInformation' : function() {
        tutorialStateDep.depend();
        updateUi();
        if(tutorialState == -1){
            return i18n('task2TutorialState0InformationZero') + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0] + i18n('task2TutorialState0InformationZeroTwo');
        } else if(tutorialState == 0){
            return i18n('task2TutorialState0InformationOne');// + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0] + i18n('task2TutorialState0InformationTwo');
        } else if(tutorialState == 1){
            return i18n('task2TutorialState1InformationOne') + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0] + i18n('task2TutorialState1InformationTwo');
        } else if(tutorialState == 2){
            return i18n('task2TutorialState2InformationOne') + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0] + i18n('task2TutorialState2InformationTwo');
        } else if(tutorialState == 3){
            return i18n('task2TutorialState3Information');
        } else if(tutorialState == 4){
            return i18n('task2TutorialState4Information');
        } else if(tutorialState == 5){
            return i18n('task2TutorialState5Information');
        } else if(tutorialState == 6){
            return i18n('task2TutorialState6Information');
        } else if(tutorialState == 7){
            return i18n('task2TutorialState7Information');
        } else if(tutorialState == 8){
            return i18n('task2TutorialState8InformationOne') + ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0].bold() + '.';
        }
    },
    'i18nNextButton' : function(){
        tutorialStateDep.depend();
        if(tutorialState == 8){
            return 'start'
        } else{
            return 'next';
        }
        
    },
    'nextButtonDisable' : function(){
        tutorialActionDep.depend();
        tutorialStateDep.depend();
        if(maxTutorialState > tutorialState || Session.get('tutorialFinished')){
            return false;
        } else{
            return nextDisabled;
        }
        
    },
    'nextButtonVisibility' : function(){
        tutorialStateDep.depend();
        return 'visible';
    },
    'i18nBackButton' : function(){
        tutorialStateDep.depend();
        return 'back';
    },
    'backButtonDisable' : function(){
        tutorialStateDep.depend();
        return false;
    },
    'backButtonVisibility' : function(){
        tutorialStateDep.depend();
        if(tutorialState == -1){
            return 'hidden';
        } else{
            return 'visible';
        }
    },
    'ranks' : function() {
        helperLengthDep.depend();
        var length;
        if(document.getElementById('humRecTable')){
            length = document.getElementById('humRecTable').rows.length;
        } else{
            length = TutorialRecommendations.find().count();
        }
        var ranks = [];
        for (var i = 1; i <= length; i++) { 
            ranks.push({number : i});
        }
        return ranks;
     },
     'recommendations' : function() {
        return TutorialRecommendations.find({}, {sort: {order: 1}, limit: recommendationsLength});    
     },
     'algRecommendations' : function(){
        searchDep.depend();
        if(searchString == '(?=.*)'){
            if(document.getElementById('algRecTable')){
                var original = document.getElementById('algRecTable');
                for(var i = 0; i < original.rows.length; i++){                
                    original.deleteRow(i);
                }
            }
            return [];
        }
        return TutorialRecommendations.find({title: {$regex: searchString, $options: 'i'}},{sort : {order: 1}, limit: searchListLength});
     },
     'algRecommendationsOptions' : function(){
        return {
            group: {
                name: 'tutorialRec',
                pull: 'clone',
                put: false
            },
            sort: false,
            ghostClass: 'ghost',
            onMove: function (evt) {
                if(tutorialState < 4){
                    return false;
                }
            },
            onStart: function (evt) {
                if(tutorialState < 4){
                    Toast.warning('Not possible at this stage of the tutorial');
                }
            }
        }
     },
     'bins' : function(){
        return [];
     }
});

Template.task2Tutorial.events({
    'click .getInfo': function(event) {
        window.alert("Once the tutorial is over, clicking on this icon will open the course catalogue entry for this course in a seperate tab.");
    },
    'keyup #searchBox' : function(event) {
            var duplicateIndex = [];
            var original = document.getElementById('algRecTable');
            var re = new RegExp(searchString, 'i');
            for(var i = 0; i < original.rows.length; i++){                
                if(!re.test(original.rows[i].getAttribute('data-title'))){
                    duplicateIndex.push(i);
                } else{
                    for (var j = i+1; j < original.rows.length; j++){
                        if(original.rows[i].getAttribute('data-id') == original.rows[j].getAttribute('data-id')){
                            duplicateIndex.push(i);
                        }
                    }
                }
            }
            for(var k = duplicateIndex.length -1; k >= 0; k--){
                original.deleteRow(duplicateIndex[k]);
            }
            searchString = '';
            var searchTerms = document.getElementById('searchBox').value.split(' ');
            for(var i = 0; i < searchTerms.length; i++){
                searchString += '(?=.*' + searchTerms[i] + ')';
            }
            if(event.keyCode == 27){
                document.getElementById('searchBox').value = '';
                searchString = '(?=.*)';
            }
            searchDep.changed();                
    },
    'click #searchButton': function(event) {
            searchString = '';
            var searchTerms = document.getElementById('searchBox').value.split(" ");
            for(var i = 0; i < searchTerms.length; i++){
                searchString += '(?=.*' + searchTerms[i] + ')';
            }
            searchDep.changed();    
    },
    'click .removeItem': function(event) {
        if(tutorialState < 5){
            Toast.warning('Not possible at this stage of the tutorial');
            return
        }
        var index = event.currentTarget.getAttribute('data-rank') - 1;
        var original = document.getElementById('humRecTable');
        var binTable = document.getElementById('binTable');
        if(original.rows.length <= 10){
            Toast.warning('List cannot contain less than 10 recommendations');
            return
        }
        if(index < original.rows.length){
            var clone = original.rows[index].cloneNode(true);
            var removedId = original.rows[index].getAttribute('data-id');
            binTable.insertBefore(clone, binTable.childNodes[0]);
            original.deleteRow(index);
            helperLengthDep.changed(); 
            //Remove duplicates in bin
            var binTable = document.getElementById('binTable');
            for(var i = 1; i < binTable.rows.length; i++){
                if(binTable.rows[i].getAttribute('data-id') == removedId){
                    binTable.deleteRow(i);
                }
            }
        } else{
            Toast.warning('No module at this position');           
        }
        if(tutorialState == 7){
            var elements = document.getElementById('binTable').getElementsByClassName('question-handle');
            for(var i = 0; i < elements.length; i++){
                elements[i].style.border = 'none';                 
            }            
        }
        if(tutorialState == 5){
            var bin = document.getElementById('binTable');
            if(bin.rows.length > 4){
                nextDisabled = false;
                if(maxTutorialState <= tutorialState){
                    maxTutorialState = tutorialState + 1;
                }
                tutorialActionDep.changed();
            }           
        }
    },
    'click #tutorialNext': function(){
        if(tutorialState == 8){
            Session.setPersistent('tutorialFinished', true)
            Meteor.call('updateTask2State', 4, function(error){
                if(error){
                    window.alert(error);
                }
                    Router.go('task2RecommendModules');
            });
        }
        tutorialState++;
        if(maxTutorialState < tutorialState){
            maxTutorialState = tutorialState;
        }
        tutorialStateDep.changed();
    },
    'click #tutorialBack': function(){
        tutorialState--;
        tutorialStateDep.changed();
    }
});

function updateUi(){
    if(tutorialState == 0){
        //cleanup
        nextDisabled = false;
        if(document.getElementById('humRecTable')){
            document.getElementById('humRecTable').style.border = 'none';
        }
    }
    if(tutorialState == 1){
        //cleanup
        nextDisabled = false;
        //cleanup stage 2
        var numbersTable = document.getElementById('numTable');
        var recsTable = document.getElementById('humRecTable');
        for(var i = 0; i < 10; i++){
            numbersTable.rows[i].style.borderTop = 'none';
            numbersTable.rows[i].style.borderLeft = 'none';
            numbersTable.rows[i].style.borderRight = 'none';
            numbersTable.rows[i].style.borderBottom = '1px solid #FFFFFF';
        }

        //adapt design
        document.getElementById('humRecTable').style.border = 'thick solid #FF0000';
        tutorialActionDep.changed();

    }
    if(tutorialState == 2){
        //cleanup
        nextDisabled = false;
        //cleanup stage 1
        document.getElementById('humRecTable').style.border = 'none';
        //cleanup stage 3
        var elements = document.getElementById('humRecTable').getElementsByClassName('drag-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.border = 'none';
        }
     
        //adapt design
        var numbersTable = document.getElementById('numTable');
        var recsTable = document.getElementById('humRecTable');
        for(var i = 0; i < 10; i++){
            numbersTable.rows[i].style.borderLeft = 'thick solid #FF0000';
            numbersTable.rows[i].style.borderRight = 'thick solid #FF0000';
        }
        numbersTable.rows[0].style.borderTop = 'thick solid #FF0000';
        numbersTable.rows[9].style.borderBottom = 'thick solid #FF0000';
        
        tutorialActionDep.changed();
    }
    if(tutorialState == 3){
        //cleanup
        nextDisabled = false;
        //cleanup stage 2
        var numbersTable = document.getElementById('numTable');
        var recsTable = document.getElementById('humRecTable');
        for(var i = 0; i < 10; i++){
            numbersTable.rows[i].style.borderTop = 'none';
            numbersTable.rows[i].style.borderLeft = 'none';
            numbersTable.rows[i].style.borderRight = 'none';
            numbersTable.rows[i].style.borderBottom = '1px solid #FFFFFF';
        }
        //cleanup stage 4
        document.getElementById('searchBox').style.border = 'none';

        //adapt design
        var elements = document.getElementById('humRecTable').getElementsByClassName('drag-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.borderRight = 'thick solid #FF0000';
            elements[i].style.borderLeft = 'thick solid #FF0000';
        }
        elements[0].style.borderTop = 'thick solid #FF0000';
        elements[elements.length-1].style.borderBottom = 'thick solid #FF0000';

        nextDisabled = true;
        tutorialActionDep.changed();
    }
    if(tutorialState == 4){
        //cleanup
        nextDisabled = false;
        //cleanup stage 3
        var elements = document.getElementById('humRecTable').getElementsByClassName('drag-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.border = 'none';
        }
        //cleanup stage 5
        document.getElementById('removeTable').style.border = 'none';

        /*
        var table = document.getElementById('humRecTable');
        var elements = table.getElementsByClassName('drag-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.backgroundColor = 'transparent';
        }

        var removeTable = document.getElementById('removeTable');
        var removeElements = removeTable.getElementsByClassName('human-table-box');
        for(var i = 0; i < removeElements.length; i++){
            removeElements[i].style.backgroundColor = 'transparent';
        }
        */
        //adapt design
        document.getElementById('searchBox').style.border = 'thick solid #FF0000';
        /*
        var algTable = document.getElementById('algRecTable');
        var algElements = algTable.getElementsByClassName('drag-handle');
        for(var i = 0; i < algElements.length; i++){
            algElements[i].style.backgroundColor = '#FFDE46';
        }
        */
        
        nextDisabled = true;
        tutorialActionDep.changed();
    }
    if(tutorialState == 5){
        //cleanup
        nextDisabled = false;
        //cleanup stage 4
        document.getElementById('searchBox').style.border = 'none';
        //cleanup stage 6
        document.getElementById('binRecPanel').style.border = 'none';
        //adapt design
        document.getElementById('removeTable').style.border = 'thick solid #FF0000';

        /*
        document.getElementById('searchBox').style.backgroundColor = '#FFFFFF'; 
        document.getElementById('binRecPanel').style.backgroundColor = '#FFFFFF';
        //adapt design
        var removeTable = document.getElementById('removeTable');
        var removeElements = removeTable.getElementsByClassName('human-table-box');
        for(var i = 0; i < removeElements.length; i++){
            removeElements[i].style.backgroundColor = '#FFDE46';
        }
        */
        nextDisabled = true;
        tutorialActionDep.changed();      
    }
    if(tutorialState == 6){
        //cleanup
        nextDisabled = false;
        //cleanup stage 5
        document.getElementById('removeTable').style.border = 'none';
        //cleanup stage 7
        var elements = document.getElementById('humRecTable').getElementsByClassName('question-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.border = 'none';
        }
        /*
        var removeTable = document.getElementById('removeTable');
        var removeElements = removeTable.getElementsByClassName('human-table-box');
        for(var i = 0; i < removeElements.length; i++){
            removeElements[i].style.backgroundColor = 'transparent';
        }
        var table = document.getElementById('humRecTable');
        var questionHandles = table.getElementsByClassName('question-handle');
        for(var i = 0; i < questionHandles.length; i++){
            questionHandles[i].style.backgroundColor = 'transparent';
        }  
        */      
        //adapt design
        document.getElementById('binRecPanel').style.border = 'thick solid #FF0000';
        nextDisabled = true;
        tutorialActionDep.changed();
    }
    if(tutorialState == 7){
        //cleanup
        nextDisabled = false;
        //cleanup stage 6
        document.getElementById('binRecPanel').style.border = 'none';

        //adapt design
        var elements = document.getElementById('humRecTable').getElementsByClassName('question-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.borderRight = 'thick solid #FF0000';
            elements[i].style.borderLeft = 'thick solid #FF0000';
        }
        elements[0].style.borderTop = 'thick solid #FF0000';
        elements[elements.length-1].style.borderBottom = 'thick solid #FF0000';
 
    }
    if(tutorialState == 8){
        //cleanup
        nextDisabled = false;
        //cleanup stage 7
        var elements = document.getElementById('humRecTable').getElementsByClassName('question-handle');
        for(var i = 0; i < elements.length; i++){
            elements[i].style.border = 'none';
        }
    }

}