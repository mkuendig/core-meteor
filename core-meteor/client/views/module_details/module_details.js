var sortDep = new Tracker.Dependency();
var fullDescDep = new Tracker.Dependency();
var sortOrder = 'rating';
var description;
var fullDescriptionClicked = false;
var semesterPlanId;

Template.moduleDetail.created = function() {
    if (this.data.module.description) {
        if (this.data.module.description.length > 150) {
            var shortDesc = this.data.module.description.substring(0, 150);
            var finalSpaceChar = shortDesc.lastIndexOf(' ');
            description = shortDesc.slice(0, finalSpaceChar) + '... ' + '<a id="show-full-description" href="#">' + i18n('showAll') + '</a>';
        } else {
            description = this.data.module.description;
        }
    }
    var semesterId = this.data.module.semester.title.toLowerCase();
    semesterPlanId = SemesterPlans.findOrCreateSemesterPlan(semesterId, Meteor.userId());
};

Template.moduleDetail.rendered = function() {
    UIHelper.refreshRateit();
    $('.scrollable').animate({
        scrollTop: 0
    }, 500);
    $('[data-toggle="tooltip"]').tooltip();
};

Template.moduleDetail.helpers({

    description: function() {
        fullDescDep.depend();
        return description;
    },

    semester: function() {
        if (this.module) {
            return this.module.semester.title.toLowerCase();
        }
    },

    rating: function() {
        return RatingController.getRating(this.module);
    },

    avgRating: function() {
        return RatingController.getAvgRating(this.module);
    },

    prediction: function() {
        if (this.module) {
            var pred = PredictionController.getPrediction(this.module);
            if (pred > 0) {
                return pred;
            }
        }
        return i18n('noPrediction');
    },

    rooms: function() {
        var rooms = [];
        _.each(this.module.lectures, function(lecture) {
            _.each(lecture.scheduleItems, function(scheduleItem) {
                _.each(scheduleItem.rooms, function(room) {
                    if (rooms.indexOf(room.name) === -1) {
                        rooms.push(room.name);
                    }
                });
            });
        });
        return rooms.join(', ');
    },

    lectures: function() {
        var lectures = [];
        _.each(this.module.lectures, function(lecture) {
            if (lecture.scheduleItems.length > 0) {
                var startDatetime = moment(lecture.scheduleItems[0].startDatetime);
                var lectureDate = {
                    date: lecture.lectureCategory + ' - ' + moment(startDatetime).format('dd HH:mm')
                };
                if (!_.find(lectures, function(lecture) {
                        return lecture.date === lectureDate.date;
                    })) {
                    lectures.push(lectureDate);
                }
            }
        });
        return lectures;
    },

    lecturers: function() {
        return _.map(this.module.lecturers, function(lecturer) {
            return lecturer.firstname + ' ' + lecturer.lastname;
        }).join(', ');
    },

    comments: function() {
        sortDep.depend();
        var comments;
        var _moduleId = this.module._id;
        if (sortOrder === 'rating') {
            comments = _.sortBy(this.module.comments, function(comment) {
                return -(comment.upvotes.length - comment.downvotes.length);
            });
        } else if (sortOrder === 'mostRecent') {
            comments = _.sortBy(this.module.comments, function(comment) {
                //comment.createdAt is already a date
                return -(comment.createdAt.getTime());
            });
        }
        return _.map(comments, function(comment) {
            comment.creator = (comment.createdById === Meteor.userId());
            comment._moduleId = _moduleId;
            return comment;
        });
    },

    uncommented: function() {
        return !(_.find(this.module.comments, function(comment) {
            return comment.createdById === Meteor.userId();
        }));
    },

    unrated: function() {
        return !(_.find(this.module.ratings, function(rating) {
            return rating.userId === Meteor.userId();
        }));
    },

    studySelector: function() {
        if (this.module) {
            return {
                id: 'study-selector',
                placeholder: i18n('select'),
                options: {
                    allowClear: true,
                    moduleId: this.module.moduleId
                }
            };
        }
    },

    studySelectorOptions: function() {
        var currentSelection = StudyPlanController.getStudyOverview(this.module.moduleId);
        var options = '';
        _.each([{
            id: 'bsc-option',
            value: 'bsc',
            text: 'Bachelor'
        }, {
            id: 'bsc-option',
            value: 'msc',
            text: 'Master'
        }], function(option) {
            var selected = currentSelection === option.value ? 'selected' : '';
            options += '<option value="' + option.value + '" ' + selected + '>' + option.text + '</option>';
        });
        return options;
    },

    studySelected: function() {
        return StudyPlanController.getStudyOverview(this.module.moduleId) ? true : false;
    },

    regulationSelector: function() {
        var studyId = StudyPlanController.getStudyOverview(this.module.moduleId);
        return {
            id: 'regulation-selector',
            placeholder: i18n('regulation'),
            options: {
                allowClear: true,
                moduleId: this.module.moduleId,
                studyId: studyId
            }
        };
    },

    regulationSelectorOptions: function() {
        var studyId = StudyPlanController.getStudyOverview(this.module.moduleId);
        var moduleId = this.module.moduleId;
        var options = '';
        _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
            var selected = _.contains(regulation.modules, moduleId) ? 'selected' : '';
            options += '<option value="' + regulation.id + '" ' + selected + '>' + regulation.name + '</option>';
        });
        return options;
    },

    notInCurrentSemester: function() {
        var currentSemesters = _.pluck(SemesterPlans.getCurrentSemesterPlanNames(), 'name');
        return !_.contains(currentSemesters, this.module.semester.title.toLowerCase());
    },

    inSemesterPlan: function() {
        var moduleId = this.module.moduleId;
        var semesterPlan = SemesterPlans.findOne(semesterPlanId);
        if (!_.isUndefined(semesterPlan)) {
            return _.find(semesterPlan.modules, function(module) {
                return module.moduleId === moduleId;
            });
        } else {
            return false;
        }
    },

    getTitlePrediction: function() {
        return i18n('prediction') + UIHelper.userName();
    }
});

Template.moduleDetail.events({
    'click #add-comment-button': function() {
        var commentText = Template.wysiwygEditor.getContent();
        if (commentText !== undefined && commentText !== '') {
            Meteor.call('insertComment', this.module.moduleId, commentText);
        }
    },
    'click #openVVZ': function() {
        window.open(this.module.uzhUrl, '_blank');
    },

    'click #show-full-description': function() {
        description = this.module.description;
        fullDescriptionClicked = true;
        fullDescDep.changed();
    },

    'click #add-comment': function() {
        $('#writeCommentPanel').show();
        $('#add-comment').hide();
    },

    'click #back-button': function() {
        $('#writeCommentPanel').hide();
        $('#add-comment').show();
    },

    'click #sort-by-best-rated': function() {
        sortOrder = 'rating';
        sortDep.changed();
    },

    'click #sort-by-most-recent': function() {
        sortOrder = 'mostRecent';
        sortDep.changed();
    },

    'click #add-to-semester-plan': function() {
        Meteor.call('addModuleToSemesterPlan', semesterPlanId, this.module.moduleId);
    },

    'click #remove-from-semester-plan': function() {
        Meteor.call('removeModuleFromSemesterPlan', semesterPlanId, this.module.moduleId);
    },

    'click #add-to-study-plan': function() {
        Meteor.call('setRating', this.module.moduleId, null);
    },

    'click #remove-from-study-plan': function() {
        $('#remove-button');
        $('#remove-title').html(i18n('removeFromStudyPlan'));
        $('#remove-modal').modal();
    },

    'click #remove-button': function() {
        Meteor.call('removeRating', this.module.moduleId);
        Meteor.call('removeModuleFromStudyOverview', this.module.moduleId);
        $('#remove-modal').modal('hide');
    },

    'change #study-selector': function(event) {
        Meteor.call('removeModuleFromStudyOverview', this.options.moduleId);
        if (event.val === 'bsc') {
            Meteor.call('addModuleToStudyOverview', 'bsc', this.options.moduleId);
        } else if (event.val === 'msc') {
            Meteor.call('addModuleToStudyOverview', 'msc', this.options.moduleId);
        }
    },

    'change #regulation-selector': function(event) {
        Meteor.call('removeModuleFromRegulations', this.options.moduleId);
        if (event.val !== '') {
            Meteor.call('addModuleToRegulation', this.options.moduleId, this.options.studyId, event.val);
        }
    }
});