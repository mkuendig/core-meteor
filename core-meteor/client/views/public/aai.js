var nameDep = new Tracker.Dependency();
Template.aai.created = function() {
    if (Meteor.userId()) {
        Meteor.call('registrationStart', Meteor.userId(), 'existing account');
        
        Router.continueAfterLogin();
    }
    Session.set('aaiBlock', true);
    Session.set('aaiWithFB', false);
};

Template.aai.helpers({
});

Template.aai.events({
	'click #debugLogin': function(event) {
        Meteor.call('createDummyLogin', function(error, id) {
            if (error) {
                console.log('error - createDummby\n' + error);
            } else {
                Meteor.loginWithPassword(id, id, function(error) {
                    if (typeof error === 'undefined') {
                        Session.set('debug', true);
                        Router.go('task4Instructions');
                        }
                    else {
                        window.alert("Some error in debug mode");
                    }
                });
            }
        });
    }
});
