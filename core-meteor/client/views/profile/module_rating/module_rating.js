Template.moduleRatingProfile.rendered = function() {
    UIHelper.refreshRateit();
    $('[data-toggle="tooltip"]').tooltip();
    UIHelper.initiateDrag(true);
    $('.easypiechart').easyPieChart();

    this.autorun(function() {
        var percentage = RatingController.getRatingPercentage();
        $('.easypiechart').data('easyPieChart').update(percentage);
    });
};

Template.moduleRatingProfile.destroyed = function() {
    PredictionController.startPredictionUpdateProcess();
};

Template.moduleRatingProfile.helpers({
    getRatingPercent: function() {
        return RatingController.getRatingPercentage();
    },

    modules: function() {
        return Modules.getModulesFromCurrentUser();
    },

    rating: function() {
        return RatingController.getRating(this);
    },

    regulations: function(studyId) {
        var regulations = Meteor.user().profile[studyId].regulations;
        _.each(regulations, function(regulation) {
            regulation.studyId = studyId;
        });
        return regulations;
    },

    getArea: function(moduleId) {
        var regulationBSC = Meteor.user().profile.bsc.regulations;
        var regulationMSC = Meteor.user().profile.msc.regulations;
        var regulations;
        if (regulationBSC) {
            if (regulationMSC) {
                regulations = regulationBSC.concat(regulationMSC);
            } else {
                regulations = regulationBSC;
            }
        } else if (regulationMSC) {
            regulations = regulationMSC;
        }
        return UIHelper.getArea(moduleId, regulations);
    },

    hasRegulations: function() {
        var regulationBSC = Meteor.user().profile.bsc.regulations;
        if (regulationBSC && regulationBSC.length > 0) {
            return true;
        }
        var regulationMSC = Meteor.user().profile.msc.regulations;
        if (regulationMSC && regulationMSC.length > 0) {
            return true;
        }
        return false;
    },

    i18ntootltipStars: function() {
        return i18n('tootltipStars');
    }
});

Template.moduleRatingProfile.events({
    'click .remove-module': function() {
        var removeButton = $('#remove-button');
        removeButton.attr('data-module-id', this.moduleId);
        removeButton.attr('data-event', 'remove-self-module');
        $('#remove-title').html(i18n('removeModule'));
        $('#remove-modal').modal();
    },

    'click #remove-button': function(event) {
        var moduleId = $(event.target).attr('data-module-id');
        Meteor.call('removeRating', moduleId);
        Meteor.call('removeModuleFromRegulations', moduleId);
        Toast.success(i18n('user_module_relations_selection_deleted'));
        $('#remove-modal').modal('hide');
    },

    'click #finish': function() {
        Toast.success(i18n('saved'));
        Router.go('home');
    }
});