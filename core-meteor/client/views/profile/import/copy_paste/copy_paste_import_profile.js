Template.copyPasteImportProfile.helpers({
    i18nAddModules: function() {
        return i18n('addModules');
    }
});

Template.copyPasteImportProfile.events({
    'click #save': function() {
        Template.copyPasteImportTemplate.save();
        Router.go('moduleRatingProfile');
    }
});