// The modules variable is set according to the modules that should be displayed
// The datagrid reads the modules from this variable when it is rendered

var allModules, columns, filters;

Template.moduleSelectionProfile.events({
    'click #finish': function() {
        Router.go('moduleRatingProfile');
    }
});

Template.moduleSelectionProfile.created = function() {
    allModules = Modules.find().fetch();
    columns = ['moduleSelectorWizard', 'moduleId', 'title', 'ects'];
    filters = ['semesterFilter', 'ectsFilter', 'orgUnitFilter', 'lecturerFilter'];
    var selectedModules = RatingController.getRatedModuleIds();
    Template.moduleTable.resetTable(allModules, columns, null, selectedModules);
};