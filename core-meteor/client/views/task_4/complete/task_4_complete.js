Template.task4Complete.rendered = function() {
    //if(_.isUndefined(ExperimentSubject.findOne()) || _.isUndefined(ExperimentSubject.findOne().recommendationMatrix)){
    //    Router.go('notAllowed');
    //}
    //Meteor.call('updateTask3State', 6);
    //Meteor.call('task3Complete', Meteor.userId());
}

Template.task4Complete.helpers({
    'matchFirstName': function(){
        if(ExperimentSubject.find().fetch()[0] && ExperimentSubject.find().fetch()[0].matchPartnerCallName){
            if(ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0]){
                return ExperimentSubject.find().fetch()[0].matchPartnerCallName.split(' ')[0];
            } else{
                return ExperimentSubject.find().fetch()[0].matchPartnerCallName;
            }
        } else{
            return 'none';
        }   
    }
});
Template.task4Complete.events({
    'click #logout': function() {
        Meteor.call('task3Logout', Meteor.userId());
        Router.go('home');
        Meteor.logout(function() {
            //Navigat to Shibboleth Logout URL on success
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.logout(function() {
                        window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                    });
                } else {
                    window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                }
            });
        });
    },
    'click #debugReset': function() {
        Meteor.call('updateTask3State', 0);
    },
    'click #randomTreatment' : function(){
        Meteor.call('storeRandomTreatments', 'kjoDae8dc', function(error){
            var pers = ExperimentSubject.findOne().personalizedTreatment;
            var right = ExperimentSubject.findOne().humanListIsRight;
            var text = 'Treatment will be ';
            if(pers == 1){
                text += 'personalized ';
            } else{
                text += 'anonymized ';
            }
            text += ' and the human list will be '
            if(right == 1){
                text += 'right.';
            } else{
                text += 'left.';
            }
            window.alert(text);
        });
    }
});
