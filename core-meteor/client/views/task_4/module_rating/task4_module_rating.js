Template.task4ModuleRating.rendered = function() {
    UIHelper.refreshRateit();
    $('[data-toggle="tooltip"]').tooltip();
};

Template.task4ModuleRating.helpers({
    i18nContinue: function() {
        var remainingModules = RatingController.getMinRatingThreshold() - RatingController.countRatedModules();
        if (remainingModules > 1) {
            return i18n('endRatings') + ' (' + i18n('minRating') + ' ' + remainingModules + ' ' + i18n('minRatingModulePlural') + ')';
        } else if (remainingModules === 1) {
            return i18n('endRatings') + ' (' + i18n('minRating') + ' ' + remainingModules + ' ' + i18n('minRatingModuleSingular') + ')';
        } else {
            return i18n('endRatings');
        }
    },
    continueDisabled: function() {
        var remainingModules = RatingController.countRatedHS16Modules();
        return remainingModules > 0;
    },

    i18nBack: function() {
        return i18n('back');
    },

    modules: function() {
        return Modules.getHS16ModulesFromCurrentUser();
    },

    rating: function() {
        return RatingController.getRating(this);
    }

});

Template.task4ModuleRating.events({
    'click #step4-next': function() {
        //PredictionController.startPredictionUpdateProcess();
        Router.go('task4Complete');
    },

    'click #step4-back': function() {
        Router.go('task4ModuleSelection');
    }
});