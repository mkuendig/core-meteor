// The modules variable is set according to the modules that should be displayed
// The datagrid reads the modules from this variable when it is rendered

Template.modules.created = function() {
    var allModules = Modules.find().fetch();
    var columns = ['moduleId', 'title', 'ects', 'prediction'];
    if (Session.get('moduleTableUrl') !== '/modules') {
        Template.moduleTable.resetTable(allModules, columns);
    }
};