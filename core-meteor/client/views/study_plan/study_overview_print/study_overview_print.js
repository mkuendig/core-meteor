var regulationId = '';
var studyId = '';

var findRegulation = function(regulationId) {
    return _.find(Meteor.user().profile[studyId].regulations, function(regulation) {
        return regulation.id === regulationId;
    });
};

Template.studyPlanTable.created = function() {
    studyId = this.data.studyId;
};

Template.studyPlanTable.rendered = function() {
    var definedEcts = 0;
    var completedEcts = 0;
    _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
        completedEcts += UIHelper.completedEcts(regulation);
        definedEcts += regulation.ects;
    });
};

Template.studyPlanTable.helpers({
    getStudyId: function() {
        return studyId;
    },

    dynamicTitle: function() {
        return i18n('your') + ' ' + i18n(studyId) + ' ' + i18n('study');
    },

    regulationCompletedEcts: function() {
        return UIHelper.completedEcts(this);
    },

    totalCompletedEcts: function() {
        var sum = 0;
        _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
            sum += UIHelper.completedEcts(regulation);
        });
        return sum;
    },

    totalEcts: function() {
        return UIHelper.totalEctsRegulation(studyId);
    },

    regulations: function() {
        return UIHelper.getRegulations(studyId);
    },

    modules: function() {
        if (regulationId) {
            var regulation = findRegulation(regulationId);
            var modules = Modules.getModulesFromCurrentUser();
            if (regulation && regulation.modules) {
                var regulationModuleIds = Modules.find({
                    moduleId: {
                        $in: regulation.modules
                    }
                }, {
                    fields: {
                        moduleId: 1
                    }
                }).fetch();
                regulationModuleIds = _.pluck(regulationModuleIds, 'moduleId');
                _.each(modules, function(module) {
                    module.checked = _.contains(regulationModuleIds, module.moduleId) ? true : false;
                });
            }
            return modules;
        }
    }
});

Template.studyPlanTable.events({
    'click #csvExport': function() {
        var modules = Modules.getModulesFromCurrentUser();
        var csvExporter = new CSVExporter();
        csvExporter.addContent(studyId, modules);
        csvExporter.download();
    }

});

Template.regulationRowPrint.helpers({
    regulationModules: function(moduleIds) {
        return UIHelper.regulationModules(moduleIds);
    },

    regulationSelfDefinedModules: function(selfDefinedModuleIds) {
        return UIHelper.regulationSelfDefinedModules(selfDefinedModuleIds);
    }
});