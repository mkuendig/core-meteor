Template.regulationBox.rendered = function() {
    $('#' + this.data.id).easyPieChart();
    UIHelper.initiateDrop();
    this.autorun(function() {
        var data = Template.currentData();
        var percentage = Math.floor(UIHelper.completedEcts(data) / data.ects * 100);
        $('#' + data.id).data('easyPieChart').update(percentage);
    });
    $('[data-toggle="tooltip"]').tooltip();
};

Template.regulationBox.helpers({
    percentage: function() {
        return Math.floor(UIHelper.completedEcts(this) / this.ects * 100);
    },

    completedEcts: function() {
        return UIHelper.completedEcts(this);
    }
});

Template.regulationBox.events({
    'click #regulation-settings': function() {
        $('#create-modal').modal();
        $('#create-modal-title').html(i18n('editRegulation'));
        $('#regulation-name-input').val(this.name);
        $('#regulation-ects-input').val(this.ects);
        $('#create-modal-form').attr('data-event', 'update-regulation');
        $('#create-modal-form').attr('data-regulation-id', this.id);
        $('#create-modal-submit').html(i18n('edit'));
    },

    'click #regulation-remove': function() {
        $('#remove-button').attr('data-regulation-id', this.id);
        $('#remove-button').attr('data-event', 'remove-regulation');
        $('#remove-title').html(i18n('removeRegulation'));
        $('#remove-modal').modal();
    }
});