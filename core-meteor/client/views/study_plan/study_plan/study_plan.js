var regulationId = '';
var studyId = ''; // 'bsc' or 'msc'
var modules;

var findRegulation = function(regulationId) {
    if (Meteor.user().profile[studyId]) {
        return _.find(Meteor.user().profile[studyId].regulations, function(regulation) {
            return regulation.id === regulationId;
        });
    } else {
        return [];
    }
};

var fetchModules = function() {
    modules = Modules.getModulesFromCurrentUser();
    _.each(modules, function(module) {
        _.find(Meteor.user().profile[studyId].regulations, function(regulation) {
            if (_.contains(regulation.modules, module.moduleId)) {
                module.regulation = regulation.name;
                module.regulationId = regulation.id;
                return true; // break out of _.find
            } else {
                module.regulation = '';
            }
        });
    });
};

Template.studyPlan.created = function() {
    // set studyId in this.autorun() to allow directly switching from bsc to msc (created is not called again automatically because it is the same template)
    this.autorun(function() {
        var data = Template.currentData();
        studyId = data.studyId;
    });
    fetchModules();
};

Template.studyPlan.rendered = function() {
    this.autorun(function() {
        Template.currentData();
        fetchModules();
        var chartData = [];
        var definedEcts = 0;
        var completedEcts = 0;
        var totalEctsInChartData = 0; // to prevent error when calling $.plot
        if (Meteor.user().profile[studyId]) {
            _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
                var slice = {};
                slice.label = regulation.name;
                var ects = UIHelper.completedEcts(regulation);
                totalEctsInChartData += ects;
                completedEcts += ects;
                definedEcts += regulation.ects;
                slice.data = ects;
                chartData.push(slice);
            });
        }

        chartData.push({
            label: i18n('unassigned'),
            data: definedEcts - completedEcts,
            color: '#F2F2F2'
        });

        if (chartData.length > 1 && totalEctsInChartData > 0) {
            $.plot($('#study-chart'), chartData, {
                series: {
                    pie: {
                        radius: 0.75,
                        innerRadius: 0.25,
                        show: true,
                        label: {
                            show: true,
                            radius: 0.95,
                            formatter: function(label) {
                                return '<div style=\'text-align:center;padding-left:3%;padding-right:3%;border-radius:6px;\'>' + label + '</div>';
                            },
                            background: {
                                opacity: 0.5,
                                color: '#FFFFFF'
                            }
                        }
                    }
                },
                colors: ['#25396E', '#415494', '#5B73BD', '#7D98DB', '#7DABDB', '#B2DAF7', '#C0E7FA'],
                legend: {
                    show: false
                }
            });
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    UIHelper.initiateDrag(false);
};

Template.studyPlan.helpers({
    getStudyId: function() {
        return studyId;
    },

    isBsc: function() {
        return studyId === 'bsc';
    },

    dynamicTitle: function() {
        return i18n('your') + ' ' + i18n(studyId) + ' ' + i18n('study');
    },

    regulationCompletedEcts: function() {
        return UIHelper.completedEcts(this);
    },

    totalCompletedEcts: function() {
        var sum = 0;
        if (Meteor.user().profile[studyId]) {
            _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
                sum += UIHelper.completedEcts(regulation);
            });
        }
        return sum;
    },

    totalEcts: function() {
        return UIHelper.getSumTotalEcts(studyId);
    },

    regulations: function() {
        var regulations = UIHelper.getRegulations(studyId);
        if (!regulations || regulations.length === 0) {
            $('[data-toggle="tooltip"]').tooltip();
        }
        return regulations;
    },

    modules: function() {
        return modules;
    },

    checked: function() {
        return regulationId === this.regulationId;
    },

    studyPlanImg: function() {
        return '/images/welcome_page/study_plan_' + Session.get('language') + '.png';
    }
});

Template.studyPlan.events({
    'click #csvExport': function() {
        var userModules = Modules.getModulesFromCurrentUser();
        var csvExporter = new CSVExporter();
        csvExporter.addContent(studyId, userModules);
        csvExporter.download();
    },

    'click #new-regulation': function() {
        document.getElementById('create-modal-form').reset();
        $('#create-modal').modal();
        $('#create-modal-form').attr('data-event', 'add-regulation');
        $('#regulation-name-input').attr('placeholder', i18n('regulationNameExample'));
        $('#create-modal-title').html(i18n('createNewRegulation'));
        $('#create-modal-submit').html(i18n('create'));
    },

    'click .remove-self-defined-module': function() {
        $('#remove-button').attr('data-self-module-id', this.id);
        $('#remove-button').attr('data-event', 'remove-self-module');
        $('#remove-title').html(i18n('removeModule'));
        $('#remove-modal').modal();
    },

    'click .remove-module-regulation': function(event) {
        var moduleId = $(event.target).attr('data-module-id');
        Meteor.call('removeModuleFromRegulations', moduleId);
    },


    'click .edit-self-defined-module': function() {
        $('#create-modal-title').html(i18n('editModule'));
        $('#regulation-name-input').val(this.name);
        $('#regulation-ects-input').val(this.ects);
        $('#create-modal-form').attr('data-event', 'update-self-defined-module');
        $('#create-modal-form').attr('data-self-module-id', this.id);
        $('#create-modal-submit').html(i18n('edit'));
        $('#create-modal').modal();
    },

    'click .add-self-defined-module': function() {
        var regulationId = this.id;
        document.getElementById('create-modal-form').reset();
        $('#create-modal-form').attr('data-event', 'add-self-defined-module');
        $('#regulation-name-input').attr('placeholder', i18n('moduleNameExample'));
        $('#create-modal-form').attr('data-regulation-id', regulationId);
        $('#create-modal-title').html(i18n('createNewSelfDefinedModule'));
        $('#create-modal-submit').html(i18n('create'));
        $('#create-modal').modal();
    },

    'submit form': function(event) {
        event.preventDefault();
        $('#create-modal').modal('hide');
        var id, name = event.target.name.value;
        var ects = parseFloat(event.target.ects.value);
        var eventType = $('#create-modal-form').attr('data-event');
        if (eventType === 'add-regulation') {
            Meteor.call('addRegulation', studyId, name, ects);
        } else if (eventType === 'update-regulation') {
            id = $('#create-modal-form').attr('data-regulation-id');
            Meteor.call('updateRegulation', studyId, id, name, ects);
        } else if (eventType === 'add-self-defined-module') {
            var regulationId = $('#create-modal-form').attr('data-regulation-id');
            Meteor.call('addSelfDefinedModule', name, ects, studyId, regulationId);
        } else if (eventType === 'update-self-defined-module') {
            id = $('#create-modal-form').attr('data-self-module-id');
            Meteor.call('updateSelfDefinedModule', id, name, ects);
        }
    },

    'click #remove-button': function() {
        var id, eventType = $('#remove-button').attr('data-event');
        $('#remove-modal').modal('hide');
        if (eventType === 'remove-regulation') {
            id = $('#remove-button').attr('data-regulation-id');
            var regulation = findRegulation(id);
            _.each(regulation.selfDefinedModules, function(selfDefinedModuleId) {
                Meteor.call('removeSelfDefinedModule', selfDefinedModuleId);
            });
            Meteor.call('removeRegulation', id);
        } else if (eventType === 'remove-self-module') {
            id = $('#remove-button').attr('data-self-module-id');
            Meteor.call('removeSelfDefinedModule', id);
        }
        $('#remove-button').attr('data-regulation-id', '');
        $('#remove-button').attr('data-self-module-id', '');
        $('#remove-button').attr('data-event', '');
    },

    'click .add-modules': function() {
        regulationId = this.id;
        $('#select-modules-modal').modal();
    },

    'click .module-select-checkbox': function(event) {
        var targetElement = $(event.target);
        var moduleId = targetElement.attr('data-module-id');
        var module = _.find(modules, function(userModule) {
            if (userModule.moduleId === moduleId) {
                return true;
            }
        });
        var regulation = findRegulation(regulationId);
        targetElement.toggleClass('text-success');
        if (targetElement.hasClass('text-success')) {
            module.regulation = regulation.name;
            module.regulationId = regulationId;
            Meteor.call('addModuleToRegulation', moduleId, studyId, regulationId);
            Toast.success(i18n('moduleAddedToSemester'));
        } else {
            module.regulation = '';
            module.regulationId = '';
            Meteor.call('removeModuleFromRegulations', moduleId);
            Toast.success(i18n('moduleRemovedFromSemester'));
        }
    },

    'click #example': function() {
        $('#show-example-modal').modal();
    }
});

Template.regulationRow.rendered = function() {
    this.autorun(function() {
        Template.currentData();
        UIHelper.initiateDrag(false);
    });
    $('[data-toggle="tooltip"]').tooltip();
};

Template.regulationRow.helpers({
    regulationModules: function(moduleIds) {
        return UIHelper.regulationModules(moduleIds);
    },

    regulationSelfDefinedModules: function(selfDefinedModuleIds) {
        return UIHelper.regulationSelfDefinedModules(selfDefinedModuleIds);
    }
});