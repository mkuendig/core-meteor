Template.semesterPlanModules.created = function() {
    var semesterPlanId = Session.get('semesterPlanId');
    var semesterId = Session.get('semesterId');
    var checkedModules = SemesterPlans.getModuleIds(semesterPlanId);
    var columns = ['moduleSelector', 'moduleId', 'title', 'ects', 'prediction'];
    var modules = Modules.getModulesForSemester(semesterId.toUpperCase()).fetch();
    Template.moduleTable.setCheckedModules(checkedModules);
    if (Session.get('moduleTableUrl') !== '/semester-plan/' + semesterId + '/modules') {
        Template.moduleTable.resetTable(modules, columns, semesterId, checkedModules);
    }
};

Template.semesterPlanModules.helpers({
    semesterId: function() {
        return Session.get('semesterId');
    }
});