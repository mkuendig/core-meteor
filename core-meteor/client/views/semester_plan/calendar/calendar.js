Template.semesterPlanCalendar.rendered = function() {
    $('[data-toggle="tooltip"]').tooltip();
};

Template.semesterPlanCalendar.helpers({
    modules: function() {
        return Modules.getSemesterPlanModules(Session.get('semesterPlanId'));
    },

    semesterId: function() {
        return Session.get('semesterId');
    },

    semesterPlanId: function() {
        return Session.get('semesterPlanId');
    },

    availableLecturesSelector: function(moduleId) {
        return {
            id: moduleId + '-dropdown',
            placeholder: i18n('placeHolderLecture'),
            multiple: 'multiple',
            options: {
                moduleId: moduleId
            }
        };
    },

    lectureOptions: function() {
        var selectedLectures = this.selectedLectures;
        var options, lectureName = '';
        _.each(this.lectures, function(lecture) {
            if (lecture.scheduleItems.length < 1) {
                return;
            }
            var startDatetime = moment(lecture.scheduleItems[0].startDatetime);
            if (lecture.lectureCategory.substring(0, 6) !== 'Übung') {
                lectureName = lecture.lectureCategory + ' ' + moment(startDatetime).format('dd HH:mm');
            } else {
                lectureName = 'Übung ' + moment(startDatetime).format('dd HH:mm');
            }
            var selected = _.contains(selectedLectures, lecture.uzhId) ? 'selected' : '';
            options += '<option value="' + lecture.uzhId + '" ' + selected + '>' + lectureName + '</option>';
        });
        return options;
    },

    multipleLectures: function() {
        var exercises = _.filter(this.lectures, function(lecture) {
            return lecture.lectureCategory !== 'Vorlesung';
        });
        return exercises.length > 1;
    }
});

Template.semesterPlanCalendar.events({
    'change [id$=dropdown]': function(event) {
        Meteor.call('setSelectedLectures', Session.get('semesterPlanId'), this.options.moduleId, event.val);
        $('#calendar').fullCalendar('refetchEvents');
    },

    'click .removeModule': function(event) {
        var moduleId = $(event.target).attr('data-module-id');
        Meteor.call('removeModuleFromSemesterPlan', Session.get('semesterId'), moduleId);
        Toast.success(i18n('moduleRemovedFromSemester'));
        Template.calendar.refetchEvents();
    },

    'click #export-button': function() {
        var icsExporter = new ICSExporter();
        var modules = Modules.getSemesterPlanModules(Session.get('semesterPlanId'));
        _.each(modules, function(module) {
            _.each(module.lectures, function(lecture) {
                if (_.contains(module.selectedLectures, lecture.uzhId)) {
                    _.each(lecture.scheduleItems, function(scheduleItem) {
                        var rooms = _.pluck(scheduleItem.rooms, 'name').join(', ');
                        icsExporter.addEvent(module.title, module.moduleId, rooms, scheduleItem.startDatetime, scheduleItem.endDatetime);
                    });
                }
            });
        });
        icsExporter.download('stundenplan.ics');
        Meteor.call('logEvent', 'INFO', Meteor.userId() + ' - calendar export');
    }
});