var semesterPlanId;

Template.semesterPlanOverview.created = function() {
    semesterPlanId = Session.get('semesterPlanId');
};

Template.semesterPlanOverview.rendered = function() {
    UIHelper.refreshRateit();
    $('[data-toggle="tooltip"]').tooltip();
};
Template.semesterPlanOverview.helpers({
    semesterId: function() {
        return Session.get('semesterId');
    },

    plannedModules: function() {
        var plannedModules = Modules.getSemesterPlanModules(semesterPlanId);
        _.each(plannedModules, function(module) {
            module.rating = RatingController.getRating(module);
            module.prediction = PredictionController.getPrediction(module);
        });
        return plannedModules;
    },

    totalEcts: function() {
        var modules = Modules.getSemesterPlanModules(semesterPlanId);
        var totalEcts = 0;
        _.each(modules, function(module) {
            totalEcts += module.ects;
        });
        return totalEcts;
    },
    i18nTooltipDelete: function() {
        return i18n('tooltipDelete');
    },
    getTitlePrediction: function() {
        return i18n('prediction') + UIHelper.userName();
    }
});

Template.semesterPlanOverview.events({
    'click .removeModule': function(event) {
        var moduleId = $(event.target).attr('data-module-id');
        Meteor.call('removeModuleFromSemesterPlan', semesterPlanId, moduleId);
        Toast.success(i18n('moduleRemovedFromSemester'));
    }
});