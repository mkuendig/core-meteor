Template.semesterPlanHeader.helpers({
    nrPlannedModules: function () {
        return Modules.getSemesterPlanModules(Session.get('semesterPlanId')).length;
    }
});