var getMandatoryModulesFilter = function() {
    var studyProfile = UserProfileController.getCurrentProfile();
    var studyField = studyProfile.field.slice(0, studyProfile.field.lastIndexOf('_'));
    return studyField + '_mandatory';
};


Template.semesterPlanRecommendations.helpers({

    semesterId: function() {
        return Session.get('semesterId');
    },

    planSemesterId: function() {
        return i18n('planYour') + ' ' + i18n(Session.get('semesterId'));
    },

    mandatoryModulesExist: function() {
        var semesterTitle = Session.get('semesterId') ? Session.get('semesterId').toUpperCase() : 'notFound';
        var mandatoryModulesRegExp = new RegExp(getMandatoryModulesFilter());
        var mandatoryModules = Modules.find({
            'categories.slug': mandatoryModulesRegExp,
            'predictions.userId': Meteor.userId(),
            'semester.title': semesterTitle,
            'ratings.userId': {
                $nin: [Meteor.userId()]
            }
        });
        return mandatoryModules.count() > 0;
    },

    mandatoryModulesFilter: function() {
        return getMandatoryModulesFilter();
    },

    recommendationsTitle: function() {
        var userName = UIHelper.userName();
        if (_.isEmpty(userName)) {
            userName = i18n('you');
        }
        return i18n('recommendationsFor') + userName;
    }
});