// The modules variable is set according to the modules that should be displayed
// The datagrid reads the modules from this variable when it is rendered

var categoryIndexDep = new Tracker.Dependency();
var selectedDep = new Tracker.Dependency();
var moduleCategories,
    categoryIndex,
    selectedModules,
    suggestedModules;
var allSelected = false;

var getSuggestedModules = function() {
    categoryIndexDep.depend();
    selectedModules = RatingController.getRatedModuleIds();
    if (moduleCategories[categoryIndex]) {
        var category = moduleCategories[categoryIndex];
        suggestedModules = Modules.find({
            categories: {
                $elemMatch: {
                    moduleType: category.moduleType,
                    slug: category.slug
                }
            }
        }, {
            sort: {
                moduleId: 1
            }
        });
        return suggestedModules;
    } else {
        selectedModules = RatingController.getRatedModuleIds();
        var allModules = Modules.find().fetch({}, {
            sort: {
                moduleId: 1
            }
        });
        var columns = ['moduleSelectorWizard', 'moduleIdWizard', 'title', 'ects'];
        Template.moduleTable.resetTable(allModules, columns, null, selectedModules);
        return null;
    }
};

var refreshTooltips = function() {
    $('[data-toggle="tooltip"]').tooltip('destroy');
    setTimeout(function() {
        $('[data-toggle="tooltip"]').tooltip();
    }, 500);
};

var getProgress = function() {
    return Math.round((categoryIndex) / (moduleCategories.length + 1) * 100) + '%';
};

var sortModuleCategories = function(moduleCategories) {
    var mandatoryModules = _.where(moduleCategories, {
        moduleType: 'mandatory_modules'
    });
    var compulsoryElectiveModules = _.where(moduleCategories, {
        moduleType: 'compulsory_elective_modules'
    });
    var electiveModules = _.where(moduleCategories, {
        moduleType: 'elective_modules'
    });
    return _.union(mandatoryModules, compulsoryElectiveModules, electiveModules);
};

Template.moduleSelectionWizard.events({
    'click #step3-next': function() {
        Router.go('moduleRatingWizard');
        Meteor.call('logImportMethod', 'Imported modules via manual selection', Meteor.userId(), new Date().toString());
    },

    'click #step3-copyRecord': function() {
        Router.go('copyPasteImportWizard');
    },

    'click #suggested-modules-next': function() {
        allSelected = false;
        categoryIndex += 1;
        selectedDep.changed();
        categoryIndexDep.changed();
        $('.scrollable').animate({
            scrollTop: 0
        }, 500);
        $('.progress-bar').css('width', getProgress());
        $('#progressText').html(getProgress());
    },

    'click #suggested-modules-back': function() {
        categoryIndex -= 1;
        categoryIndexDep.changed();
        $('.scrollable').animate({
            scrollTop: 0
        }, 500);
        $('.progress-bar').css('width', getProgress());
        $('#progressText').html(getProgress());
    },

    'click .module-suggestion-checkbox': function(event) {
        refreshTooltips();
        var targetElement = $(event.target);
        var moduleId = targetElement.attr('data-module-id');
        targetElement.toggleClass('text-success');
        Meteor.call('toggleRating', moduleId);
        if (targetElement.hasClass('text-success')) {
            Toast.success(i18n('user_module_relations_selection_added'));
        } else {
            Toast.success(i18n('user_module_relations_selection_deleted'));
        }
    },

    'click .select-all': function() {
        $('.fa-plus').click();
        allSelected = true;
        selectedDep.changed();
        refreshTooltips();
    },

    'click .unselect-all': function() {
        $('.fa-check').click();
        allSelected = false;
        selectedDep.changed();
        refreshTooltips();
    },

    'click #step3-back': function() {
        Router.go('importMethodWizard');
    }
});

Template.moduleSelectionWizard.rendered = function() {
    $('[data-toggle="tooltip"]').tooltip();
};

Template.moduleSelectionWizard.created = function() {
    moduleCategories = Categories.getDirectChildren(Meteor.user().profile.studyProfile.current.field);
    moduleCategories = sortModuleCategories(moduleCategories);
    categoryIndex = 0;
};

Template.moduleSelectionWizard.helpers({
    progressWidth: function() {
        return 'width: ' + getProgress();
    },
    progress: function() {
        return getProgress();
    },
    suggestedModules: function() {
        return getSuggestedModules();
    },

    categoryTitle: function() {
        categoryIndexDep.depend();
        if (moduleCategories[categoryIndex]) {
            return moduleCategories[categoryIndex].title;
        }
    },

    allSelected: function() {
        selectedDep.depend();
        return allSelected;
    },

    zeroIndex: function() {
        categoryIndexDep.depend();
        return categoryIndex === 0;
    },

    checked: function() {
        return _.contains(selectedModules, this.moduleId);
    },

    moreThanTenModules: function() {
        return getSuggestedModules().count() > 10;
    },

    i18nContinue: function() {
        var remainingModules = RatingController.getMinRatingThreshold() - RatingController.countSelectedModules();
        if (remainingModules > 1) {
            return i18n('endSelection') + ' (' + i18n('minSelection') + ' ' + remainingModules + ' ' + i18n('minSelectionModulePlural') + ')';
        } else if (remainingModules === 1) {
            return i18n('endSelection') + ' (' + i18n('minSelection') + ' ' + remainingModules + ' ' + i18n('minSelectionModuleSingular') + ')';
        } else {
            return i18n('endSelection');
        }
        return i18n('endSelection');
    },

    continueDisabled: function() {
        var remainingModules = RatingController.getMinRatingThreshold() - RatingController.countSelectedModules();
        return remainingModules > 0;
    },

    i18nCopyRecordButton: function() {
        return i18n('copyRecordButton');
    },

    i18nBackToImportSelection: function() {
        return i18n('backToImportSelection');
    }
});