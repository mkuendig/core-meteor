Template.copyPasteImportWizard.helpers({
    i18nContinue: function() {
        return i18n('continue');
    },
    i18nManualSelectionButton: function() {
        return i18n('changeToManualSelectionButton');
    },
    i18nback: function() {
        return i18n('back');
    }
});


Template.copyPasteImportWizard.events({
    'click #save': function() {
        Template.copyPasteImportTemplate.save();
        Router.go('moduleRatingWizard');
    },
    'click #step3-manualSelection': function() {
        Router.go('moduleSelectionWizard');
    },
    'click #step3-back': function() {
        Router.go('importMethodWizard');
    }
});