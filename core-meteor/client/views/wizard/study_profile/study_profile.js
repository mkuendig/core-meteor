Template.studyProfileWizard.disableButtons = function() {
    $('#next').addClass('disabled');
};

Template.studyProfileWizard.enableButtons = function() {
    $('#next').removeClass('disabled');
};

Template.studyProfileWizard.events({
    'click #next': function() {
        Template.studyProfileTemplate.saveProfile();
        Router.go('importMethodWizard');
    }
});