Template.wizardHeaderTab.helpers({
    'isCompleted': function(routeName) {
        var routes = ['studyProfileWizard', 'importMethodWizard', 'copyPasteImportWizard', 'moduleSelectionWizard', 'moduleRatingWizard'],
            activeRoute = Router.current().route.getName(), //Router.current().route.name,
            activeRouteIndex = _.indexOf(routes, activeRoute),
            routeIndex = _.indexOf(routes, routeName);
        return routeIndex < activeRouteIndex;
    }
});