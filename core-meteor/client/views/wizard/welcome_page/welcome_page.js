Template.welcomePage.helpers({
    i18nContinue: function() {
        if ($('#userAgreementCheckbox:checkbox:checked').length > 0) {
            return i18n('next');
        }
        return i18n('welcomePageSubmitButton');
    },

    continueDisabled: function() {
        if ($('#userAgreementCheckbox:checkbox:checked').length > 0) {
            return false;
        }
        return true;
    }
});

Template.welcomePage.events({
    'change #userAgreementCheckbox': function(event) {
        var continueButton = jQuery('#step0-next');
        if (event.target.checked) {
            continueButton.val(i18n('next'));
            continueButton.prop('disabled', false);
        } else {
            continueButton.val(i18n('welcomePageSubmitButton'));
            continueButton.prop('disabled', true);
        }
    },
    'click #showUserAgreement': function() {
        $('#userAgreementModal').modal();
    },
    'click #showNetworkUserAgreement': function() {
        $('#networkUserAgreementModal').modal();
    },
    'click #step0-next': function(event) {
        event.preventDefault();
        UserAgreementController.acceptCurrentUserAgreement();
        if ($('#networkUserAgreementCheckbox:checkbox:checked').length > 0) {
            UserAgreementController.acceptCurrentNetworkUserAgreement();
        } else {
            UserAgreementController.denyNetworkUserAgreement();
        }
        Router.go('studyProfileWizard');
    }
});
