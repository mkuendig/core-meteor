Template.registrationWelcomePage.rendered = function() {
    Meteor.call('checkRegistrationThreshold', function(error, result){
        if(error){
            Router.go('registrationWelcomePage');
        } else{
            if(result){
                Router.go('registrationWelcomePage');
            } else{
                Router.go('registrationLogout');
            }
        }
    });
}


Template.registrationWelcomePage.helpers({
    i18nContinue: function() {
        return i18n('welcomePageSubmitButton');
    },

    isDisabled: function() {
        if ($('#participationAgreementCheckbox:checkbox:checked').length > 0) {
            return false;
        }
        return true;
    },

    threshold: function() {
        return false;
    }
});

Template.registrationWelcomePage.events({
    'change #participationAgreementCheckbox': function(event) {
        var agreementButton = jQuery('#agreementButton');
        if (event.target.checked) {
            agreementButton.prop('disabled', false);
        } else {
            agreementButton.prop('disabled', true);
        }
    },
    'click #agreementButton': function(event) {
        event.preventDefault();
        
        UserAgreementController.acceptCurrentUserAgreement();
        if ($('#participationAgreementCheckbox:checkbox:checked').length > 0) {
            Router.go('registrationAAI');
        } else {
            window.alert("Please accept the the participation agreement before continuing.");
        }
    }
});
