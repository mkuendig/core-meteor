Template.registrationCopyPasteImport.helpers({
    i18nContinue: function() {
        return i18n('continue');
    },
    i18nManualSelectionButton: function() {
        return i18n('changeToManualSelectionButton');
    },
    i18nback: function() {
        return i18n('backToImportSelection');
    }
});


Template.registrationCopyPasteImport.events({
    'click #save': function() {
        Template.copyPasteImportTemplate.save();
        Router.go('registrationModuleRating');
    },
    'click #step3-manualSelection': function() {
        Router.go('registrationModuleSelection');
    },
    'click #step3-back': function() {
        Router.go('registrationImportMethod');
    }
});