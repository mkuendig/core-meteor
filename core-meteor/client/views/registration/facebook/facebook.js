Template.registrationFacebook.helpers({
    i18nConnect: function() {
        return i18n('facebookConnectButton');
    }
});
Template.registrationFacebook.events({
    'click #connectButton': function(event) {
    	FBController.loginToFB();
    	//Router.go('registrationStudyInformation');
    }
});
