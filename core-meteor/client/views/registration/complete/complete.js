Template.registrationComplete.rendered = function() {
    Meteor.call('completeRegistration', function(error, result){
        if(error){
            console.log(error);
        }
        if(result == 0){
        } else if (result == 1){
            var text = '<strong>' + result + '</strong> <b>of your Facebook friends</b> has';
            document.getElementById('numFriends').innerHTML = text;
        } else{
            var text = '<strong>' + result + '</strong> <b>of your Facebook friends</b> have';
            document.getElementById('numFriends').innerHTML = text;            
        }
        if(Session.get('mailInSessionSent') == undefined || !Session.get('mailInSessionSent')){
            Meteor.call('sendRegistrationMail', 0);
            Session.setPersistent('mailInSessionSent', true);
        }
    });
}

Template.registrationComplete.helpers({
    mail : function(){
        return '<b>'+Meteor.user().emails[0].address+'</b>';
    }
});
Template.registrationComplete.events({
    'click #restartModules': function(event) {
        Router.go('registrationStudyInformation');
    },
    'click #debugErase': function(event) {
    	Meteor.call('deleteUser', function(error) {
    		if(error){
    			window.alert(error);
    		} else{

            }
    	});
        window.alert("user deleted");
    },
    'click #logout': function() {
        Meteor.call('logLogout', Meteor.userId());
        Router.go('home');
        Meteor.logout(function() {
            //Navigat to Shibboleth Logout URL on success
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.logout(function() {
                        window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                    });
                } else {
                    window.location.href = window.location.origin + '/Shibboleth.sso/Logout';
                }
            });
        });
    },
    'click #resendMail': function() {
        window.alert('The confirmation email has been resent. This may take a few minutes.');
        Meteor.call('sendRegistrationMail', 1);
    }
});
