Template.registrationAAI.rendered = function() {
    Session.setPersistent('mailInSessionSent', false);
}

Template.registrationAAI.events({
    'click #debugLogin': function(event) {
        Meteor.call('createDummyLogin', function(error, id) {
            if (error) {
                console.log('error - createDummby\n' + error);
            } else {
                Meteor.loginWithPassword(id, id, function(error) {
                    if (typeof error === 'undefined') {
                        Router.go('aai');
                        }
                    else {
                        window.alert("Some error in debug mode");
                    }
                });
            }
        });
    }
});
