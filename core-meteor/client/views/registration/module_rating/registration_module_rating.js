Template.registrationModuleRating.rendered = function() {
    UIHelper.refreshRateit();
    $('[data-toggle="tooltip"]').tooltip();
};

Template.registrationModuleRating.helpers({
    i18nContinue: function() {
        var remainingModules = RatingController.getMinRatingThreshold() - RatingController.countRatedModules();
        if (remainingModules > 1) {
            return i18n('endRatings') + ' (' + i18n('minRating') + ' ' + remainingModules + ' ' + i18n('minRatingModulePlural') + ')';
        } else if (remainingModules === 1) {
            return i18n('endRatings') + ' (' + i18n('minRating') + ' ' + remainingModules + ' ' + i18n('minRatingModuleSingular') + ')';
        } else {
            return i18n('endRegistration');
        }
    },
    continueDisabled: function() {
        var remainingModules = RatingController.getMinRatingThreshold() - RatingController.countRatedModules();
        return remainingModules > 0;
    },

    i18nBack: function() {
        return i18n('addMoreModules');
    },

    modules: function() {
        return Modules.getModulesFromCurrentUser();
    },

    rating: function() {
        return RatingController.getRating(this);
    }

});

Template.registrationModuleRating.events({
    'click #step4-next': function() {
        PredictionController.startPredictionUpdateProcess();
        Router.go('registrationComplete');
    },

    'click #step4-back': function() {
        Router.go('registrationImportMethod');
    }
});