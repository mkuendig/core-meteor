Template.registrationStudyInformation.disableButtons = function() {
    $('#next').addClass('disabled');
};

Template.registrationStudyInformation.enableButtons = function() {
    $('#next').removeClass('disabled');
};

Template.registrationStudyInformation.events({
    'click #next': function() {
        Template.studyProfileTemplate.saveProfile();
        Meteor.call('updateFutureDegree',Session.get('futureDegree'));
        Router.go('registrationImportMethod');
    }
});