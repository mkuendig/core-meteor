Permission = (function() {
    var self = {};

    /**
     * Checks that the user owns the document
     * @param userId
     * @param doc
     * @returns {*|boolean}
     */
    self.isCreatedBySameUser = function(userId, doc) {
        return doc && doc.createdById === userId;
    };

    /**
     * Checks if the user is logged in
     * @param userId
     * @returns {*|boolean}
     */
    self.userIsLoggedIn = function(userId) {
        return userId;
    };

    /**
     * Always deny
     */
    self.denyAlways = function() {
        return false;
    };
    return self;
})();