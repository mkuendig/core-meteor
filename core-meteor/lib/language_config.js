LanguageConfig = (function() {

    var self = {};

    self.initializeLanguage = function() {
        var language;
        if (Meteor.user()) {
            language = Meteor.user().profile.preferredLanguage;
        } else {
            language = i18n.getLanguage() || navigator.language.substr(0, 2);
        }
        language = (language !== 'en') ? 'de' : 'en';
        Session.set('language', language);
        i18n.setLanguage(language);
        moment.locale(language);
    };

    self.setLanguage = function(language, reloadPage) {
        Session.set('language', language);
        i18n.setLanguage(language);
        moment.locale(language);
        if (Meteor.user()) {
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $set: {
                    'profile.preferredLanguage': language
                }
            });
            if (reloadPage) {
                location.reload();
            }
        }
    };

    return self;
})();