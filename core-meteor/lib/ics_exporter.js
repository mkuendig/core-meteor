ICSExporter = function() {

    var self = this;
    var calendarEvents = [];
    var calendarStart = [
        'BEGIN:VCALENDAR',
        'VERSION:2.0',
        'BEGIN:VTIMEZONE',
        'TZID:Europe/Zurich',
        'BEGIN:DAYLIGHT',
        'TZOFFSETFROM:+0100',
        'TZOFFSETTO:+0200',
        'TZONEZNAME:CEST',
        'DTSTART:19700329T020000',
        'RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU',
        'END:DAYLIGHT',
        'BEGIN:STANDARD',
        'TZOFFSETFROM:+0200',
        'TZOFFSETTO:+0100',
        'TZNAME:CET',
        'DTSTART:19701025T030000',
        'RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU',
        'END:STANDARD',
        'END:VTIMEZONE'
    ].join('\n');
    var calendarEnd = '\nEND:VCALENDAR';

    self.addEvent = function(subject, description, location, begin, stop) {

        var nowDate = moment();
        var startDate = moment(begin);
        var endDate = new moment(stop);

        var nowYear = nowDate.format('YYYY');
        var nowMonth = nowDate.format('MM');
        var nowDay = nowDate.format('DD');
        var nowHours = nowDate.format('HH');
        var nowMinutes = nowDate.format('mm');
        var nowSeconds = nowDate.format('ss');

        var startYear = startDate.format('YYYY');
        var startMonth = startDate.format('MM');
        var startDay = startDate.format('DD');
        var startHours = startDate.format('HH');
        var startMinutes = startDate.format('mm');
        var startSeconds = startDate.format('ss');

        var endYear = endDate.format('YYYY');
        var endMonth = endDate.format('MM');
        var endDay = endDate.format('DD');
        var endHours = endDate.format('HH');
        var endMinutes = endDate.format('mm');
        var endSeconds = endDate.format('ss');

        var now = nowYear + nowMonth + nowDay + 'T' + nowHours + nowMinutes + nowSeconds;
        var start = startYear + startMonth + startDay + 'T' + startHours + startMinutes + startSeconds;
        var end = endYear + endMonth + endDay + 'T' + endHours + endMinutes + endSeconds;

        var calendarEvent = [
            'BEGIN:VEVENT',
            'DESCRIPTION:' + description,
            'DTSTART:' + start,
            'DTEND:' + end,
            'DTSTAMP:' + now,
            'LOCATION:' + location,
            'SUMMARY:' + subject,
            'TRANSP:OPAQUE',
            'END:VEVENT'
        ].join('\n');
        calendarEvents.push(calendarEvent);
        return calendarEvent;
    };

    self.download = function(filename) {
        if (calendarEvents.length < 1) {
            return false;
        }
        var calendar = calendarStart + '\n' + calendarEvents.join('\n') + calendarEnd;
        var blob = new Blob([calendar], {
            type: 'text/x-vCalendar;charset=utf-8'
        });
        window.saveAs(blob, filename);
        return calendar;
    };
};