Select2i18n = function() {

    var self = this;

    self.init = function() {
        var select2Translations = {
            formatNoMatches: function() {
                return i18n('select2NoMatches');
            },
            formatInputTooShort: function() {
                return i18n('select2MoreCharacters');
            },
            formatInputTooLong: function() {
                return i18n('select2LessCharacters');
            }
        };
        $.extend($.fn.select2.defaults, select2Translations);
    };
};