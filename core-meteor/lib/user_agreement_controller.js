UserAgreementController = (function() {
    var self = {};

    self.currentUserAgreement = 4;
    self.currentNetworkUserAgreement = 1;

    self.hasAcceptedLatestUserAgreement = function() {
        var acceptedUserAgreement = Meteor.user().profile.userAgreement;
        return !_.isUndefined(acceptedUserAgreement) && acceptedUserAgreement === self.currentUserAgreement;
    };

    self.hasAcceptedLatestNetworkUserAgreement = function() {
        var acceptedNetworkUserAgreement = Meteor.user().profile.networkUserAgreement;
        if (_.isUndefined(acceptedNetworkUserAgreement)) {
            return false;
        } else if (acceptedNetworkUserAgreement < 0) {
            return true;
        } else if (acceptedNetworkUserAgreement === self.currentNetworkUserAgreement) {
            return true;
        } else {
            return false;
        }
    };

    self.hasDeniedNetworkUserAgreement = function() {
        return Meteor.user().profile.networkUserAgreement < 0;
    };

    self.acceptCurrentUserAgreement = function() {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $set: {
                'profile.userAgreement': self.currentUserAgreement
            }
        });
    };

    self.acceptCurrentNetworkUserAgreement = function() {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $set: {
                'profile.networkUserAgreement': UserAgreementController.currentNetworkUserAgreement
            }
        });
    };

    self.denyNetworkUserAgreement = function() {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $set: {
                'profile.networkUserAgreement': -1
            }
        });
    };

    self.resetNetwordUserAgreement = function() {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $set: {
                'profile.networkUserAgreement': 0
            }
        });
    };

    return self;
})();
