RatingController = (function() {

    var self = {};
    var MIN_RATING_THRESHOLD = 5;

    /**
     * Returns the minimal number of ratings that needs
     * to be made in order to leave the wizard.
     * @returns {number}
     */
    self.getMinRatingThreshold = function() {
        return MIN_RATING_THRESHOLD;
    };

    /**
     * Check if a module has been rated (or added
     * to his modules) by the logged in user
     * @param  {String} moduleId
     * @return {boolean}
     */
    self.ratingExists = function(moduleId) {
        return Modules.findOne({
            moduleId: moduleId,
            'ratings.userId': Meteor.userId()
        }) ? true : false;
    };

    /**
     * Update rating of the logged in user
     * @param  {String} moduleId
     * @param  {Number} rating null if no rating
     */
    self.updateRating = function(moduleId, rating) {
        var userId = Meteor.userId();
        var createdAt = new Date();
        rating = rating ? rating : null;
        Modules.update({
            moduleId: moduleId,
            'ratings.userId': userId
        }, {
            $set: {
                'ratings.$.rating': rating,
                'ratings.$.createdAt': createdAt
            }
        });
        if (rating !== null) {
            Meteor.call('exportRating', moduleId, rating, createdAt);
        } else {
            Meteor.call('deleteRatingFromRecommender', moduleId);
        }
        Meteor.call('logRating', 'UPDATED_RATING', userId, moduleId, new Date().toString(), rating);
    };

    /**
     * Create a rating for module by the logged in user
     * @param {String} moduleId
     * @param {Number} rating (optional)
     */
    self.addRating = function(moduleId, rating) {
        var userId = Meteor.userId();
        var createdAt = new Date();
        rating = rating ? rating : null;
        Modules.update({
            moduleId: moduleId
        }, {
            $addToSet: {
                ratings: {
                    userId: userId,
                    rating: rating,
                    createdAt: createdAt
                }
            }
        });
        if (rating !== null) {
            Meteor.call('exportRating', moduleId, rating, createdAt);
        }
        Meteor.call('logRating', 'NEW_RATING', userId, moduleId, new Date().toString(), rating);
    };

    /**
     * Remove rating of logged in user
     * @param  {String} moduleId
     */
    self.removeRating = function(moduleId) {
        var userId = Meteor.userId();
        Modules.update({
            moduleId: moduleId
        }, {
            $pull: {
                ratings: {
                    userId: userId
                }
            }
        });
        Meteor.call('deleteRatingFromRecommender', moduleId);
        Meteor.call('logRating', 'REMOVED_RATING', userId, moduleId, new Date().toString(), '');

    };

    /**
     * Get the current users rating of the provided module
     * @param {Object} Modules Collection entry
     * @return {Number}
     */
    self.getRating = function(module) {
        var moduleRating;
        _.find(module.ratings, function(rating) {
            if (rating.userId === Meteor.userId()) {
                moduleRating = rating.rating;
                return true;
            }
        });
        return moduleRating;
    };

    self.getAvgRating = function(module) {
        try {
            return module.stats.avgRating;
        } catch (err) {
            return null;
        }
    };

    self.getNumberRatings = function(module) {
        try {
            return module.stats.numberOfRatings;
        } catch (err) {
            return null;
        }
    };


    /**
     * Get the modules rated by the current user
     * @return {Array}
     */
    self.getRatedModuleIds = function() {
        var modules = Modules.find({
            'ratings.userId': Meteor.userId()
        }).fetch();
        return _.pluck(modules, 'moduleId');
    };

    /**
     * Count number of selected modules
     * @returns {*}
     */
    self.countSelectedModules = function() {
        return Modules.find({
            'ratings.userId': Meteor.userId()
        }).count();
    };

    /**
     * Count number of rated modules
     * @returns {*}
     */
    self.countRatedModules = function() {
        return Modules.find({
            'ratings.userId': Meteor.userId(),
            'ratings.rating': {
                $ne: null
            }
        }).count();
    };

    self.countRatedHS16Modules = function() {
        return Modules.find({
            'ratings.userId': Meteor.userId(),
            'semester.title': 'HS16',
            'ratings.rating': {
                $eq: null
            }            
        }).count();
    };

    /**
     * Gets the percentage rated modules
     * @returns {number}
     */
    self.getRatingPercentage = function() {
        var visitedModules = Modules.find({
            'ratings.userId': Meteor.userId()
        }).fetch();
        var ratedModules = _.filter(visitedModules, function(module) {
            return self.getRating(module);
        });
        var res = Math.round(ratedModules.length / visitedModules.length * 100);
        if(isNaN(res)){
            return 0;
        } else{
            return res;
        }
    };

    return self;
})();

Meteor.methods({

    setRating: function(moduleId, rating) {
        if (RatingController.ratingExists(moduleId)) {
            RatingController.updateRating(moduleId, rating);
        } else {
            RatingController.addRating(moduleId, rating);
        }
    },

    updateRating: function(moduleId, rating) {
        RatingController.updateRating(moduleId, rating);
    },

    resetRating: function(moduleId) {
        RatingController.updateRating(moduleId, null);
    },

    removeRating: function(moduleId) {
        RatingController.removeRating(moduleId);
    },

    /**
     * Add relation to module with rating null (visited but non-rated module)
     * @param {String} moduleId
     */
    toggleRating: function(moduleId) {
        if (RatingController.ratingExists(moduleId)) {
            RatingController.removeRating(moduleId);
        } else {
            RatingController.addRating(moduleId, null);
        }
    }
});
