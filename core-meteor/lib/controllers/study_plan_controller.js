StudyPlanController = (function() {

    var self = {};

    /**
     * Add a module to either the bsc or msc study overview
     * @param {String} studyId  Either 'bsc' or 'msc'
     * @param {String} moduleId
     */
    self.addModule = function(studyId, moduleId) {
        var modifier = {};
        modifier.$addToSet = {};
        modifier.$addToSet['profile.' + studyId + '.modules'] = moduleId;
        Meteor.users.update({
            _id: Meteor.userId()
        }, modifier);
    };

    /**
     * Remove a module completely from all study overviews
     * @param  {String} moduleId
     */
    self.removeModule = function(moduleId) {
        self.removeModuleFromRegulations(moduleId);
        _.each(['bsc', 'msc'], function(studyId) {
            var modifier = {};
            modifier.$pull = {};
            modifier.$pull['profile.' + studyId + '.modules'] = moduleId;
            Meteor.users.update({
                _id: Meteor.userId()
            }, modifier);
        });
    };

    /**
     * Create a new regulation and add it to the bsc or msc study overview
     * @param {String} studyId        Either 'bsc' or 'msc'
     * @param {String} regulationName
     * @param {String} regulationEcts Ects threshold of the regulation
     */
    self.addRegulation = function(studyId, regulationName, regulationEcts) {
        var key = 'profile.' + studyId + '.regulations';
        var modifier = {};
        var uniqId = Random.id(20);
        modifier.$addToSet = {};
        modifier.$addToSet[key] = {
            id: uniqId,
            name: regulationName,
            ects: regulationEcts
        };
        Meteor.users.update({
            _id: Meteor.userId()
        }, modifier);
    };

    /**
     * Update a regulation
     * @param  {String} regulationId
     * @param  {String} studyId
     * @param  {String} regulationName
     * @param  {String} regulationEcts
     */
    self.updateRegulation = function(studyId, regulationId, regulationName, regulationEcts) {
        var selector = {};
        selector._id = Meteor.userId();
        selector['profile.' + studyId + '.regulations.id'] = regulationId;
        var modifier = {};
        modifier.$set = {};
        modifier.$set['profile.' + studyId + '.regulations.$.name'] = regulationName;
        modifier.$set['profile.' + studyId + '.regulations.$.ects'] = regulationEcts;
        Meteor.users.update(selector, modifier);
    };

    /**
     * Remove a regulation from the bsc or msc study overview
     * @param  {String} studyId        Either 'bsc' or 'msc'
     * @param  {String} regulationName
     */
    self.removeRegulation = function(regulationId) {
        _.each(['bsc', 'msc'], function(studyId) {
            var modifier = {};
            modifier.$pull = {};
            modifier.$pull['profile.' + studyId + '.regulations'] = {
                id: regulationId
            };
            Meteor.users.update({
                _id: Meteor.userId()
            }, modifier);
        });
    };

    /**
     * Add a module to a regulation
     * @param {String} studyId        Either 'bsc' or 'msc'
     * @param {String} regulationId
     * @param {String} moduleId
     */
    self.addModuleToRegulation = function(moduleId, studyId, regulationId) {
        self.addModule(studyId, moduleId);
        var selector = {};
        selector._id = Meteor.userId();
        selector['profile.' + studyId + '.regulations.id'] = regulationId;
        var modifier = {};
        modifier.$addToSet = {};
        modifier.$addToSet['profile.' + studyId + '.regulations.$.modules'] = moduleId;
        Meteor.users.update(selector, modifier);
    };

    /**
     * Removes a moduleId from all regulations so no regulations contains the module anymore
     * @param  {String} moduleId
     */
    self.removeModuleFromRegulations = function(moduleId) {
        _.each(['bsc', 'msc'], function(studyId) {
            _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
                var selector = {};
                selector._id = Meteor.userId();
                selector['profile.' + studyId + '.regulations.id'] = regulation.id;
                var modifier = {};
                modifier.$pull = {};
                modifier.$pull['profile.' + studyId + '.regulations.$.modules'] = moduleId;
                Meteor.users.update(selector, modifier);
            });
        });
    };

    /**
     * Find the study overview that contains the given moduleId
     * @param  {String} moduleId
     * @return {String} 'bsc', 'msc', or undefined
     */
    self.getStudyOverview = function(moduleId) {
        if (Meteor.users.findOne({
                _id: Meteor.userId(),
                'profile.bsc.modules': moduleId
            })) {
            return 'bsc';
        } else if (Meteor.users.findOne({
                _id: Meteor.userId(),
                'profile.msc.modules': moduleId
            })) {
            return 'msc';
        }
    };

    self.addSelfDefinedModule = function(moduleName, ects) {
        var uniqId = Random.id(20);
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $addToSet: {
                'profile.selfDefinedModules': {
                    id: uniqId,
                    name: moduleName,
                    ects: ects
                }
            }
        });
        return uniqId;
    };

    self.removeSelfDefinedModule = function(id) {
        self.removeSelfDefinedModuleFromRegulations(id);
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $pull: {
                'profile.selfDefinedModules': {
                    id: id
                }
            }
        });
    };

    self.updateSelfDefinedModule = function(id, name, ects) {
        Meteor.users.update({
            _id: Meteor.userId(),
            'profile.selfDefinedModules.id': id
        }, {
            $set: {
                'profile.selfDefinedModules.$': {
                    id: id,
                    name: name,
                    ects: ects
                }
            }
        });
    };

    self.addSelfDefinedModuleToRegulation = function(id, studyId, regulationId) {
        var selector = {};
        selector._id = Meteor.userId();
        selector['profile.' + studyId + '.regulations.id'] = regulationId;
        var modifier = {};
        modifier.$addToSet = {};
        modifier.$addToSet['profile.' + studyId + '.regulations.$.selfDefinedModules'] = id;
        Meteor.users.update(selector, modifier);
    };

    self.removeSelfDefinedModuleFromRegulations = function(id) {
        _.each(['bsc', 'msc'], function(studyId) {
            _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
                var selector = {};
                selector._id = Meteor.userId();
                selector['profile.' + studyId + '.regulations.id'] = regulation.id;
                var modifier = {};
                modifier.$pull = {};
                modifier.$pull['profile.' + studyId + '.regulations.$.selfDefinedModules'] = id;
                Meteor.users.update(selector, modifier);
            });
        });
    };

    return self;
})();

Meteor.methods({
    addModuleToStudyOverview: function(studyId, moduleId) {
        StudyPlanController.addModule(studyId, moduleId);
    },
    removeModuleFromStudyOverview: function(moduleId) {
        StudyPlanController.removeModule(moduleId);
    },
    addRegulation: function(studyId, regulationName, regulationEcts) {
        StudyPlanController.addRegulation(studyId, regulationName, regulationEcts);
    },
    updateRegulation: function(studyId, regulationId, regulationName, regulationEcts) {
        StudyPlanController.updateRegulation(studyId, regulationId, regulationName, regulationEcts);
    },
    removeRegulation: function(regulationId) {
        StudyPlanController.removeRegulation(regulationId);
    },
    addModuleToRegulation: function(moduleId, studyId, regulationId) {
        StudyPlanController.removeModule(moduleId);
        StudyPlanController.addModuleToRegulation(moduleId, studyId, regulationId);
    },
    removeModuleFromRegulations: function(moduleId) {
        StudyPlanController.removeModuleFromRegulations(moduleId);
    },
    addSelfDefinedModule: function(moduleName, ects, studyId, regulationId) {
        var id = StudyPlanController.addSelfDefinedModule(moduleName, ects);
        StudyPlanController.addSelfDefinedModuleToRegulation(id, studyId, regulationId);

    },
    removeSelfDefinedModule: function(id) {
        StudyPlanController.removeSelfDefinedModule(id);
    },
    updateSelfDefinedModule: function(id, name, ects) {
        StudyPlanController.updateSelfDefinedModule(id, name, ects);
    },
    addSelfDefinedModuleToRegulation: function(id, studyId, regulationId) {
        StudyPlanController.removeSelfDefinedModuleFromRegulations(id);
        StudyPlanController.addSelfDefinedModuleToRegulation(id, studyId, regulationId);
    }
});