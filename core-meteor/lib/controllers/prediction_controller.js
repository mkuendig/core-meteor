PredictionController = (function() {
    var self = {};

    /**
     * Check if a module has a prediction for the logged in user
     * @param  {String} moduleId
     * @return {boolean}
     */
    self.predictionExists = function(moduleId) {
        return Modules.findOne({
            moduleId: moduleId,
            'predictions.userId': Meteor.userId()
        }) ? true : false;
    };

    /**
     * Update prediction of the logged in user
     * @param  {String} moduleId
     * @param  {Number} prediction null if no prediction
     */
    self.updatePrediction = function(moduleId, prediction, userId) {
        if (_.isUndefined(userId)) {
            userId = Meteor.userId();
        }
        Modules.update({
            moduleId: moduleId,
            'predictions.userId': userId
        }, {
            $set: {
                'predictions.$.prediction': prediction
            }
        });
    };

    /**
     * Create a prediction for module by the logged in user
     * @param {String} moduleId
     * @param {Number} prediction (optional)
     */
    self.addPrediction = function(moduleId, prediction, userId) {
        if (_.isUndefined(userId)) {
            userId = Meteor.userId();
        }
        Modules.update({
            moduleId: moduleId
        }, {
            $addToSet: {
                predictions: {
                    userId: userId,
                    prediction: prediction ? prediction : null
                }
            }
        });
    };

    /**
     * Get the current users prediction of the provided module
     * @param {Object} module Collection entry
     * @return {Number}
     */
    self.getPrediction = function(module) {
        var modulePrediction = null;
        _.find(module.predictions, function(prediction) {
            if (prediction.userId === Meteor.userId()) {
                modulePrediction = prediction.prediction;
                return true;
            }
        });
        if (!modulePrediction) {
            modulePrediction = 0;
        }
        return modulePrediction;
    };

    /**
     * Check if a module has a prediction for the logged in user
     * @param  {String} moduleId
     * @return {boolean}
     */
    self.getPredictionFromModuleId = function(moduleId, userId) {
        if (_.isUndefined(userId)) {
            userId = Meteor.userId();
        }
        var module = Modules.findOne({
            moduleId: moduleId,
            'predictions.userId': userId
        });
        var modulePrediction = null;
        if (module) {
            _.find(module.predictions, function(prediction) {
                if (prediction && prediction.userId === userId) {
                    modulePrediction = prediction.prediction;
                    return true;
                }
            });
        }
        return modulePrediction;
    };

    /**
     * Reset all predictions for current user
     */
    self.resetAllPredictions = function() {
        Modules.update({
            'predictions.userId': Meteor.userId()
        }, {
            $unset: {
                'predictions.$': ''
            }
        }, {
            multi: true
        });
    };

    self.markModuleAsNotInterested = function(moduleId) {
        Modules.update({
            moduleId: moduleId,
            'predictions.userId': Meteor.userId()
        }, {
            $set: {
                'predictions.$.isNotInterested': true
            }
        });
    };

    self.startPredictionUpdateProcess = function() {
        Session.set('isUpdatingTopPredictions', true);
        Meteor.call('updateTopPredictionsFromRecommender', function() {
            Session.set('isUpdatingTopPredictions', false);
        });
        setTimeout(function() {
            Meteor.call('updateAllPredictionsFromRecommender');
        }, 2000);
    };

    self.updatePredictions = function(predictions, userId) {
        if (_.isUndefined(userId)) {
            userId = Meteor.userId();
        }
        _.each(predictions, function(prediction) {
            var moduleId = prediction.meteorModuleId;
            var predictionRounded = Math.round(parseFloat(prediction.rating) * 100000) / 100000;
            var oldPrediction = self.getPredictionFromModuleId(moduleId, userId);
            if (oldPrediction) {
                if (oldPrediction !== predictionRounded) {
                    self.updatePrediction(moduleId, predictionRounded, userId);
                }
            } else {
                self.addPrediction(moduleId, predictionRounded, userId);
            }
        });
    };

    return self;
})();

Meteor.methods({
    markModuleAsNotInterested: function(moduleId) {
        PredictionController.markModuleAsNotInterested(moduleId);
        Meteor.call('exportNotInterested', moduleId);
    }
});
