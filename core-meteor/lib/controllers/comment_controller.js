Meteor.methods({
    insertComment: function(_moduleId, comment) {
        var userId = Meteor.userId();
        var createdAt = new Date();
        Modules.update({
            moduleId: _moduleId
        }, {
            $addToSet: {
                comments: {
                    createdById: userId,
                    createdAt: createdAt,
                    comment: comment,
                    upvotes: [],
                    downvotes: []
                }
            }
        });
        Meteor.call('logComment', 'CREATED', userId, _moduleId, createdAt.toString(), comment);
    },

    upvoteComment: function(_moduleId, createdById) {
        Modules.update({
            _id: _moduleId,
            'comments.createdById': createdById
        }, {
            $pull: {
                'comments.$.downvotes': Meteor.userId()
            },
            $addToSet: {
                'comments.$.upvotes': Meteor.userId()
            }
        });
    },

    removeUpvoteFromComment: function(_moduleId, createdById) {
        Modules.update({
            _id: _moduleId,
            'comments.createdById': createdById
        }, {
            $pull: {
                'comments.$.upvotes': Meteor.userId()
            }
        });
    },

    removeDownvoteFromComment: function(_moduleId, createdById) {

        Modules.update({
            _id: _moduleId,
            'comments.createdById': createdById
        }, {
            $pull: {
                'comments.$.downvotes': Meteor.userId()
            }
        });
    },

    downvoteComment: function(_moduleId, createdById) {
        Modules.update({
            _id: _moduleId,
            'comments.createdById': createdById
        }, {
            $pull: {
                'comments.$.upvotes': Meteor.userId()
            },
            $addToSet: {
                'comments.$.downvotes': Meteor.userId()
            }
        });
    },

    editComment: function(_moduleId, comment) {
        var userId = Meteor.userId();
        Modules.update({
            _id: _moduleId,
            'comments.createdById': userId
        }, {
            $set: {
                'comments.$.comment': comment
            }
        });
        Meteor.call('logComment', 'EDITED', userId, _moduleId, new Date().toString(), comment);
    },

    removeComment: function(_moduleId) {
        var userId = Meteor.userId();
        Modules.update({
            _id: _moduleId
        }, {
            $pull: {
                comments: {
                    createdById: userId
                }
            }
        });
        Meteor.call('logComment', 'REMOVED', userId, _moduleId, new Date().toString(), '');
    }
});