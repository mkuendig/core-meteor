SubsController = (function() {

    var self = {};

    var subs = new SubsManager();

    self.minimumSubscriptions = function() {
        return [
            subs.subscribe('organizationalUnits'),
            subs.subscribe('modules'),
            subs.subscribe('predictions'),
            subs.subscribe('semesterPlans'),
            subs.subscribe('ratings'),
            subs.subscribe('fbPermission'),
            subs.subscribe('experimentSubject')
        ];
    };

    self.essentialSubscriptions = function() {
        self.minimumSubscriptions();
        return [
            subs.subscribe('categories'),
            subs.subscribe('persons'),
            subs.subscribe('modulesAdditionalFields')
        ];
    };

    self.moduleLectures = function(moduleIds) {
        return [
            subs.subscribe('moduleLectures', moduleIds)
        ];
    };

    self.moduleDetails = function(moduleId) {
        return [
            subs.subscribe('moduleDetails', moduleId)
        ];
    };
    self.tutorialSubscriptions = function() {
        self.minimumSubscriptions();
        return [
            subs.subscribe('tutorialRecommendations')
        ];
    };
    self.humanRecommenderSubscriptions = function() {
        self.minimumSubscriptions();
        return [
            subs.subscribe('algRecommendations')
        ];
    }

    return self;
})();
