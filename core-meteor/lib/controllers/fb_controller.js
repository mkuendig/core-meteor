FBController = (function() {
    var self = {};

    self.initialize = function() {
        window.fbAsyncInit = function() {
            FB.init({
                appId: '120646968314854',
                xfbml: true,
                version: 'v2.5'
            });
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    };

    self.findOrCreateFBPermission = function(shibbolethId, fbId, fbAccessToken) {
        Meteor.call('findOrCreateFBPermission', shibbolethId, fbId, fbAccessToken, Meteor.userId(), function(error) {
            if (error) {
                console.log('error - findOrCreateFBPermission\n' + error);
            } else {
                Session.set('fbPermissionExists', true);
                Meteor.call('updateFBInformation', fbId, fbAccessToken);
            }
        });
    };

    self.loginToFB = function() {
        FB.login(function(response) {
            if (response.status !== 'connected') {
                return;
            }
            var fbId = response.authResponse.userID;
            var fbAccessToken = response.authResponse.accessToken;
            Meteor.call('updateFBInformation', fbId, fbAccessToken);
            Router.go('registrationStudyInformation');
        }, {
            scope: 'public_profile,user_friends'
        });
    };

    self.connectAAIWithFB = function(shibbolethId) {
        if (Session.get('fbResponse') !== undefined) {
            var fbId = Session.get('fbResponse').authResponse.userID;
            var fbAccessToken = Session.get('fbResponse').authResponse.accessToken;
            self.findOrCreateFBPermission(shibbolethId, fbId, fbAccessToken);
        }
    };

    self.fbPermissionExists = function() {
        return !_.isUndefined(FBPermission.findOne({
            userId: Meteor.userId()
        }));
    };

    if (Meteor.isServer) {
        var contains = function(id) {
            // Per spec, the way to identify NaN is that it is not equal to itself
            var findNaN = id !== id;
            var indexOf;

            if (!findNaN && typeof Array.prototype.indexOf === 'function') {
                indexOf = Array.prototype.indexOf;
            } else {
                indexOf = function(id) {
                    var i = -1,
                        index = -1;

                    for (i = 0; i < this.length; i += 1) {
                        var item = this[i];

                        if ((findNaN && item !== item) || item === id) {
                            index = i;
                            break;
                        }
                    }

                    return index;
                };
            }
            return indexOf.call(this, id) > -1;
        };

        Meteor.methods({
            'updateFBInformation': function(id, accessToken) {
                var ac = Meteor.http.call('GET', 'https://graph.facebook.com/debug_token?input_token=' + accessToken + '&access_token=120646968314854' + '|' + '70baeb00ae08465541dba6fbac6d6119');
                if (id !== ac.data.data.user_id) {
                    return false;
                }
                var result = Meteor.http.get('https://graph.facebook.com/v2.5/' + id + '?access_token=' + accessToken + '&fields=first_name, last_name, age_range, email, gender, locale, friends, timezone, verified');

                var updateId = FBInfo.findOrCreateFBInfo(Meteor.userId(), id);

                if (result.data.first_name) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            firstName: result.data.first_name
                        }
                    });
                }

                if (result.data.last_name) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            lastName: result.data.last_name
                        }
                    });
                }

                if (result.data.age_range) {
                    var min = '0';
                    var max = '100';
                    if (result.data.age_range.min) {
                        min = result.data.age_range.min;
                    }
                    if (result.data.age_range.max) {
                        max = result.data.age_range.max;
                    }
                    var range = min + '-' + max;
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            ageRange: range
                        }
                    });
                }

                if (result.data.email) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            email: result.data.email
                        }
                    });
                }

                if (result.data.gender) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            gender: result.data.gender
                        }
                    });
                }
                if (result.data.locale) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            locale: result.data.locale
                        }
                    });
                }
                if (result.data.timezone) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            timezone: result.data.timezone
                        }
                    });
                }
                if (result.data.verified) {
                    FBInfo.update({
                        _id: updateId
                    }, {
                        $set: {
                            verified: result.data.verified
                        }
                    });
                }
                var createdAt = new Date();
                //Add new friends
                if (result.data.friends && result.data.friends.data) {

                    var len = result.data.friends.data.length;
                    var allFriendsId = _.pluck(FBInfo.findOne(updateId).friends, 'facebookId');
                    for (var i = 0; i < len; i += 1) {
                        var friendId = result.data.friends.data[i].id;
                        var friendName = result.data.friends.data[i].name;
                        if (!contains.call(allFriendsId, friendId)) {
                            FBInfo.update({
                                _id: updateId
                            }, {
                                $addToSet: {
                                    friends: {
                                        facebookId: friendId,
                                        facebookName: friendName,
                                        createdAt: createdAt
                                    }
                                }
                            });
                        }
                    }
                

                    // Remove unfriended people
                    var currentFriendsId = [];
                    for(var i = 0; i < len; i += 1){
                        currentFriendsId.push(result.data.friends.data[i].id);
                    }
                    var len = allFriendsId.length;
                    for(var i = 0; i < len; i += 1){
                        if (!contains.call(currentFriendsId, allFriendsId[i])) {
                            FBInfo.update({
                                _id: updateId}, 
                                {$pull: {"friends": {facebookId: allFriendsId[i]}}}
                            );
                        }
                    }
                    if (result.data.friends.summary.total_count) {
                        FBInfo.update({
                            _id: updateId
                        }, {
                            $set: {
                                totalFriends: result.data.friends.summary.total_count
                            }
                        });
                    }
                }
                Meteor.call('registrationFbUpdate', Meteor.userId());
            },

            'getShibbolethId': function(fbId, inputTok) {
                var result = Meteor.http.call('GET', 'https://graph.facebook.com/debug_token?input_token=' + inputTok + '&access_token=120646968314854' + '|' + '70baeb00ae08465541dba6fbac6d6119');
                if (fbId === result.data.data.user_id) {
                    var fbPermission = FBPermission.findOne({
                        'facebookId': fbId
                    });
                    if (fbPermission) {
                        return fbPermission.shibbolethId;
                    } else {
                        return 'none';
                    }
                } else {
                    return 'none';
                }

            },

            'findOrCreateFBPermission': function(shibbolethId, fbId, fbToken, userId) {
                check(shibbolethId, String);
                check(userId, String);
                check(fbId, String);
                check(fbToken, String);

                var result = Meteor.http.call('GET', 'https://graph.facebook.com/debug_token?input_token=' + fbToken + '&access_token=120646968314854' + '|' + '70baeb00ae08465541dba6fbac6d6119');
                if (fbId === result.data.data.user_id) {
                    FBPermission.findOrCreateFBPermission(fbId, shibbolethId, userId);
                    Meteor.call('logConnectingAAIFB', 'ADDED', Meteor.userId(), new Date());
                }
            }
        });
    }

    if (Meteor.isClient) {
        self.initialize();
    }
    return self;
})();
