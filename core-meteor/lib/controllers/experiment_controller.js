ExperimentController = (function() {
	if (Meteor.isServer) {
        var contains = function(id) {
            // Per spec, the way to identify NaN is that it is not equal to itself
            var findNaN = id !== id;
            var indexOf;
            if (!findNaN && typeof Array.prototype.indexOf === 'function') {
                indexOf = Array.prototype.indexOf;
            } else {
                indexOf = function(id) {
                    var i = -1,
                        index = -1;
                    for (i = 0; i < this.length; i += 1) {
                        var item = this[i];
                        if ((findNaN && item !== item) || item === id) {
                            index = i;
                            break;
                        }
                    }
                    return index;
                };
            }
            return indexOf.call(this, id) > -1;
        };
		Meteor.methods({
            'updateTask3State' : function(num){
                ExperimentSubject.update({userId: Meteor.userId()},{
                //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                    $set:{
                        task3State: num
                    }
                })
                if(!_.isUndefined(ExperimentSubject.findOne({userId: Meteor.userId()})) && !_.isUndefined(ExperimentSubject.findOne({userId: Meteor.userId()}).recommendationMatrix) && _.isUndefined(ExperimentSubject.findOne({userId: Meteor.userId()}).personalizedTreatment)){
                    var randNum = Math.floor(Math.random() * 2);
                    var persCount = ExperimentSubject.find({personalizedTreatment: 1}).count();
                    var anonCount = ExperimentSubject.find({personalizedTreatment: 0}).count();
                    console.log('Current treatment allocation -> Personalized: ' + persCount + '\tAnonymized: ' + anonCount);
                    if(persCount > anonCount){
                        console.log('Assign user' + Meteor.userId() + ' width anonymous treatment (0) and humanListIsRight: ' + randNum);
                        ExperimentSubject.update({userId: Meteor.userId()},{
                        //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                            $set:{
                                personalizedTreatment: 0,
                                humanListIsRight: randNum
                            }
                        })
                    } else{
                        console.log('Assign user' + Meteor.userId() + ' width personalized treatment (1) and humanListIsRight: ' + randNum);
                        ExperimentSubject.update({userId: Meteor.userId()},{
                        //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                            $set:{
                                personalizedTreatment: 1,
                                humanListIsRight: randNum
                            }
                        })
                    }
                }
            },
            'task3StoreSurvey4' : function(ans1, ans2, ans3, ans4, ans5, ans6, ans7){
                var answers = {
                    answ1: ans1,
                    answ2: ans2,
                    answ3: ans3,
                    answ4: ans4,
                    answ5: ans5,
                    answ6: ans6,
                    answ7: ans7                 
                }
                ExperimentSubject.update({userId: Meteor.userId()},{
                //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                    $set:{
                        task3Survey4: answers
                    }
                })                

            },
            'task3StoreSurvey3' : function(answers){
                ans = [];
                for(var i = 0; i < answers.length; i++){
                    ans.push({answ1: answers[i].answ1, answ2: answers[i].answ2, answ3: answers[i].answ3, answ4: answers[i].answ4, answ5: answers[i].answ5, moduleId: answers[i].moduleId, userId: Meteor.userId()});
                }
                ExperimentSubject.update({userId: Meteor.userId()},{
                //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                    $set:{
                        task3Survey3: ans
                    }
                })
            },
            'task3StoreSurvey2' : function(hum, alg, lookups, text1, text2, text3){
                ExperimentSubject.update({userId: Meteor.userId()},{
                //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                    $set:{
                        task3Survey2Alg: alg,
                        task3Survey2Hum: hum,
                        task3Survey2Lookups: lookups,
                        task3Survey2FreeText1: text1,
                        task3Survey2FreeText2: text2,
                        task3Survey2HumanListEstimated: text3
                    }
                })
            },
            'task3StoreSurvey1' : function(answer){
                ExperimentSubject.update({userId: Meteor.userId()},{
                //ExperimentSubject.update({userId: 'tqdNoPzqnPtvfYdTo'},{
                    $set:{
                        task3Survey1: answer
                    }
                })
            },
            'assignRandomTreatments' : function(pw){
                /*
                if(pw != 'kjoDae8dc'){
                    return false;
                }  
                var allSubjects = ExperimentSubject.find({algRec: {$exists: true}, humRec: {$exists: true}});
                allSubjects.forEach(function(subj) {
                    var randValues = [1, 1, 1, 0, 0, 0, 0, 0];
                    var num1 = randValues[Math.floor(Math.random() * randValues.length)];
                    var num2 = Math.floor(Math.random() * 2);

                    console.log('Randomized allocation of treatment');
                    ExperimentSubject.update({userId: subj.userId}, {$set: {personalizedTreatment: num1, humanListIsRight: num2}});

                    console.log('Personalized & Right: ' + ExperimentSubject.find({personalizedTreatment: 1, humanListIsRight: 1}).count());
                    console.log('Personalized & Left: ' + ExperimentSubject.find({personalizedTreatment: 1, humanListIsRight: 0}).count());
                    console.log('Anonymous & Right: ' + ExperimentSubject.find({personalizedTreatment: 0, humanListIsRight: 1}).count());
                    console.log('Anonymous & Left: ' + ExperimentSubject.find({personalizedTreatment: 0, humanListIsRight: 0}).count());

                });    
                */
            },
            'storeRecommendationsMatrix' : function(pw){
                if(pw != 'kjoDae8dc'){
                    return false;
                }
                var allSubjects = ExperimentSubject.find({algRec: {$exists: true}, humRec: {$exists: true}});
                allSubjects.forEach(function(subj) {
                    console.log('Creating recommendation matrix for user ' + subj.userId);
                    var algRec = subj.algRec;
                    var humRec = subj.humRec;
                    var matrix = [];
                    for(var i = 0; i < algRec.length; i++){
                        matrix.push({userId: algRec[i].userId, order: 1, moduleId: algRec[i].moduleId, title: algRec[i].title, uzhUrl: algRec[i].uzhUrl, rating: 5});
                    }
                    for(var i = 0; i < humRec.length; i++){
                        var contained = false;
                        for(var j = 0; j < matrix.length; j++){
                            if(matrix[j].moduleId == humRec[i].moduleId){
                                contained = true;
                            }
                        }
                        if(!contained){
                            matrix.push({userId: humRec[i].userId, order: 1, moduleId: humRec[i].moduleId, title: humRec[i].title, uzhUrl: humRec[i].uzhUrl, rating: 5});
                        }
                    }
                    shuffle(matrix);
                    var ord = 1;
                    for(var i = 0; i < matrix.length; i++){
                        matrix[i].order = ord;
                        ord++;
                    }
                    console.log('Size of recommendation matrix: ' + matrix.length);

                    ExperimentSubject.update({userId: subj.userId}, {$set: {recommendationMatrix: matrix}});

                });                
            },
            'updateTask2State' : function(num){
                ExperimentSubject.update({userId: Meteor.userId()},{
                //ExperimentSubject.update({userId: 'YJNDbL57gh7Df7gKx'},{
                    $set:{
                        Task2Stage: num
                    }
                })
            },
            'storeSurvey2One' : function(ans1, ans2, ans3){
                if(ans1 && ans2 && ans3){
                    var answers = {
                        answ1: ans1,
                        answ2: ans2,
                        answ3: ans3                 
                    }
                    ExperimentSubject.update({userId: Meteor.userId()},{
                        $set: {
                            task2SurveyOne: answers
                        }
                    });
                    console.log('Successfully stored answers to first survey for user ' + Meteor.userId());
                } else{
                    console.log('Error occured while saving answers to survey one for user ' + Meteor.userId());
                }
            },
            'storeSurvey2Two' : function(ans4, ans5, ans6, ans7, ans8, ans9){
                var answers = {
                    answ4: ans4,
                    answ5: ans5,
                    answ6: ans6,
                    answ7: ans7,
                    answ8: ans8,
                    answ9: ans9                  
                }
                ExperimentSubject.update({userId: Meteor.userId()},{
                    $set: {
                        task2SurveyTwo: answers
                    }
                });
                console.log('Successfully stored answers to second survey for user ' + Meteor.userId());
            },
            'storeHumanRecommenderPredictionsAndLog' : function(subjId, matchId, recs, bin, log){

                console.log('Storing human Recommendations of user ' + matchId);
                var recList = [];
                for(var i = 0; i < 10; i++){
                    var rec = AlgRecommendations.findOne({moduleId: recs[i]});
                    recList.push({userId: matchId, order: i, moduleId: rec.moduleId, title: rec.title, uzhUrl: rec.uzhUrl, rating: 5});  
                }
                ExperimentSubject.update({'userId' : matchId}, {$set: {'humRec': recList}});

                console.log('Storing full sort list of user ' + matchId);
                ExperimentSubject.update({'userId' : matchId}, {$set: {'humRecSortList': recs}});

                console.log('Storing bin list of user ' + matchId);
                ExperimentSubject.update({'userId' : matchId}, {$set: {'humRecBinList': bin}});

                console.log('Storing recommend behaviour of user ' + subjId);
                ExperimentSubject.update({'userId': subjId}, {$set: {'recBehavior': []}});
                for(var i = 0; i < log.length; i++){
                    ExperimentSubject.update({'userId': subjId}, {$push: {'recBehavior': log[i]}});
                }       
            },
            'checkRegistrationThreshold' : function(){
                var numberRegistered = ExperimentSubject.find({'completedRegistration': 1}).count();
                if(numberRegistered > 999){
                    return false;
                } else{
                    return true;
                }
            },
            'getRegisteredFriends' : function(){
                var thisSubject = ExperimentSubject.findOne({userId: Meteor.userId()});
                if(thisSubject !== undefined){
                    return thisSubject.registeredFriends.length;
                } else{
                    console.log("Error during retrieval of registered friends");
                    return 0;
                }
            },
            'completeRegistration' : function(){
                var updateId = ExperimentSubject.findOrCreateExperimentSubject(Meteor.userId());
                ExperimentSubject.update({
                        _id: updateId
                    }, {
                        $set: {
                            completedRegistration: 1
                        }
                    });
                //Get all FB friends ever logged into the application
                var userObj = FBInfo.findOne({userId: Meteor.userId()});
                if(userObj == undefined){
                    console.log('FB info for user ' + Meteor.userId() + 'could not be retrieved');
                    return
                }
                var allFriendsId = _.pluck(userObj.friends, 'facebookId');
                
                //Test for all friends if they registered, and if yes, add them to the registered friends
                var len = allFriendsId.length;
                for (var i = 0; i < len; i += 1) {
                    var friend = FBInfo.findOne({facebookId: allFriendsId[i]});
                    if(friend  !== undefined){
                        var isRegistered = ExperimentSubject.findOne({userId: friend.userId});
                        if(isRegistered !== undefined){
                            if(isRegistered.completedRegistration === 1){
                                var thisSubject = ExperimentSubject.findOne({_id: updateId});
                                //Add registered friends to list
                                if(!contains.call(thisSubject.registeredFriends, friend.userId)){
                                    ExperimentSubject.update({
                                        userId: thisSubject.userId
                                    }, {
                                        $addToSet: {
                                            registeredFriends: {
                                                userId: friend.userId
                                            }
                                        }
                                    });
                                }
                                //Add user to lists of friends
                                if(!contains.call(friend.registeredFriends, thisSubject.userId)){
                                    ExperimentSubject.update({
                                        userId: friend.userId
                                    }, {
                                        $addToSet: {
                                            registeredFriends: {
                                                userId: thisSubject.userId
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    } 
                }
                Meteor.call('registrationComplete', Meteor.userId());

                var numberRegistered = ExperimentSubject.find({'completedRegistration': 1}).count();
                if(numberRegistered % 10 == 0){
                    var content = numberRegistered + ' users have registered.';
                    Email.send({
                      to: 'core@ifi.uzh.ch',
                      from: 'core@ifi.uzh.ch',
                      subject: 'Update',
                      text: content
                    });
                }
                var thisSubject = ExperimentSubject.findOne({userId: Meteor.userId()});
                if(thisSubject !== undefined){
                    return thisSubject.registeredFriends.length;
                } else{
                    console.log("Error during retrieval of registered friends");
                    return 0;
                }
            },
            'sendRegistrationMail': function(mode){
                    var participant;
                    if(Meteor.user() && Meteor.user().profile && Meteor.user().profile.callName){
                        participant = Meteor.user().profile.callName;
                    } else{
                        var participantEntry = Meteor.users.findOne({_id: Meteor.userId()});
                        if(participantEntry && participantEntry.profile && participantEntry.profile.callName){
                            participant = participantEntry.profile.callName;
                        }
                    }
                    var mailAddress;
                    if(Meteor.user() && Meteor.user().emails[0] && Meteor.user().emails[0].address && false){
                        mailAddress = Meteor.user().emails[0].address;
                    } else{
                        var participantEntry = Meteor.users.findOne({_id: Meteor.userId()});
                        if(participantEntry && participantEntry.emails[0] && participantEntry.emails[0].address){
                            mailAddress =  participantEntry.emails[0].address;
                        }                        
                    }
                    
                    if(participant == undefined){
                        participant = 'participant'
                    } else{
                        participant = participant.split(' ')[0];
                    }

                    if(mailAddress == undefined){
                        console.log("No mail address deposited");
                        return true;
                    }

                    Meteor.call('getRegisteredFriends', function(error, result) {
                        var friendString = '';
                        if(result == 0){
                            friendString = 'none of your Facebook friends have';
                        } else if (result == 1){
                            friendString =  result + ' of your Facebook friends has';
                        } else{
                            friendString =  result + ' of your Facebook friends have';   
                        }
                        var mailContent = 'Dear ' + participant + ',\n\n'+
                            'thank you for registering for the study about course recommender systems.\n\n'+
                            'After the end of the registration phase (after August, 21) we will contact you by email with instructions on the next steps.\n\n'+
                            'Currently, ' + friendString + ' registered for this study. To increase your chances of being matched (for tasks 2, 3, 4), please let your fellow students know about this study.\n\n'+
                            'Below please find the participation agreement for future reference. If you are unable to participate, change your mind, or if you have any questions, please let us know by sending an email to core@ifi.uzh.ch.\n\n'+
                            'Kind regards\n'+
                            'Your CoRe Team\n\n'+
                            'Participation Agreement\n\n'+
                            'I. Requirements\n\n'+
                            'To participate, you must be:\n'+
                            '- enrolled in a BSc or MSc program with the Faculty of Business, Economics and Informatics of the University of Zurich since Spring 2016 or earlier (we reserve the right to verify this information)\n'+
                            '- planning to take courses in Fall 2016\n'+
                            '- a Facebook user\n\n'+
                            'II. Tasks\n\n'+
                            'All tasks can be performed online on a computer (*not* from a smartphone or tablet):\n'+
                            '1. Registration (~20 min) until August 21, 2016\n'+
                            '2. Give course recommendations (~30 min) any time between August 22 and September 4, 2016\n'+
                            '3. Receive course recommendations (~20 min) any time between September 5 and 18, 2016\n'+
                            '4. Rate modules & fill out survey (~20 min) any time between February 13 and 27, 2017 (after the fall semester 2016)\n\n'+
                            'III. Compensation\n\n'+
                            '- 50 CHF for completing all tasks\n'+
                            'Note: for the tasks 2, 3, and 4, we will match you with another participant. In the unlikely event that we cannot match you, or if the study needs to be aborted for technical reasons, you will not be asked to complete the tasks 2, 3 and 4. In that case, you will only receive a compensation of 10 CHF for registering.\n'+
                            '- Payment in Migros gift vouchers by email after completion of your assigned tasks; to request payment in cash, please contact us at core@ifi.uzh.ch\n\n'+
                            'IV. Data and Privacy\n\n'+
                            'In this study, we collect the following information about you: name and email address from Switch/AAI, your public Facebook profile, a list of your Facebook friends who are also registered for this study, and the information provided by you during the study. All information is kept confidential, is processed anonymously, and is used only for research purposes. No personally identifiable information will be published or shared with third parties.\n'+
                            'For tasks 2 and 3, you will be matched with one of your Facebook friends and you will be asked to recommend modules to each other. During these tasks, your match partner will learn that you are participating in this study, which modules our recommendation algorithm would suggest to you, and which modules you eventually recommend to him/her. However, your match partner will *not* be told which modules you have rated and what ratings you have given these modules.\n\n'+
                            'By registering for this study you have confirmed that you understand and accept the participation agreement.';
     

                        Email.send({
                          to: mailAddress,
                          //to: 'hooljohannes@gmail.com',
                          from: 'core@ifi.uzh.ch',
                          subject: 'Registration Confirmation for Study about Course Recommender Systems',
                          text: mailContent
                        });
                    });
            },
            'updateFutureDegree': function(degree){
                var updateId = ExperimentSubject.findOrCreateExperimentSubject(Meteor.userId());
                ExperimentSubject.update({
                        _id: updateId
                    }, {
                        $set: {
                            hs16Degree: degree
                        }
                    });
            },
            'getFutureDegree' : function(){
                var id = Meteor.userId();

                var degr = ExperimentSubject.findOne({
                    userId: id
                });
                if(degr){
                    return degr.hs16Degree;
                } else{
                    return "undefined";
                }
            },
            'getNumberOfNotPredictedRatings' : function(pw){
                if(pw != 'kjoDae8dc'){
                    return false;
                }
                var allSubjects = ExperimentSubject.find({completedRegistration: 1, registeredFriends: {$ne: []}});
                allSubjects.forEach(function(subj) {
                    console.log(subj.userId);
                    console.log(Modules.find({'predictions.userId' : {$nin : [subj.userId]}}).count());
                });                
            },
            'updateAllExperimentPredictions' : function(pw){
                /*
                if(pw != 'kjoDae8dc'){
                    return false;
                }
                var length = ExperimentSubject.find({completedRegistration: 1, registeredFriends: {$ne: []}}).count();
                console.log('updating predictions for ' + length + ' users');
                var allSubjects = ExperimentSubject.find({completedRegistration: 1, registeredFriends: {$ne: []}});
                allSubjects.forEach(function(subj) {
                    Meteor.call('updateAllPredictionsFromRecommenderById', subj.userId);
                });
                */
                return true;
            },
            'storeTopAlgPred' : function(pw){
                /*
                if(pw != 'kjoDae8dc'){
                    return false;
                }
                var recs = AlgRecommendations.find({userId: 'zeiYyiRXeQ24Gk23C'});
                var pos = 0;
                var experimentPredictions = [];
                recs.forEach(function(rec){
                    if(pos < 10){
                        experimentPredictions.push({'userId': 'zeiYyiRXeQ24Gk23C', 'order': pos, 'moduleId': rec.moduleId, 'title': rec.title, 'uzhUrl': rec.uzhUrl, 'rating': 2});
                    }
                    pos++;
                });
                ExperimentSubject.update({'userId' : 'zeiYyiRXeQ24Gk23C'}, {$set: {'algRec': experimentPredictions}}); 
                */                               
            },
            'storeOrderedPredictionLists' : function(matched, pw){
                /*
            	if(pw != 'kjoDae8dc'){
            		return false;
            	}
                var allSubjects;
                if(matched == 'all'){
                    allSubjects = ExperimentSubject.find({'completedRegistration' :1});
                }
                if(matched == 'matched'){
                    allSubjects = ExperimentSubject.find({'completedRegistration' : 1, matchPartnerId: {$exists: true}});
                }
				 
				allSubjects.forEach(function(subj) {
                    var pos = 0;
                    AlgRecommendations.clearAlgRecommendations(subj.userId);
                    console.log('Assembling and sorting recommendations for user: ' + subj.userId);
            		var recs = Modules.getHS16RecommendationsByUserId(subj.userId).fetch();
                    if(subj.hs16Degree == 'Master'){
                        recs = _.filter(recs, function(module) {
                            var match = false;
                            _.find(module.categories, function(category) {
                                if (category.slug.indexOf('msc') > -1) {
                                    match = true;
                                    return true; // break out of _.find
                                }
                            });
                            return match;
                        });
                    }
                    shuffle(recs);
                    recs.sort(compare);
                    var experimentPredictions = [];
            		recs.forEach(function(rec){
                        if(pos < 10){
                            experimentPredictions.push({'userId': subj.userId, 'order': pos, 'moduleId': rec.moduleId, 'title': rec.title, 'uzhUrl': rec.uzhUrl, 'rating': rec.predictions[0].prediction});
                        }
                        AlgRecommendations.insert({'userId': subj.userId, 'order': pos, 'moduleId': rec.moduleId, 'title': rec.title, 'uzhUrl': rec.uzhUrl, 'rating': rec.predictions[0].prediction});
                        pos++;
            		});
                    ExperimentSubject.update({'userId' : subj.userId}, {$set: {'algRec': experimentPredictions}});
            		var nonRecs = Modules.getHS16MissingRecommendationsByUserId(subj.userId).fetch();
                    if(subj.hs16Degree == 'Master'){
                        nonRecs = _.filter(nonRecs, function(module) {
                            var match = false;
                            _.find(module.categories, function(category) {
                                if (category.slug.indexOf('msc') > -1) {
                                    match = true;
                                    return true;
                                }
                            });
                            return match;
                        });
                    }
                    shuffle(nonRecs);
            		nonRecs.forEach(function(rec) {
                        AlgRecommendations.insert({'userId': subj.userId, 'order': pos, 'moduleId': rec.moduleId, 'title': rec.title, 'uzhUrl': rec.uzhUrl, 'rating': 0});
                        pos++;
            		});
            		console.log("Inserted " + (pos-1) +" modules for user: " + subj.userId);
				});   
                */        	
            }
		});
	}
    function compare(a,b) {
      if (a.predictions[0].prediction < b.predictions[0].prediction)
        return 1;
      if (a.predictions[0].prediction > b.predictions[0].prediction)
        return -1;
      return 0;
    }

    function shuffle(a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    }   
})();