FilterController = (function() {
    var self = {};

    self.filterByOrganizationalUnit = function(modulesSubset, orgUnits) {
        return _.filter(modulesSubset, function(module) {
            var orgMatch = false;
            _.find(module.organizationalUnits, function(orgUnit) {
                if (_.contains(orgUnits, orgUnit.title)) {
                    orgMatch = true;
                    return true; // break out of _.find
                }
            });
            return orgMatch;
        });
    };

    self.filterByEcts = function(modulesSubset, ectsList) {
        return _.filter(modulesSubset, function(module) {
            var ectsMatch = false;
            if (_.contains(ectsList, module.ects)) {
                ectsMatch = true;
            }
            return ectsMatch;
        });
    };

    self.filterBySemester = function(modulesSubset, semesterTitle) { //semesterTitle = 'HS' or 'FS'
        return _.filter(modulesSubset, function(module) {
            return module.semester.title.substring(0, 2) === semesterTitle;
        });
    };

    self.filterByWeekdays = function(modulesSubset, weekdays) {
        return _.filter(modulesSubset, function(module) {
            var weekdaysMatch = false;
            _.find(module.weekdays, function(weekday) {
                if (_.contains(weekdays, weekday)) {
                    weekdaysMatch = true;
                    return true; // break out of _.find
                }
            });
            return weekdaysMatch;
        });
    };

    self.filterByLecturers = function(modulesSubset, lecturerIds) {
        return _.filter(modulesSubset, function(module) {
            var lecturersMatch = false;
            _.each(module.lecturers, function(moduleLecturer) {
                _.each(lecturerIds, function(lecturerId) {
                    if (moduleLecturer.uzhId === lecturerId) {
                        lecturersMatch = true;
                    }
                });
            });
            return lecturersMatch;
        });
    };

    self.filterByModuleCategories = function(modulesSubset, moduleCategories) {
        return _.filter(modulesSubset, function(module) {
            var match = false;
            _.find(module.categories, function(category) {
                if (_.contains(moduleCategories, category.slug)) {
                    match = true;
                    return true; // break out of _.find
                }
            });
            return match;
        });
    };

    self.filterBscModules = function(modulesSubset) {
        return _.filter(modulesSubset, function(module) {
            var match = false;
            _.find(module.categories, function(category) {
                if (category.slug.indexOf('msc') > -1) {
                    match = true;
                    return true; // break out of _.find
                }
            });
            return match;
        });
    };

    return self;
})();