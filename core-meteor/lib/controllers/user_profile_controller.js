UserProfileController = (function() {
    var self = {};

    var updateProfile = function(data) {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $set: data
        });
    };

    var deleteProfile = function(data) {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $unset: data
        });
    };

    /**
     * Updates the current profile of a user
     * @param faculty
     * @param studyCourse
     * @param level
     * @param field
     */
    self.updateCurrentProfile = function(studyProfile) {
        updateProfile({
            'profile.studyProfile.current': {
                faculty: studyProfile.faculty,
                studyCourse: studyProfile.studyCourse,
                level: studyProfile.level,
                field: studyProfile.field
            }
        });
    };

    /**
     * Updates the past profile of a user
     * @param faculty
     * @param studyCourse
     * @param level
     * @param field
     */
    self.updatePastProfile = function(studyProfile) {
        updateProfile({
            'profile.studyProfile.past': {
                faculty: studyProfile.faculty,
                studyCourse: studyProfile.studyCourse,
                level: studyProfile.level,
                field: studyProfile.field
            }
        });
    };

    /**
     * Deletes the past profile of a user
     */
    self.deletePastProfile = function() {
        deleteProfile({
            'profile.studyProfile.past': ''
        });
    };

    /**
     * Gets profile if available, null otherwise
     * @param status either 'current' or 'past'
     */
    self.getProfile = function(status) {
        try {
            return Meteor.user().profile.studyProfile[status];
        } catch (error) {
            return null;
        }
    };

    /**
     * Gets current profile if available, null otherwise
     */
    self.getCurrentProfile = function() {
        return self.getProfile('current');
    };

    /**
     * Gets past profile if available, null otherwise
     */
    self.getPastProfile = function() {
        return self.getProfile('past');
    };

    self.isMasterStudent = function() {
        var currentProfile = self.getCurrentProfile();
        if (currentProfile) {
            return currentProfile.level.substring(0, 3) === 'msc';
        } else {
            return false;
        }
    };

    return self;
})();