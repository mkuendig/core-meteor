/**
 * Parses a copy pasted student record page (http://www.students.uzh.ch/record.html)
 */
StudentRecordsParser = function() {
    var self = this;
    var typeIdentifiers = {
        'completedWithSuccess': ['erfolgreich abgeschlossen', 'Completed with Success'],
        'completedWithoutSuccess': ['ohne Erfolg beendet', 'Completed Unsuccessfully']
    };

    /**
     * Initializes parsing procedure
     * @param modulesString copy pasted string
     * @returns {*} an array with objects containing the attributes moduleId, completedWithSuccess, mark
     */
    self.parseModulesString = function(modulesString) {
        if (modulesString.length < 1) {
            return;
        }
        modulesString = self._splitLines(modulesString);
        return self._parseLines(modulesString);
    };

    self.filterUnknownModules = function(modules) {
        return _.filter(modules, function(module) {
            return Modules.findOne({
                'moduleId': module.moduleId
            });
        });
    };

    /**
     * Adds parsed modules as ratings (with value null)
     * @param modules modules parsed with the parseModulesString method
     */
    self.saveEntries = function(modules) {
        _.each(modules, function(module) {
            // add only rating if there is no relation yet
            if (!RatingController.ratingExists(module.moduleId)) {
                Meteor.call('setRating', module.moduleId, null);
            }
        });
    };

    /**
     * Split lines into array
     * @param modules string to be parsed
     * @returns {Array|*} array containing the lines
     * @private
     */
    self._splitLines = function(modules) {
        return modules.split('\n');
    };

    /**
     * Split all elementes separated by whitespace
     * @param line string to be parsed
     * @returns {Array|*} array containing all splitted elements
     * @private
     */
    self._splitWhitespace = function(line) {
        return line.split(/\s+/g);
    };

    /**
     * Parses a record
     * @param firstLine
     * @param secondLine
     * @param completedWithSuccess
     * @returns {{moduleId: *, completedWithSuccess: *, mark: *}}
     * @private
     */
    self._parseEntry = function(firstLine, secondLine, completedWithSuccess) {
        var firstLineSplitted = self._splitWhitespace(firstLine);
        var secondLineSplitted = self._splitWhitespace(secondLine);
        return {
            moduleId: firstLineSplitted[0],
            completedWithSuccess: completedWithSuccess,
            mark: secondLineSplitted[secondLineSplitted.length - 2]
        };
    };

    /**
     * Detects the type of a record string
     * @param typeIndetifiers
     * @param string
     * @returns {boolean}
     * @private
     */
    self._isType = function(typeIndetifiers, string) {
        for (var i = 0; i < typeIndetifiers.length; i += 1) {
            if (string.indexOf(typeIndetifiers[i]) > -1) {
                return true;
            }
        }
        return false;
    };

    /**
     * Parses all lines according to the record type
     * @param lines array of lines to be parsed
     * @returns {Array} parsed modules
     * @private
     */
    self._parseLines = function(lines) {
        var modules = [];
        for (var i = 0; i < lines.length - 1; i += 1) {
            var firstLine = lines[i];
            var secondLine = lines[i + 1];
            if (self._isType(typeIdentifiers.completedWithSuccess, secondLine)) {
                modules.push(self._parseEntry(firstLine, secondLine, true));
            } else if (self._isType(typeIdentifiers.completedWithoutSuccess, secondLine)) {
                //TODO in a further version we could change this to save the information about the completion
                modules.push(self._parseEntry(firstLine, secondLine, true));
            }
        }

        // remove all duplicates
        return _.uniq(modules, function(item) {
            return item.moduleId;
        });

    };
};