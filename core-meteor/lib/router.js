var continueEnabled;
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'pageNotFound',
    load: function() {
        $('.scrollable').animate({
            scrollTop: 0
        }, 0);
    }
});
Router.continueAfterLogin = function() {
    Router.go('task3Instructions');

};
var requireLogin = function() {
    if (Meteor.user()) {
        LanguageConfig.setLanguage(Meteor.user().profile.preferredLanguage, false);
        this.next();
    } else {
        LanguageConfig.setLanguage('de', false);
        if (Meteor.loggingIn()) {
            this.render(this.loadingTemplate);
        } else {
            this.render('accessDenied');
        }
    }
};
var requireAcceptedUserAgreement = function() {
        this.next();
};
Router.onBeforeAction('loading');
// For all the routes except for login, we check if the user is logged in before executing the action
Router.onBeforeAction(requireLogin, {
    except: ['aai', 'login', 'notAllowed']
});
// For all the routes except for login & welcomePage & registrationInformation, we check if the user has accepted the latest user agreement
Router.onBeforeAction(requireAcceptedUserAgreement, {
});
Router.onBeforeAction(function() {
    //Session.set('fbPermissionExists', FBController.fbPermissionExists());
    this.next();
});
Router.map(function() {
    this.route('aai', {
        path: '/login',
        layoutTemplate: 'publicLayout'
    });
    this.route('login', {
        path: '/',
        action: function() {
            //Read the information passed on by shibboleth
            var shibbolethId = headers.get('x-id');
            if (Meteor.user()) {
                Router.continueAfterLogin();
            } else {
                if (_.isUndefined(shibbolethId) || shibbolethId === '(null)') {
                    console.log('Missing authentication information');
                    throw new Meteor.Error(400, 'Missing authentication information');
                }
                //If the shibboleth id is present we hash it
                var hashedUsername = CryptoJS.SHA256(shibbolethId).toString();
                //The hashed id is the username, the id itself is the password
                Meteor.loginWithPassword(hashedUsername, shibbolethId, function(error) {
                    if (typeof error === 'undefined') {
                        //Meteor.call('logEvent', 'INFO', 'User logged in with AAI: ' + Meteor.userId());
                        Meteor.call('addCallName', headers.get('x-displayname'), function() {
                           Meteor.call('task3Login', Meteor.userId());
                            Router.continueAfterLogin();
                        });
                    } else if (error.error === 403 && error.reason === 'User not found') {
                        //If the login fails because there is no user yet, we create one
                        var email = headers.get('x-mail');
                        Meteor.call('createLogin', shibbolethId, email, Session.get('language'), headers.get('x-displayname'), function(error) {
                            if (!error) {
                                //Log the newly created user in
                                Meteor.loginWithPassword(hashedUsername, shibbolethId, function() {
                                    Meteor.call('task3Login', Meteor.userId());
                                    Router.continueAfterLogin();
                                });
                            } else {
                                console.log(error);
                            }
                        });
                    } else {
                        console.log(error);
                    }
                });
            }
        }
    });
    this.route('logout', {
        path: '/logout'
    });
    this.route('home', {
        path: '/home',
        action: function() {
            Session.set('semesterId', 'fs16'); // set to upcoming semester according to active tab
            this.render();
        },
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('facts', {
        path: '/facts'
    });
    this.route('modules', {
        path: '/modules',
        action: function() {
            Session.set('semesterId', null);
            this.render();
        },
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('moduleRatingProfile', {
        path: '/profile/module-rating',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('studyProfile', {
        path: '/profile/study-profile',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('importMethodProfile', {
        path: '/profile/import/method',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('moduleSelectionProfile', {
        path: '/profile/import/module-selection',
        action: function() {
            Session.set('semesterId', null);
            this.render();
        },
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('copyPasteImportProfile', {
        path: '/profile/import/copy-paste',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('studyPlan', {
        path: '/study-plan/:_id',
        data: function() {
            return {
                studyId: this.params._id
            };
        },
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('studyPlanTable', {
        path: '/study-overview/:_id',
        data: function() {
            return {
                studyId: this.params._id
            };
        },
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('moduleDetail', {
        path: '/module-detail/:_id',
        waitOn: function() {
            return SubsController.moduleDetails(this.params._id);
        },
        data: function() {
            return {
                module: Modules.findOne({
                    moduleId: this.params._id
                })
            };
        }
    });
    var renderSemesterPlan = function() {
        var semesterPlanId = SemesterPlans.findOrCreateSemesterPlan(this.params._semesterId, Meteor.userId());
        Session.set('semesterPlanId', semesterPlanId);
        Session.set('semesterId', this.params._semesterId);
        this.render();
    };
    this.route('semesterPlanCalendar', {
        path: '/semester-plan/:_semesterId/calendar',
        action: renderSemesterPlan,
        waitOn: function() {
            var semesterPlanId = SemesterPlans.findOrCreateSemesterPlan(this.params._semesterId, Meteor.userId());
            var moduleIds = SemesterPlans.getModuleIds(semesterPlanId);
            return SubsController.moduleLectures(moduleIds);
        }
    });
    this.route('semesterPlanOverview', {
        path: '/semester-plan/:_semesterId/overview',
        action: renderSemesterPlan,
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('semesterPlanModules', {
        path: '/semester-plan/:_semesterId/modules',
        action: renderSemesterPlan,
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('semesterPlanRecommendations', {
        path: '/semester-plan/:_semesterId/recommendations',
        action: renderSemesterPlan,
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationWelcomePage', {
        path: '/registration/welcome',
        layoutTemplate: 'registrationLayout'
    });
    this.route('registrationAAI', {
        path: 'registration/aai',
        layoutTemplate: 'registrationLayout',
    });
    this.route('registrationFacebook', {
        path: 'registration/facebook',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationStudyInformation', {
        path: 'registration/studyinformation',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationImportMethod', {
        path: '/registration/import/method',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationCopyPasteImport', {
        path: '/registration/import/copy-paste',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationModuleSelection', {
        path: '/registration/import/module-selection',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationModuleRating', {
        path: 'registration/rating',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationComplete', {
        path: '/registration/complete',
        layoutTemplate: 'registrationLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('registrationLogout', {
        path: 'registration/logout',
        layoutTemplate: 'registrationLayout',
    });
    this.route('task2Instructions', {
        path: '/recommendModules/instructions',
        layoutTemplate: 'task2Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('notAllowed', {
        path: 'notAllowed',
        layoutTemplate: 'task3Layout'
    });
    this.route('task2SurveyOne', {
        path: 'recommendModules/surveyOne',
        layoutTemplate: 'task2Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task2Tutorial', {
        path: 'recommendModules/tutorial',
        layoutTemplate: 'task2Layout',
        waitOn: SubsController.tutorialSubscriptions,
    });
    this.route('task2RecommendModules', {
        path: 'recommendModules/recommend',
        layoutTemplate: 'task2Layout',
        waitOn: SubsController.humanRecommenderSubscriptions
    });
    this.route('task2Survey', {
        path: 'recommendModules/surveyTwo',
        layoutTemplate: 'task2Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task2Complete', {
        path: 'recommendModules/complete',
        layoutTemplate: 'task2Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task3Instructions', {
        path: 'receiveRecommendations/instructions',
        layoutTemplate: 'task3Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task3SurveyOne', {
        path: 'receiveRecommendations/survey_1',
        layoutTemplate: 'task3Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task3SurveyTwo', {
        path: 'receiveRecommendations/survey_2',
        layoutTemplate: 'task3Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task3SurveyThree', {
        path: 'receiveRecommendations/survey_3',
        layoutTemplate: 'task3Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task3SurveyFour', {
        path: 'receiveRecommendations/survey_4',
        layoutTemplate: 'task3Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task3Complete', {
        path: 'receiveRecommendations/complete',
        layoutTemplate: 'task3Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task4Instructions', {
        path: 'rateModules/instructions',
        layoutTemplate: 'task4Layout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('task4ModuleSelection', {
        path: '/rateModules/module-selection',
        layoutTemplate: 'task4Layout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('task4ModuleRating', {
        path: '/rateModules/module-rating',
        layoutTemplate: 'task4Layout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('task4Complete', {
        path: '/rateModules/complete',
        layoutTemplate: 'task4Layout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('welcomePage', {
        path: '/wizard/welcome',
        layoutTemplate: 'wizardLayout',
        waitOn: SubsController.minimumSubscriptions
    });
    this.route('studyProfileWizard', {
        path: '/wizard/study-profile',
        layoutTemplate: 'wizardLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('importMethodWizard', {
        path: '/wizard/import/method',
        layoutTemplate: 'wizardLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('copyPasteImportWizard', {
        path: '/wizard/import/copy-paste',
        layoutTemplate: 'wizardLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('moduleSelectionWizard', {
        path: '/wizard/import/module-selection',
        layoutTemplate: 'wizardLayout',
        waitOn: SubsController.essentialSubscriptions
    });
    this.route('moduleRatingWizard', {
        path: '/wizard/rating',
        layoutTemplate: 'wizardLayout',
        waitOn: SubsController.essentialSubscriptions
    });
});