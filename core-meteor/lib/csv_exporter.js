CSVExporter = function() {
    
    var self = this;
    var tExport = '';
    var level = '';


    self.addContent = function(studyId, modules) {
        level = studyId;

        //create csv table with titles for export
        tExport += i18n('regulation') + ';' + i18n('moduleno') + ';' + i18n('title') + ';' + i18n('ects') + ';' + i18n('passed') + ';' + i18n('total');
        tExport += '\n';

        //each regulation
        var regulations = UIHelper.getRegulations(studyId);
        _.each(regulations, function(regulation) {
            tExport += regulation.name + ';;;;' + UIHelper.completedEcts(regulation) + ';' + regulation.ects + ';' + i18n('ects');
            tExport += '\n';

            //add modules
            if (regulation.modules && regulation.modules.length > 0) {
                _.each(regulation.modules, function(rId) {
                    var module = _.findWhere(modules, {
                        moduleId: rId
                    });
                    tExport += ';' + module.moduleId + ';' + module.title + ';' + module.ects;
                    tExport += '\n';
                });
            }

            //add selfDefinedModules
            if (regulation.selfDefinedModules) {
                var selfDefModules = Meteor.user().profile.selfDefinedModules;
                _.each(regulation.selfDefinedModules, function(rId) {
                    var module = _.findWhere(selfDefModules, {
                        id: rId
                    });
                    tExport += ';;' + module.name + ';' + module.ects;
                    tExport += '\n';
                });
            }
        });

        //add total
        var sum = 0;
        _.each(Meteor.user().profile[studyId].regulations, function(regulation) {
            sum += UIHelper.completedEcts(regulation);
        });
        tExport += i18n('total') + ';;;;' + sum + ';' + UIHelper.totalEctsRegulation(studyId) + ';' + i18n('ects');
    };

    self.download = function() {
        if (tExport.length < 1) {
            return false;
        }
        var blob = new Blob([tExport], {
            type: 'text/x-vCalendar;charset=utf-8'
        });
        var fileName = i18n('studyOverviewFileName') + '_' + level + '.csv';
        window.saveAs(blob, fileName);
        return tExport;
    };
};