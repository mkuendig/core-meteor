// Get all nodes for graph representation
print('Id,Label,Type');
db.users.find().forEach(function(user) {
    var studyCourse = '';
    if (user.profile.studyProfile) {
        if (user.profile.studyProfile.current.level === 'bsc_economics') {
            studyCourse = 'bsc_economics_student';
        } else if (user.profile.studyProfile.current.level === 'bsc_informatics') {
            studyCourse = 'bsc_informatics_student';
        } else if (user.profile.studyProfile.current.level === 'msc_informatics') {
            studyCourse = 'msc_informatics_student';
        } else if (user.profile.studyProfile.current.level === 'msc_economics') {
            studyCourse = 'msc_economics_student';
        } else {
            studyCourse = 'other_student';
        }
    } else {
        studyCourse = 'other_student';
    }
    print(user._id + ',User,' + studyCourse);
});
db.modules.find().forEach(function(module) {
    var moduleId = module.moduleId;
    var studyCourse = '';
    if (moduleId.indexOf('BOEC') > -1) {
        studyCourse = 'bsc_economics_module';
    } else if (moduleId.indexOf('AOEC') > -1) {
        studyCourse = 'bsc_economics_module';
    } else if (moduleId.indexOf('MOEC') > -1) {
        studyCourse = 'msc_economics_module';
    } else if (moduleId.indexOf('MFOEC') > -1) {
        studyCourse = 'msc_economics_module';
    } else if (moduleId.indexOf('DOEC') > -1) {
        studyCourse = 'phd_economics_module';
    } else if (moduleId.indexOf('AINF') > -1) {
        studyCourse = 'bsc_informatics_module';
    } else if (moduleId.indexOf('BINF') > -1) {
        studyCourse = 'bsc_informatics_module';
    } else if (moduleId.indexOf('MINF') > -1) {
        studyCourse = 'msc_informatics_module';
    } else if (moduleId.indexOf('DINF') > -1) {
        studyCourse = 'phd_informatics_module';
    } else {
        studyCourse = 'other_module';
    }
    print(module.moduleId + ',Module,' + studyCourse);
});
/**

Colors:


phd_economics_module    #EDAB70
bsc_economics_student   #15BAFF
other_module            #818741
msc_economics_module    #87173C
msc_economics_student   #0170FF
bsc_economics_module    #D41B79
bsc_informatics_student #47FA11
other_student           #54726C
msc_informatics_student #13721D
msc_informatics_module  #A02009
bsc_informatics_module  #FF3F18
phd_informatics_module  #EDAB71







**/