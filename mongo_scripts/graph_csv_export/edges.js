print('Source,Target');
db.modules.find().forEach(function(module) {
    module.ratings.forEach(function(rating) {
        if (rating.rating !== null) {
            print(rating.userId + ',' + module.moduleId);
        }
    });
});