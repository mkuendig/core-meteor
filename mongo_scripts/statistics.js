// print the number of ratings for each module
var totalRatingCount = 0;
var ratingSum = 0;
var bscEconomicsRatingCount = 0;
var mscEconomicsRatingCount = 0;
var bscInformaticsRatingCount = 0;
var mscInformaticsRatingCount = 0;
var ratingsCountPerModule = [];
var ratingValues = {};
var userRatingCounts = {};
var init = function() {
    db.modules.find({
        isActive: true
    }).forEach(function(module) {
        var moduleRatingCount = 0;
        module.ratings.forEach(function(rating) {
            if (rating.rating !== null) {
                totalRatingCount += 1;
                ratingSum += rating.rating;
                var user = db.users.findOne({
                    _id: rating.userId
                });
                if (user && user.profile.studyProfile) {
                    if (user.profile.studyProfile.current.level === 'bsc_informatics') {
                        bscInformaticsRatingCount += 1;
                    } else if (user.profile.studyProfile.current.level === 'msc_informatics') {
                        mscInformaticsRatingCount += 1;
                    } else if (user.profile.studyProfile.current.level === 'bsc_economics') {
                        bscEconomicsRatingCount += 1;
                    } else if (user.profile.studyProfile.current.level === 'msc_economics') {
                        mscEconomicsRatingCount += 1;
                    }
                }
                moduleRatingCount += 1;
                if (ratingValues[rating.rating]) {
                    ratingValues[rating.rating] += 1;
                } else {
                    ratingValues[rating.rating] = 1;
                }
                if (userRatingCounts[rating.userId]) {
                    userRatingCounts[rating.userId] += 1;
                } else {
                    userRatingCounts[rating.userId] = 1;
                }
            }
        });
        ratingsCountPerModule.push(moduleRatingCount);
    });
};
var printRatingsDistribution = function() {
    print('\n*** Rating distribution ***');
    for (var index in ratingValues) {
        print('Number of ' + index + '-star ratings: ' + ratingValues[index]);
    }
};
var printRatingsCountPerModule = function() {
    ratingsCountPerModule.sort(function(a, b) {
        return a - b;
    });
    var counter0 = 0;
    var counter1 = 0;
    var counter2 = 0;
    var counter3 = 0;
    var counter4 = 0;
    ratingsCountPerModule.forEach(function(ratingCount) {
        // print(ratingCount);
        if (ratingCount === 0) {
            counter0 += 1;
        } else if (ratingCount < 3) {
            counter1 += 1;
        } else if (ratingCount < 10) {
            counter2 += 1;
        } else if (ratingCount < 40) {
            counter3 += 1;
        } else {
            counter4 += 1;
        }
    });
    print('\n\n*** Rating count distribution (per module) ***');
    print('Modules with 0 ratings: ' + counter0);
    print('Modules with less than 3 ratings: ' + counter1);
    print('Modules with less than 10 ratings: ' + counter2);
    print('Modules with less than 40 ratings: ' + counter3);
    print('Modules with 40 or more ratings:  ' + counter4);
};
var printRatingsPerUser = function() {
    var counter0 = 0;
    var counter1 = 0;
    var counter2 = 0;
    var counter3 = 0;
    var counter4 = 0;
    for (var index in userRatingCounts) {
        if (userRatingCounts[index] < 10) {
            counter0 += 1;
        } else if (userRatingCounts[index] < 10) {
            counter0 += 1;
        } else if (userRatingCounts[index] < 20) {
            counter1 += 1;
        } else if (userRatingCounts[index] < 30) {
            counter2 += 1;
        } else if (userRatingCounts[index] < 40) {
            counter3 += 1;
        } else {
            counter4 += 1;
        }
    }
    print('\n\n*** Number of ratings per user ***');
    print('Users with less than 10 ratings: ' + counter0);
    print('Users with 10 - 20 ratings: ' + counter1);
    print('Users with 20 - 30 ratings: ' + counter2);
    print('Users with 30 - 40 ratings: ' + counter3);
    print('Users with 40 or more ratings: ' + counter4);
};
var printUserStatistics = function() {
    var totalUserCount = 0;
    var networkAgreementAcceptedCount = 0;
    var bscEconomicsStudentCount = 0;
    var mscEconomicsStudentCount = 0;
    var bscInformaticsStudentCount = 0;
    var mscInformaticsStudentCount = 0;
    var otherStudyCourseCount = 0;
    db.users.find().forEach(function(user) {
        totalUserCount += 1;
        if (user.profile.networkUserAgreement > 0) {
            networkAgreementAcceptedCount += 1;
        }
        if (userRatingCounts[user._id] > 0 && user.profile.studyProfile) {
            if (user.profile.studyProfile.current.studyCourse === 'informatics') {
                if (user.profile.studyProfile.current.level.substr(0, 3) === 'bsc') {
                    bscInformaticsStudentCount += 1;
                } else if (user.profile.studyProfile.current.level.substr(0, 3) === 'msc') {
                    mscInformaticsStudentCount += 1;
                }
            } else if (user.profile.studyProfile.current.studyCourse === 'economics') {
                if (user.profile.studyProfile.current.level.substr(0, 3) === 'bsc') {
                    bscEconomicsStudentCount += 1;
                } else if (user.profile.studyProfile.current.level.substr(0, 3) === 'msc') {
                    mscEconomicsStudentCount += 1;
                }
            } else if (user.profile.studyProfile.current.studyCourse === 'other') {
                otherStudyCourseCount += 1;
            }
        }
    });
    print('\n\n *** Users ***');
    print('User count (total): ' + totalUserCount);
    print('User count (with at least 1 rating): ' + Object.keys(userRatingCounts).length);
    print('Bsc Economics student count: ' + bscEconomicsStudentCount);
    print('Msc Economics student count: ' + mscEconomicsStudentCount);
    print('Bsc Informatics student count: ' + bscInformaticsStudentCount);
    print('Msc Informatics student count: ' + mscInformaticsStudentCount);
    print('Other study course: ' + otherStudyCourseCount);
    print('Users connected with FB: ' + db.fbPermission.find().count());
    print('Users who accepted network agreement: ' + networkAgreementAcceptedCount);
    print('\n\n');
};
init();
print('\n\n*** CoRe statistics ***\n');
print('Total rating count: ' + totalRatingCount);
print('Bsc Economics rating count: ' + bscEconomicsRatingCount);
print('Msc Economics rating count: ' + mscEconomicsRatingCount);
print('Bsc Informatics rating count: ' + bscInformaticsRatingCount);
print('Msc Informatics rating count: ' + mscInformaticsRatingCount);
print('Average rating: ' + ratingSum / totalRatingCount);
printRatingsDistribution();
printRatingsCountPerModule();
printRatingsPerUser();
printUserStatistics();
// print all ratings
// db.modules.find({
//     isActive: true
// }).forEach(function(module) {
//     module.ratings.forEach(function(rating) {
//         if (rating.rating !== null) {
//             print(rating.rating);
//         }
//     });
// });